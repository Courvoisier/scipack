


%module(directors="1",package="sp") spsig

%feature("autodoc", "1");  // 0 == no param types, 1 == show param types

%{
#include "lab/sig/sig.h"
#include "sciplot/core/sp.h"
#include "signalobject.h"
%}

%include "std_string.i"


// This is the defs of scipack
%include scipackdefs.i

// This is the defs of scipack.sciplot.core
%include sciplot/core/sciplotdefs.i
%import(module="scipack.sciplot.sp")  sciplot/core/object.i
%import(module="scipack.sciplot.sp")  sciplot/core/container.i

%include lab/sig/sigdefs.i
%import(module="scipack.lab.sig") lab/sig/sig.i


%rename(SignalObject) spSignalObject;

%feature("director") spSignalObject;

// We set the kwargs for some methods
%feature("kwargs") spSignalObject::spSignalObject;

%DocClass(spSignalObject,"A graphic sciplot object for displaying signals, i.e., instances of the class sciplot.lab.sig.Signal.")
class spSignalObject : public spObject
{
	public : 

    // The string representation in python
    ObjRepr;

	virtual void Draw(spRect &rect);

	virtual ~spSignalObject();
    
	spSignalObject(spContainer *container,SIGNAL signal, unsigned long flags = 0, const char *name = NULL);

    %DocMeth(IsSignal,"IsSignal(signal) -> bool","Returns True if the signal being displayed by the spSignalObject is the same as signal.")
	bool IsSignal(SIGNAL sig);
    
    %DocMeth(GetSignal,"GetSignal(signal) -> Signal","Returns the signal displayed by the spSignalObject.")
	SIGNAL GetSignal();

    %extend {
        bool __test(char *name) {if (SWIG_TypeQuery(name)) return true; else return false;}
    }

    //
    //
    //  ATTRIBUTES
    //
    //

    %attobj_remove(pos)
    %attobj_remove(pos1)
    %attobj_remove(margin)
    %attobj_remove(size)

    // Attribute : symbol
    %extend{
        void _SetSymbol(char *c, double param=0) {
            int size;
            $self->AttBeforeUpdate(); 
            if (!strcmp(c,"") || !strcmp(c,"_")) $self->SetAttribute(new spSignalAttributeLine());
            else if (!strcmp(c,".")) $self->SetAttribute(new spSignalAttributeDot());
            else if (!strcmp(c,"C")) $self->SetAttribute(new spSignalAttributeConst());
            else if (!strcmp(c,"D")) $self->SetAttribute(new spSignalAttributeDirac());
            else if (!strcmp(c,".")) {
                    size = (int) param;
                    if (size == 0) size = 4;
                    $self->SetAttribute(new spSignalAttributeCross(size));
            }
            else if (!strcmp(c,"o")) {
                    size = (int) param;
                    if (size == 0) size = 4;
                    $self->SetAttribute(new spSignalAttributeCircle(size));
            }
            else if (!strcmp(c,"+")) {
                    size = (int) param;
                    if (size == 0) size = 4;
                    $self->SetAttribute(new spSignalAttributeCross(size));
            }
            else if (!strcmp(c,"o_") || !strcmp(c,"_o")) {
                    size = (int) param;
                    if (size == 0) size = 8;
                    $self->SetAttribute(new spSignalAttributeLineCircle(size));
            }
            else if (!strcmp(c,"Do")) {
                    size = (int) param;
                    if (size == 0) size = 12;
                    $self->SetAttribute(new spSignalAttributeDiracCircle(size));
            }
            else if (!strcmp(c,"oC")) {
                    size = (int) param;
                    if (size == 0) size = 8;
                    $self->SetAttribute(new spSignalAttributeCircleConst(size));
            }
            else if (!strcmp(c,"|")) {
                    $self->SetAttribute(new spSignalAttributeHisto(param));
            }
            $self->AttAfterUpdate();
        }
        PyObject *_GetSymbol() {
            if (!strcmp($self->attribute->GetName(),"Const")) return PyString_FromString("C");
            if (!strcmp($self->attribute->GetName(),"Dirac")) return PyString_FromString("D");
            if (!strcmp($self->attribute->GetName(),"CircleDirac")) return PyString_FromString("Do");
            if (!strcmp($self->attribute->GetName(),"CircleConst")) return PyString_FromString("oC");
            if (!strcmp($self->attribute->GetName(),"LineCircle")) return PyString_FromString("o_");
            if (!strcmp($self->attribute->GetName(),"Line")) return PyString_FromString("_");
            if (!strcmp($self->attribute->GetName(),"Dot")) return PyString_FromString(".");
            if (!strcmp($self->attribute->GetName(),"Cross")) return PyString_FromString("+");
            if (!strcmp($self->attribute->GetName(),"Circle")) return PyString_FromString("o");
            if (!strcmp($self->attribute->GetName(),"Histo")) return PyString_FromString("|");
            return Py_None;
        }
    }
    %attobj_arglist_def(symbol,Symbol,
        """
        **Attribute** self.symbol = curve_type [,param]
    
        The symbol used to draw the signal.
    
            param       : a number which is used as a parmameter for certain curve_type

            curve_type  : a string that defines the symbol used for drawing the signal, either
                - '_' : a regular (linear interpolation) line
                - 'C' : step-wise function (right continuous)
                - 'o' : circles around each sample (param = pixel size of circles)
                - 'oC' : stepwise function with circles around the samples (param = pixel size of circles)
                - 'o_' : linear interpolation with circles around the samples (param = pixel size of circles)
                - '.' : dots for each sample
                - '+' : cross for each sample (param = pixel size of crosses)
                - '|' : histogram type (param = ordinate base (by default 0))
                - 'D' : used to represent succesive Dirac's
                - 'Do' : same as 'D' but with circles on the Dirac's

        """);


    // Attribute : labelx
    %extend {
        void _SetLabelx(const char *label) {
            if ($self->container && !strcmp($self->container->GetClassName(),"spView") && $self->container->container && !strcmp($self->container->container->GetClassName(),"spFramedView")) {
                spFramedView *fv = (spFramedView *) $self->container->container;
                fv->GetLabelxObject()->SetText(label);
            }
        }
        const char *_GetLabelx() {
            if ($self->container && !strcmp($self->container->GetClassName(),"spView") && $self->container->container && !strcmp($self->container->container->GetClassName(),"spFramedView")) {
                spFramedView *fv = (spFramedView *) $self->container->container;
                return fv->GetLabelxObject()->GetText();
            }
            return _CCHAR("");
        }
    }
    %attobj_def(labelx,Labelx,
        """
        **Attribute** self.labelx = string
    
        The label of the x-axis of the framedView the signal is drawn in
        (i.e., this is an attribute of the framedView which corresponds to self.container.container)
        """);


    // Attribute : labelx
    %extend {
        void _SetLabely(const char *label) {
            if ($self->container && !strcmp($self->container->GetClassName(),"spView") && $self->container->container && !strcmp($self->container->container->GetClassName(),"spFramedView")) {
                spFramedView *fv = (spFramedView *) $self->container->container;
                fv->GetLabelyObject()->SetText(label);
            }
        }
        const char *_GetLabely() {
            if ($self->container && !strcmp($self->container->GetClassName(),"spView") && $self->container->container && !strcmp($self->container->container->GetClassName(),"spFramedView")) {
                spFramedView *fv = (spFramedView *) $self->container->container;
                return fv->GetLabelyObject()->GetText();
            }
            return _CCHAR("");
        }
    }
    %attobj_def(labely,Labely,
        """
        **Attribute** self.labely = string
    
        The label of the y-axis of the framedView the signal is drawn in
        (i.e., this is an attribute of the framedView which corresponds to self.container.container)
        """);


    // Attribute : title
    %extend {
        void _SetTitle(const char *title) {
            if ($self->container && !strcmp($self->container->GetClassName(),"spView") && $self->container->container && !strcmp($self->container->container->GetClassName(),"spFramedView")) {
                spFramedView *fv = (spFramedView *) $self->container->container;
                fv->GetTitleObject()->SetText(title);
            }
        }
        const char *_GetTitle() {
            if ($self->container && !strcmp($self->container->GetClassName(),"spView") && $self->container->container && !strcmp($self->container->container->GetClassName(),"spFramedView")) {
                spFramedView *fv = (spFramedView *) $self->container->container;
                return fv->GetTitleObject()->GetText();
            }
            return _CCHAR("");
        }
    }
    %attobj_def(title,Title,
        """
        **Attribute** self.title = string
    
        The title of the framedView the signal is drawn in
        (i.e., this is an attribute of the framedView which corresponds to self.container.container)
        """);

    // Attribute : real
    %extend {
        void _SetReal(bool flag) {
            $self->flagReal = flag;
        }
        bool _GetReal() {
            return $self->flagReal;
        }
    }
    %attobj_def(real,Real,
        """
        **Attribute** self.real = bool
    
        If True then the real part of the signal is displayed. Otherwise it is the imaginary part which is displayed
        """);

};


//
// Python code
//
//
%pythoncode %{
import os
execfile(os.path.split(os.path.abspath(__file__))[0]+"/_signalobject.py")	
%}

