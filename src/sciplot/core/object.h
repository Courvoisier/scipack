
#ifndef _SP_Object_H_
#define _SP_Object_H_


class spContainer;



class SPEXPORT spCursorCallback
{
	public :
    
#ifdef SP_SCRIPT
    // The Python callback
	void *scriptFunc;
    void SetScriptFunc(void *ptr) {scriptFunc = ptr;}
#endif
    
    // The callback (a C++ function that can eventually trigger to the scriptFunc)
	const char* (*getFunc)(spObject *,bool, spRealPoint &);
	inline bool IsSet() {return (getFunc != NULL);}
    void SetGetFunc(const char *(*ptr)(spObject *,bool, spRealPoint &)) {getFunc = ptr;}
    void SetGetFunc(const char *(*ptr)(spCursorCallback *, spObject *,bool, spRealPoint &)) {getFunc = (const char * (*)(spObject *,bool, spRealPoint &)) ptr;}
    
    // The method to call the call back (either C function or the script function)
    const char * Call(spObject *,bool, spRealPoint &);
    
    // The constuctors/destructor
	spCursorCallback(const char *(*func)(spObject *,bool, spRealPoint &)=NULL);
	~spCursorCallback();
	spCursorCallback(const spCursorCallback & ccb);
    
	// The copy operator
    spCursorCallback& operator =(const spCursorCallback& other);
};




//
//
// This file defines the main class of the library : the spObject class
// The spObject class corresponds to the base class from which derives any displayable object
//
// Most spObject belongs to a spContainer object (which is itself an spObject) and so on recursively. The toppest spObject is a spTopview.
// Each spTopview is associated to a spCanvas.
//
// Sometimes you need to have an object which "follows" the mouse. This is of course the case for any cursor like object
// (e.g., a cross hair object or a little text box displaying the coordinates of the mouse).
// Generally you want these objects to be on top of all the other ones (i.e., they are the last objects drawn on the window).
// Moreover they should be able to draw/erase themselves in a very efficient way.
// In SciPlot, these objects (which are still of class spObject) are referred to as "overlay" objects.
// Two types of overlay objects are implemented
//   a- objects that are drawn/erased using the wxINVERT logical mode, inversing the pixel colors (this is useful for instance for a cross hair cursor)
//   b- objects that are drawn/erased using the double buffer of the window (this could be used for little box cursor)
//
// The list of all these objects are kept in a list of the spTopview.
// WARNING : These objects are NOT included in the list of objects belonging to a spContainer, though they behave as if they belong to a spContainer (in terms of local coordinates, clipping and so on ...)
//
// REMARK : Overlay object cannot receive any event
//


// This is the different type of possible overlay
typedef enum spOverlayType_ {
	spOverlayNone = 0,
	spOverlayBuffer
} spOverlayType;


//
// The spObject class
//
class SPEXPORT spObject : public spRef
{
	public :
    
    virtual bool IsInteractor() {return false;};
    
    spCursorCallback cursorCallback;
    bool HasCursorCallback() {return cursorCallback.IsSet();}
	const char *ExecuteCursorCallback(bool flagFullInfo, spRealPoint &pt) {
		return cursorCallback.Call(this,flagFullInfo,pt);
	}
    void SetCursorCallback(spCursorCallback cb) {
        cursorCallback = cb;
    }
            
    
	//
	// Constructors/Destructor
	//
	spObject(int i,const char *name=NULL);  // Only used when creating a TopView
	spObject(spContainer *container=NULL,const spAnchor &anchor=spAnchor(),const spMargin &margin=spMargin(),unsigned long flags=0x0,const char *name=NULL);	   // Main constructor
	virtual ~spObject();                                                   // Destructor
	
	// The container this object is in
	spContainer *container;
    spContainer *GetContainer() {return container;}
    spContainer *GetNonGroupContainer();
    
	// Simple tests of class belonging
	virtual bool IsContainer() {return false;};
	virtual bool IsGrid() {return false;};
	virtual bool IsView() {return false;};
	virtual bool IsTopContainer() {return false;};
	virtual bool IsObject() {return true;}
	
	spCanvas *GetCanvas();
    
	//
	// Dealing with the bounding rectangle
	//
	// The bounding rectangle can be expressed either in window's (global) coordinates or in container's (local) coordinates
	// The box object allows to compute them.
	//
	// !!!! None of these methods/fields should be used directly except for NotifyGeometryChange() and UpdateGeometryMain().
	//
	// - UpdateGeometryMain()              : should be called to recompute the gBoundingRect() (This is a virtual function ==> do not call it in a constructor!!! Use NotifyGeometryChange() instead)
	// - NotifyGeometryChange() : should be called whenever UpdateGeometryMain should be called at the next Update()
	//
	spRect _gBoundingRect;                  // The bounding rect in global coordinate
	virtual spRect &gBoundingRect() {return _gBoundingRect;}
    
	spRect _gRect;                          // The rect in global coordinate
	virtual spRect &gRect() {return _gRect;}
    
	virtual spRealRect lRect();             // The rect in local coordinate

	spBox box;                              // The box object
	bool flagGeometryMustBeUpdated;	    // If true then UpdateGeometryMain should be called on this object
	
	virtual void UpdateGeometryMain(bool flagSendGroupEvent = true);       // Routine that takes care of the computation of the gBoundingRect (redefined in spViews in order to compute recursively the gBoundingRect of all the objects it contains)
    virtual void UpdateGeometry();
    virtual void NotifyGeometryChange();	 // Notify that the gBoundingRect should be computed
	
    // Returns a (positive) number that measures the distance of a (global) point to the object
    // (0 if the point is within the object)
    // It is used to
	virtual double GetDistance(spRealPoint pt);
	
    
	//
	// Attributes
	//
	// There are 3 attributes : Pen and Brush and Fonts
	//
    
	// Pen Attribute
    wxPen pen;
    void SetPen(const wxPen &pen);
    void SetPen(const wxColour &colour, int width = 1, int style = wxSOLID);
    void SetPen(const wxString& colourName, int width = 1, int style = wxSOLID);
    void SetPen(const wxBitmap& stipple, int width = 1);
    void SetPenStyle(int style = wxSOLID);
    void SetPenWidth(int width);
    void SetPenColour(const wxColour colour);
    inline wxColour GetPenColour() {return pen.GetColour();}
    inline int GetPenWidth() {return pen.GetWidth();}
    inline int GetPenStyle() {return pen.GetStyle();}
    wxPen &GetPen() {return GetPen();}
    
	// Brush Attribute
    bool flag_white_brush_printing;
    void SetWhiteBrushPrinting(bool flag) {flag_white_brush_printing = flag;}
    bool GetWhiteBrushPrinting() {return flag_white_brush_printing;}
    wxBrush brush;
    void SetBrush(const wxColour &colour, int style = wxSOLID);
    void SetBrushStyle(int style = wxSOLID);
    void SetBrush(const wxString& colourName, int style = wxSOLID);
    void SetBrush(const wxBitmap& stipple);
    void SetBrush(const wxBrush &brush1);
    void SetBrushColour(const wxColour colour);
    int GetBrushStyle() {return brush.GetStyle();}
    wxBrush &GetBrush();
    wxColour GetBrushColour();
    const wxBitmap *GetBrushStipple() {return brush.GetStipple();}
    
    // Font Attribute
    wxFont font;
    void SetFont(int pointSize, wxFontFamily family=wxFONTFAMILY_DEFAULT, int style=wxFONTSTYLE_NORMAL, wxFontWeight weight=wxFONTWEIGHT_NORMAL, const bool underline = false, const wxString& faceName = "", wxFontEncoding encoding = wxFONTENCODING_DEFAULT);
    void SetFont(const wxFont &font1);
    void SetFontWeight(int font);
    void SetFontSize(int size);
    void SetFontStyle(int style=wxFONTSTYLE_NORMAL);
    void SetFontFaceName(char *faceName);
    void SetFontFamily(int family);
    int GetFontSize() {return font.GetPointSize();};
    int GetFontStyle() {return font.GetStyle();};
    int GetFontWeight() {return font.GetWeight();}
    char *GetFontFaceName();
    int GetFontFamily() {return font.GetFamily();}
    wxFont &GetFont() {return font;};
    
    wxColour textForegroundColor,textBackgroundColor;
    void SetTextBackground (const wxColour colour);
    void SetTextForeground (const wxColour colour);
    wxColour GetTextBackground () {return textBackgroundColor;}
    wxColour GetTextForeground () {return textForegroundColor;}

    
    // Make this attributes the current attributes
    void UseAttributes();
    void UseTextColour(wxColour &textBackgroundColor,wxColour &textForegroundColor);
    void UsePen(const wxPen &pen);
    void UseBrush(const wxBrush &brush);
    void UseFont(const wxFont &font);
    
    //
    // Dealing with visiblity of the object
    //
    bool flagHidden;      // Indicates whether the object is hidden or not (used for both overlay objects and regular objects)
    inline bool IsHidden() {return flagHidden;}
    bool IsVisible();     // Is the object visible ? (It is not visible either if it is hidden or if one of its super views is hidden)
    bool Hide();          // Hide the object
    void Show();          // Show the object
    
    //
    // Dealing with overlay objects
    //
    spOverlayType overlayType;
    inline bool IsOverlay() {
        return overlayType != spOverlayNone;
    }
    bool overlayActivated;
    inline bool IsOverlayActivated() {
        return overlayActivated;
    }
    void DrawOverlay();
    void EraseOverlay();
    void ActivateOverlay();
    void DeactivateOverlay(); // !! This method unref the object so it can delete it You must hold a reference before calling it
    

    virtual void Move(double dx,double dy);                    // Move the object using translation dx,dy expressed in coordinates of the anchor point.
    inline void Move(spRealPoint dv) {Move(dv.x,dv.y);}
    bool IsDefinedBy2Points();              // Is the rect can be defined by two points including the anchor ?
    virtual bool IsMoveable() {return box.IsMoveable();};  // Is the object moveable ?
    inline bool IsResizeable() {return box.IsResizeable();}
    virtual spRealPoint GetAnchor(bool flagUpdate=false,double xmin=1,double xmax=-1);
    virtual void SetAnchor(spRealPoint anchor);
    spRealPoint GetAnchorOposite();
    void SetAnchorOposite(spRealPoint pt);
    void SetSize(double dx, double dy);
    void SetSize(spRealPoint dv) {SetSize(dv.x,dv.y);}
    void SetMargin(double xMargin, double yMargin, double mxMargin, double myMargin);
    inline spRealRect GetMargin() {return box.GetMargin(this);}
    inline void GetMargin(double *xMargin, double *yMargin, double *mxMargin, double *myMargin) {box.GetMargin(xMargin,yMargin,mxMargin,myMargin);}
    spRealPoint GetSize() {return box.GetSize();}
    inline spContainer *GetContainerMargin() {return box.GetContainerMargin(this);}
	inline void SetPixelTranslation() {return box.SetPixelTranslation();}

    virtual void AttBeforeUpdate();
    virtual void AttAfterUpdate();
    
    //
    // Dealing with Drawing
    //
    void DrawAll();  // Draw all the object
    virtual void Draw(spRect &rect);      // Pure virtual method for painting the part of the object clipped by the (global) rectangle rect
    void NotifyRectMustBeDrawn(spRect &r);  // Notify that the part of the object clipped by the (global) rectangle rect should be drawn at he next Update
    
    
    //
    // Dealing with events
    //
    virtual bool ProcessEvent(spEvent &event); // Main procedure to process an event
    bool ProcessEvent1(spEvent &event);        // Same as ProcessEvent but does not take care of leave/enter events and dragging
    void ProcessPreLeaveEnterEvent();          // Take care of leave/enter events before current event is processed
    void ProcessPostLeaveEvent();              // Take care of leave events after current event is processed
    void ProcessMsgeEvent(spEvent &event);

    spList<spInteractor,0,10> interactorList;
    void AddInteractor(spInteractor *cb);
    bool RemoveInteractor(spInteractor *cb);
    
    void SendMsge(spObject *receiver,spMsge msge);
    
    virtual void NotifyCanvasDeleted() {};
    
    //
    // Yes if object can handle cursor
    //
    virtual void SetText(const char *str) {};
    
    
    //////////////////////////////////////////////////////////////////
    //
    // DRAWING PROCEDURES
    //
    //////////////////////////////////////////////////////////////////
    void DrawPolygon(int n, spRealPoint *points);
    void DrawPolygon(int n, spPoint *points);
	void DrawArrowTip(spRealPoint tip, double angle, double length = 25, double alpha=45);
    void DrawRectangle(spRealRect r);
    void DrawRectangle(double x, double y, double w, double h);
    void DrawEllipse(spRealRect r);
    void DrawEllipse(double x, double y, double w, double h);
    void DrawCEllipse(double x, double y, double w, double h);
    void DrawCEllipse(spRealPoint pt, double w, double h=-1);
    void DrawLine(double x0, double y0, double x1, double y1);
    void DrawLine(spRealPoint pt1,spRealPoint pt2);
    void GetTextExtent(const wxString& string,int *x, int *y,int *descent = NULL,int *externalLeading = NULL,wxFont *theFont = NULL );
    void DrawText(const wxString& s,double x, double y, double angle=0);
    void DrawTextBase(const wxString& s,double x, double y, double angle);
    void DrawPoint(double x, double y);
    void DrawFormattedText(const char *str, char hPositionMode, double x, char vPositionMode, double y, double angle=0);
    void DrawFormattedText(spFormattedText &ft);
    void SetClip(spRect &rect);
    void SetClip(spRealRect &rect);
    void DeactivateDCBuffer();
    void ActivateDCBuffer();
    void DCBufferToDC(spRect &);

    spPoint Canvas2Screen(spPoint &);
    spRealPoint GetPointProjectionOnLine(spRealPoint pt, spRealPoint line_pt,double line_angle);
    
    // Dealing with boxes
    inline bool IsMarginInfinite() {return box.IsInfinite();}
    inline bool IsMarginGrid() {return box.IsGrid();}
    inline bool IsMarginPixelSize() {return box.IsPixelMargin();}
    inline bool IsMarginPixelStraightSize() {return box.IsPixelStraightMargin();}
    inline bool IsMarginEmpty() {return box.IsEmpty();}
        
    
        
    inline void UnionFloatRect(spRealRect &rect,spObject *object, bool &flagRectEmpty,double xmin, double xmax) {box.UnionFloatRect(rect,object,flagRectEmpty,xmin,xmax);}
    inline void UnionFloatRect(spRealRect &rect,spObject *object, bool &flagRectEmpty) {box.UnionFloatRect(rect,object,flagRectEmpty);}
    inline void ExpandX(spObject *object, double &xMin, double &xMax) {box.ExpandX(object, xMin, xMax);}
    inline void ExpandY(spObject *object, double &yMin, double &yMax,double xmin=1,double xmax=0) {box.ExpandY(object, yMin, yMax, xmin, xmax);}
    
    virtual const char *GetClassName() {
        return _CCHAR("spObject");
    }
};

extern void SetContainer(spObject *object, spContainer *ctn);


class spCursorCrossHair : public spObject
{
	public :
    
	spCursorCrossHair();
	virtual void Draw(spRect &rect);
    
    virtual const char *GetClassName() {
		return _CCHAR("spCursorCrossHair");
	}
};


#endif
