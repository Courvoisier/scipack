
#ifndef _SP_INTERACTOR_ZOOM_H
#define _SP_INTERACTOR_ZOOM_H


class spView;
//
// A 3-button Zoom interactor to be attached to a view
//
class spInteractorZoom3View : public spInteractorN {
	
	public : 
	
  virtual bool OnlyOnViews() { return true;};

//	virtual const char *DefaultTitle() {return "Zoom Left/right";}

	spInteractorZoom3View(const char *name=NULL);
	
	spInteractor1 *left,*unzoom,*right;
	
	inline void SetLeft(spEventSimpleType type=spOnDownLeft, unsigned long modifiers=0) {left->SetEventType(type,modifiers);};
	inline void SetRight(spEventSimpleType type=spOnDownLeft, unsigned long modifiers=0) {right->SetEventType(type,modifiers);};
	inline void SetUnzoom(spEventSimpleType type=spOnKeyDown, unsigned long key=' ') {unzoom->SetEventType(type,key);};	
};


//
// Zoom around the mouse using the wheel
//
class spInteractorZoomWheelView : public spInteractorN {
	
	public : 
	
  virtual bool OnlyOnViews() { return true;};

//	virtual const char *DefaultTitle() {return "Zoom Wheel";}

	spInteractorZoomWheelView(const char *name=NULL);
	spInteractor1 *mousex, *mousey, *mousexy;
	
	inline void SetMouseX(spEventSimpleType type=spOnMouseWheel, unsigned long modifiers=SP_MOD_SHIFT) {mousex->SetEventType(type,modifiers);};
	inline void SetMouseY(spEventSimpleType type=spOnMouseWheel, unsigned long modifiers=SP_MOD_ALT) {mousey->SetEventType(type,modifiers);};
	inline void SetMouseXY(spEventSimpleType type=spOnMouseWheel, unsigned long modifiers=SP_MOD_SHIFT || SP_MOD_ALT) {mousexy->SetEventType(type,modifiers);};	
};


//
// Zoom Using selection
//
class spInteractorZoomSelectView : public spInteractorSelectView {
	
	public : 
	
//	virtual const char *DefaultTitle() {if (flagYConstrained) return "Zoom Select-Y"; if (flagXConstrained) return "Zoom Select-X"; return "Zoom Select-XY";}

	spInteractor1 *unzoom;
	spInteractorZoomSelectView(char constrained = ' ', bool flagPropagate=false,const char *name=NULL);
	
	inline void SetUnzoom(spEventSimpleType type, unsigned long key) {unzoom->SetEventType(type,key);};	
	inline void SetZoom(spEventSimpleType type, unsigned long modifiers) {SetSelect(type,modifiers);};
};


//
// Zoom Synchronization
//

typedef enum {
	spSynchroZoomX = 0,
	spSynchroZoomXY
} spSynchroZoomMode;

class spInteractorSynchroZoom : public spInteractorN
{
  public :
	
  virtual bool OnlyOnViews() { return true;};

	spObjectSet set;
	
	spSynchroZoomMode mode;
	
	void AddView(spView *view);
	
	spInteractorSynchroZoom(spSynchroZoomMode mode = spSynchroZoomX, spView *view=NULL,const char *name=NULL);
	~spInteractorSynchroZoom();
	
};



#endif