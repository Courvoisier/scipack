//
//  comment.cpp
//  sp
//
//  Created by bacry on 25/01/13.
//
//

#include "sp.h"


static spAnchor getAnchorFunc(spObject *o,double xmin,double xmax)
{
    if (!o->container) return spAnchor();
    
    spComment *comment = (spComment *) o->container;
    spShape *line = comment->line;
    
    return spAnchor(comment->l2top.IApply(comment->l2top.Apply(line->GetAnchor()) + comment->dist));
}


spComment::spComment(spContainer *ct,spRealPoint pt, const char *str, unsigned long flags,const char *name) : spGroup(ct,flags,name)
{
    line = new spShape(this,spShapeArrow,pt,spMargin(1,1));
    line->RefAndConfig();
    
    text = new spText(this,spAnchorFunc(getAnchorFunc));
    text->SetText(str);
    text->SetPixelTranslation();
    text->SetFrameMargin(spRealPoint(6,3));
    text->SetHVMode(spJustifyHMiddle,spJustifyVMiddle);
    text->RefAndConfig();
    
    anchor = pt;
    dist = spRealPoint(30,-30);
    
    FixObject(line);
    Lock();
}


void spComment::SetAnchor(spRealPoint a)
{
    AttBeforeUpdate();
    anchor = a;
    UnLock();
    line->SetAnchor(anchor);
    Lock();
    AttAfterUpdate();    
}

void spComment::Move(double dx, double dy)
{
    AttBeforeUpdate();
    anchor += spRealPoint(dx,dy);
    UnLock();
    line->SetAnchor(anchor);
    Lock();
    AttAfterUpdate();
}


spComment::~spComment()
{
    text->Unref();
    line->Unref();
}

void spComment::NotifyObjectGeometryUpdate(spObject *object)
{
    spGroup::NotifyObjectGeometryUpdate(object);

    spRealRect r = text->lRect();
    
    spRealPoint p = line->GetAnchor();
    
    spRealPoint a = r.GetClosestCorner(p,true);
 
    
    line->SetAnchorOposite(a);
}
