

#include "sp.h"


//**********************************************************
//
// A generic interactor to select a rectangle (stored in rectSelection)
// 
//    - A call back function is called when region was selected
//    - Pen and brush can be set
//    - Automatic scrolling
//
//**********************************************************



// 
// Button is moved or is outside the view
//
void CBSelectMotionView(spInteractor *cb, spEvent &event)
{
	spInteractorSelectView *i = (spInteractorSelectView *) cb;
	
	if (event.type == spOnTime && event.timer != event.timer) return;
	
	spView *view = ((spView *) (event.object));
	spRect bview = view->gRect();
	
	if (event.type == spOnTime && i->timer != event.timer) return;
	
	spRealPoint pt = event.mouse;
	if (i->flagYConstrained) pt.y = bview.y+bview.height-1;
	else if (i->flagXConstrained) pt.x = bview.x+bview.width-1;
	
	if (event.type != spOnTime && !bview.Contains(pt)) {
		if (!i->timer->IsRunning()) i->timer->Start(event.object,10);
    return;
	}

	if (event.type == spOnTime && bview.Contains(pt)) {
    i->timer->Stop();
		return;
	}
	
	if (event.type == spOnTime) {
	  spRealPoint p = view->l2top.IApply(pt);
		spRealRect r = view->GetXYBound();
		r.Union(p);
		view->SetXYBound(r);
	}
	
	spRealPoint m1 = view->l2top.IApply(pt);
	i->select->SetAnchorOposite(m1);
	i->UpdateSelection(view);

    SPUpdate(false);
}


//
// Button is unpressed
//
void CBSelectUpView(spInteractor *cb, spEvent &event)
{
	spInteractorSelectView *i = (spInteractorSelectView *) cb;
//	spView *view = ((spView *) (event.object));
	
/*	delete i->text;
	i->text = NULL;
*/
	
	i->select->DeactivateOverlay();
	i->select->Unref();
	i->select = NULL;

	if (i->timer != NULL) delete i->timer;
	i->timer = NULL;
	
	if (i->cbSelectUp.IsSet() && i->rectSelection.width != 0 && i->rectSelection.height != 0) i->cbSelectUp.Call(cb,event);
}


//
// Button is pressed
//
void CBSelectDownView(spInteractor *cb, spEvent &event)
{
	spInteractorSelectView *i = (spInteractorSelectView *) cb;
	spView *view = ((spView *) (event.object));
	spRect bview = view->gRect();
	
	// Empty the rectSelection
	i->rectSelection = spRealRect();
	
	spRealPoint pt = event.mouse;
	if (i->flagYConstrained) pt.y = bview.y;
	else if (i->flagXConstrained) pt.x = bview.x;
	i->refPoint = view->l2top.IApply(pt);
	
	i->select = new spShape(view,spShapeRect,spAnchor(i->refPoint),spMargin(0,0),spFLAG_OBJECT_OVERLAY_BUFFER);
	i->select->Ref();
	i->select->SetBrush(i->brush);
	i->select->SetPen(i->pen);
	i->select->ActivateOverlay();

/*	i->text = new spText(view,spBox(spRealRect(bview.x+bview.width/2,bview.y+bview.height/2,0,0),view->topview),spFLAG_OBJECT_OVERLAY_BUFFER);
	i->text->String(" ");
	i->text->SetFont(9);
  i->text->ActivateOverlay();
*/
	
	i->timer = new spTimer();
}


//
// Update the current rectSelection
//
void spInteractorSelectView::UpdateSelection(spView *view) 
{
	rectSelection = select->lRect();	
  rectSelection.Standardize();
  if (flagYConstrained) {
	  spRealRect bound = view->GetXYBound();
	  rectSelection.y = bound.y;
	  rectSelection.height = bound.height; 	
  }
  else if (flagXConstrained) {
	  spRealRect bound = view->GetXYBound();
	  rectSelection.x = bound.x; 	
	  rectSelection.width = bound.width; 	
  }

	/*
	char str[300];
sprintf(str,"xMin = %g  xMax = %g\nyMin = %g  yMax = %g",i->rectSelection.x,i->rectSelection.x+i->rectSelection.width,i->rectSelection.y,i->rectSelection.y+i->rectSelection.height);
i->text->String(str);
*/	
}

//
// The constructor
//
spInteractorSelectView::spInteractorSelectView(char constrained, bool flagPropagate1,spInteractorCallback cbSelectUp1,const char *name) :  spInteractorN(NULL,name)
{
	flagYConstrained = (constrained == 'y');
	flagXConstrained = (constrained == 'x');
	select = NULL;
	cbSelectUp = cbSelectUp1;
	//	text = NULL;
	timer = NULL;
	pen = *wxBLACK_PEN;
	SetBrush(wxColour(0,50,0,30));
	Add(up=new spInteractor1(CBSelectUpView,spOnUpLeft,0,flagPropagate1));
	Add(down=new spInteractor1(CBSelectDownView,spOnDownLeft,0,false));
	Add(drag=new spInteractor1(CBSelectMotionView,spOnDragLeft,0,flagPropagate1));
	Add(time=new spInteractor1(CBSelectMotionView,spOnTime,0,flagPropagate1));	
}


//**********************************************************
//
// A generic interactor to display an object (overlay) for a certain time
// The object is either displayed at the center of the view or at the mouse position (in which case a translation can be applied) or
// at the position of the object itself
//
//**********************************************************


void CBInteractorTempObjectErase(spInteractor *cb, spEvent &event)
{
	spInteractorTempObject *i = (spInteractorTempObject *) cb;
	
	if (i->timer != event.timer) {
		return;
	}
	
	SetContainer(i->obj,NULL);

	if (i->timer) i->timer->Stop();
}

void CBInteractorTempObjectDisplay(spInteractor *cb, spEvent &event)
{
	spInteractorTempObject *i = (spInteractorTempObject *) cb;
	spContainer *container = (spContainer *) event.object;
	spRect bview = container->gRect();
	
	// We first need to erase the object if needed
	CBInteractorTempObjectErase(i,event);
	
  // Then we set the view of the object and the box.ct
  SetContainer(i->obj,container->topContainer);
	
	// We activate the overlay
	i->obj->ActivateOverlay();
	
	// We set the anchor
	switch(i->objPosition) {
		case spTempObjectPositionMouse:
//			i->obj->SetAnchorContainer(i->obj->container->topContainer);
			i->obj->SetAnchor(spRealPoint(event.mouse+i->translation));
			break;
		case spTempObjectPositionCenter : 
//			i->obj->SetAnchorContainer(i->obj->container->topContainer);
			i->obj->SetAnchor(spRealPoint(bview.x+bview.width/2,bview.y+bview.height/2)+spRealPoint(i->translation));
			break;
		case spTempObjectPositionUnchanged:
			break;
	}
	
//	i->obj->NotifyRectMustBeDrawn(container->clip);

	// Run the timer
	if (i->timer != NULL) {
		delete i->timer;
	}
	i->timer = new spTimer();
	i->timer->Start(event.object,i->delay,wxTIMER_ONE_SHOT);
	
	// And call the cbAction
	if (i->cbAction.IsSet()) i->cbAction.Call(i,event);
}

void spInteractorTempObject::NotifyObjectDeleted(spObject *object)
{
	// If the view in which the temp obj is currently displayed is deleted then we have to erase the temp object
	if (obj->container == object) {
		spEvent event; // A fake event
		CBInteractorTempObjectErase(this,event);
	} 
}

void spInteractorTempObject::SetPosition(spTempObjectPosition pos,spPoint translation1)
{
	objPosition = pos;
	translation = translation1;	
}

spInteractorTempObject::~spInteractorTempObject()
{
	obj->Unref();
}

spInteractorTempObject::spInteractorTempObject(spObject *object1, unsigned long  key, bool flagPropagate1,spInteractorCallback cbAction1,const char *name) :  spInteractorN(NULL,name)
{
	obj = object1;	
	obj->Ref();
	
  SetPosition(spTempObjectPositionCenter);
	
	cbAction = cbAction1;
	timer = NULL;
  delay = 600;
	
	Add(display=new spInteractor1(CBInteractorTempObjectDisplay,spOnKeyDown,key,flagPropagate1));
	Add(new spInteractor1(CBInteractorTempObjectErase,spOnTime,0,flagPropagate1));
}


//**********************************************************
//
// A generic interactor to select between different interactors using a key
//
//**********************************************************


void cbSelector(spInteractor *cb, spEvent &event)
{
	spInteractorSelector *i = (spInteractorSelector *) cb;
	spText *text = (spText *) i->obj;
	
	if (i->currentInteractor== -1) return;
	i->interactorList[i->currentInteractor]->Off();
	i->currentInteractor+=1;
	if (i->currentInteractor==i->interactorList.Size()) i->currentInteractor = i->firstInteractor;
	i->interactorList[i->currentInteractor]->On();	
	text->SetText(i->interactorList[i->currentInteractor]->GetName());
}

spInteractorSelector::spInteractorSelector(unsigned long  key, bool flagPropagate,const char *name) : spInteractorTempObject(new spText(NULL,spAnchor(),spJustifyHMiddle,spJustifyVMiddle,spFLAG_OBJECT_OVERLAY_BUFFER),key,flagPropagate,cbSelector,name)
{
	obj->SetFont(12,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD);
	((spText *) obj)->SetFrameMargin();
	obj->SetBrush(wxColour(200,200,200));
	
	currentInteractor = -1;	
	firstInteractor = interactorList.Size();
}

void spInteractorSelector::Add(spInteractor * inter)
{
	spInteractorN::Add(inter);
	if (currentInteractor == -1) {
		currentInteractor = firstInteractor;
		inter->On();
	}
	else {
		inter->Off();
	}
}
