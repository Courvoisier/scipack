/*
 *  spstring.h
 *  SciPlot
 *
 *  Created by bacry on 03/07/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

/********************************
 *
 * Managing Fonts and strings
 *
 ********************************/

/*
 * the Font structure 
 */

#include "wx/fontdlg.h"
#include "wx/fontenum.h"
#include "wx/fontmap.h"


/* Mode for drawing strings */
enum {
  spJustifyHLeft = 0,
  spJustifyHRight,
  spJustifyHBRight,
  spJustifyHMiddle,
  spJustifyHBMiddle,
  spJustifyVUp,
  spJustifyVDown,
  spJustifyVBase,
  spJustifyVMiddle,
  spJustifyVMiddleUp
};


class spCanvas;

#define SP_MAX_NLINES_FORMATTEDTEXT 100
class spFormattedText {
	
	public :
	
	char *str;
	
	int nLines;
	char *lines[SP_MAX_NLINES_FORMATTEDTEXT];
	double xPos[SP_MAX_NLINES_FORMATTEDTEXT];
	double yPos[SP_MAX_NLINES_FORMATTEDTEXT];
	double angle;

    spRealPoint anchorPoint;
    void SetAnchorPoint(spRealPoint pt) {anchorPoint = pt;}
    spRealPoint GetAnchorPoint() {return anchorPoint;}
    bool flagFormattedWhilePrinting;
    
    void Format(spCanvas *canvas);
    
	int hMode,vMode;
	void SetHVMode(int hmode1,int vmode1);

	spRect boundingRect;

	spFormattedText();
	~spFormattedText();
	void SetText(const char *str);
	char *GetText();
	void SetAngle(double angle);
	void Draw(spCanvas *canvas);
};



