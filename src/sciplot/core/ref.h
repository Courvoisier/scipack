/*
 *  ref.h
 *  sp
 *
 *  Created by bacry on 05/12/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef _SPREF_
#define _SPREF_


#include <stdio.h>
#include <string.h>

#include "list.hpp"

#include "scipackdefs.h"

#define SPDEBUG_ALLOC 1
#define SPDEBUG_ALLOC_NCLASSES 300


class SPEXPORT spRef
{
    private :
    
	int _nRefs;     // The reference counter

#ifdef SP_SCRIPT
    void *scriptobj;
    bool IsReferencedByScript() {return scriptobj != NULL;}
    bool IsReferencedByC() {return _nRefs != 0;}
#endif
    
	int AddRef();   // Private method to add or delete a reference
    int DelRef();
    
	char *name; // The name/title of the object
    
public:
    
    // Constructor/Destructor
	spRef(bool flagRestrictedName, const char *name1=NULL);
    virtual ~spRef();
    
    virtual bool IsInteractor() = 0;
    
    // Get the class name
    virtual const char *GetClassName() {return const_cast<char *>("UnkownClass");};

    // Get default name from class name
    static const char *GetDefaultName(const char *klassName);

    // Get/Set the name
    const char *GetName();
    void SetName(const char *name1);
    void _SetName(const char *name1,bool flagRestrictedName);
    
    // Public method for adding/deleting a ref and getting the counter
    int Ref(bool flag=false);
    int Unref(bool flag=false);
	int nRefs();
    void RefAndConfig();
    
#ifdef SPDEBUG_ALLOC
private:
    static spList<spRef,2,10> allocList;
public:
    static void InfoAllocClass();
    static void InfoAllocObjects();
#endif
    
};

extern void InfoAllocClass();
extern void InfoAllocObjects();


#endif
