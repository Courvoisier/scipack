/*
 *  sputils.cpp
 *  SciPlot
 *
 *  Created by bacry on 22/06/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#include "sp.h"



spRealPoint spPoint::operator / (spRealPoint pt2) {
    spRealPoint res(x/((double) pt2.x),y/((double) pt2.y));
    return res;
}


ostream& operator << (ostream& o, spRealRect &r)
{
	o << "RRect(" << r.x << "," << r.y << "," << r.width << "," << r.height << ")";
	return o;
}

ostream& operator << (ostream& o, spRect &r)
{
	o << "spRect(" << r.x << "," << r.y << "," << r.width << "," << r.height << ")";
	return o;
}

ostream& operator << (ostream& o, spMap1d &t)
{
	o << "Transf1(a=" << t.a << ",b=" << t.b << "," << ")";
	return o;
}

ostream& operator << (ostream& o, spMap2d &t)
{
	o << "TransfXY(" << t.trX << "," << t.trY << ")";
	return o;
}


// Increase by dx,dy in all directions
spRealRect & spRealRect::Inflate(double dx, double dy)
{
	Standardize();
	x-=dx;
	y-=dy;
	width += dx;
	height += dy;
	width=MAX(width,0);
	height=MAX(height,0);
    return *this;
}

spRealRect & spRealRect::Intersect(const spRealRect& rect)
{
	int xx,yy,ww,hh;
  
  xx = MAX(rect.x,x);
  yy = MAX(rect.y,y);
  ww = MIN(x+width,rect.x+rect.width)-xx;
  hh = MIN(y+height,rect.y+rect.height)-yy;
	
  if (hh <=0 || ww <= 0) {
    width = height = 0;
    return(*this);
  }
  
  x = xx;
  y = yy;
  width = ww;
  height = hh;
  
  return(*this);  
}

bool spRealRect::Intersects(const spRealRect& rect) const
{
	spRealRect r = Intersect(rect);
	return r.IsEmpty();
}

spRealRect& spRealRect::Union(const spRealRect& rect)
{
  double xx,yy,ww,hh,Yy,Xx;
	
  xx = MIN(rect.x,x);
	xx = MIN(xx,rect.x+rect.width);
	xx = MIN(xx,x+width);
	
  yy = MIN(rect.y,y);
  yy = MIN(yy,rect.y+rect.height);
  yy = MIN(yy,y+height);
	
	Xx = MAX(rect.x,x);
	Xx = MAX(Xx,rect.x+rect.width);
	Xx = MAX(Xx,x+width);
	
  Yy = MAX(rect.y,y);
  Yy = MAX(Yy,rect.y+rect.height);
  Yy = MAX(Yy,y+height);
	
  ww = Xx-xx;
  hh = Yy-yy;
	
  x = xx;
  y = yy;
  width = ww;
  height = hh;
	
	
  return(*this);  
}

// Increase by dx,dy in all directions
spRect & spRect::Inflate(int dx, int dy)
{
	wxRect r(x,y,width,height);
	r.Inflate(dx,dy);
	*this = r;
	return *this;
}

spRect& spRect::Intersect(const spRect& rect)
{
	wxRect r(x,y,width,height);
	wxRect r1(rect.x,rect.y,rect.width,rect.height);
	r.Intersect(r1);
	*this = r;
	return *this;
}

spRect& spRect::Union(const spRect& rect)
{
	wxRect r(x,y,width,height);
	wxRect r1(rect.x,rect.y,rect.width,rect.height);
	r.Union(r1);
	*this = r;
	return *this;
}

bool spRect::Contains(double x1, double y1) const
{
	wxRect r(x,y,width,height);
	return r.Contains(x1,y1);
}

bool spRect::Intersects(const spRect& rect) const
{
	wxRect r(x,y,width,height);
	wxRect r1(rect.x,rect.y,rect.width,rect.height);
	return r.Intersects(r1);
}


spRealPoint spRealRect::GetClosestCorner(spRealPoint pt,bool flagIncludeMidPoints)
{
    spRealPoint corner,res;
    double d,d1;
    
    corner.x = x; corner.y = y;
    d = (pt.x-corner.x)*(pt.x-corner.x) + (pt.y-corner.y)*(pt.y-corner.y);
    res = corner;
    
    corner.x = x; corner.y = y+height;
    d1 = (pt.x-corner.x)*(pt.x-corner.x) + (pt.y-corner.y)*(pt.y-corner.y);
    if (d1 < d) {
        res = corner;
        d = d1;
    }
    
    corner.x = x+width; corner.y = y+height;
    d1 = (pt.x-corner.x)*(pt.x-corner.x) + (pt.y-corner.y)*(pt.y-corner.y);
    if (d1 < d) {
        res = corner;
        d = d1;
    }

    corner.x = x+width; corner.y = y;
    d1 = (pt.x-corner.x)*(pt.x-corner.x) + (pt.y-corner.y)*(pt.y-corner.y);
    if (d1 < d) {
        res = corner;
        d = d1;
    }
    
    if (!flagIncludeMidPoints) return res;

    corner.x = x+width/2; corner.y = y;
    d1 = (pt.x-corner.x)*(pt.x-corner.x) + (pt.y-corner.y)*(pt.y-corner.y);
    if (d1 < d) {
        res = corner;
        d = d1;
    }
    
    corner.x = x+width/2; corner.y = y+height;
    d1 = (pt.x-corner.x)*(pt.x-corner.x) + (pt.y-corner.y)*(pt.y-corner.y);
    if (d1 < d) {
        res = corner;
        d = d1;
    }

    corner.x = x; corner.y = y+height/2;
    d1 = (pt.x-corner.x)*(pt.x-corner.x) + (pt.y-corner.y)*(pt.y-corner.y);
    if (d1 < d) {
        res = corner;
        d = d1;
    }

    corner.x = x+width; corner.y = y+height/2;
    d1 = (pt.x-corner.x)*(pt.x-corner.x) + (pt.y-corner.y)*(pt.y-corner.y);
    if (d1 < d) {
        res = corner;
        d = d1;
    }

    return res;
}

spMap1d spMap1d::ComposeRight(const spMap1d &tr)
{
	spMap1d res;
	if (!flagLn) {
		res = tr;
		res.a *= a;
		res.b = a*tr.b+b;
	}
	else if (!tr.GetFlagLn()) {
		res = *this;
		res.c *= tr.a;
		res.d = d+c*tr.b;
	}
	else spWarningf("spMap1d::ComposeRight()","Cannot compose two logarithm scales");
	return res;
}

spMap1d spMap1d::ComposeLeft(const spMap1d &tr) 
{
	spMap1d res;
	if (!tr.GetFlagLn()) {
		res = *this;
		res.a *= tr.a;
		res.b = tr.a*b+tr.b;
	}
	else if (!flagLn) {
		res = tr;
		res.c *= a;
		res.d = tr.d+tr.c*b;
	}
	else spWarningf("spMap1d::ComposeLeft()","Cannot compose two logarithm scales");
	return res;	
}


spMap1d spMap1d::operator+(const double x) 
{
	spMap1d tr = *this;
	tr.b+=x;
	return tr;
}


spRect spMap2d::ApplyInt(const spRealRect &r) {
	spRealPoint pp = r.GetPosition1();
	spRealPoint pt1 = Apply(r.GetPosition1());
	spRealPoint pt = Apply(r.GetPosition());
	spRealRect rr(pt,pt1);		
	return rr.Round();
}

spPoint spMap2d::ApplyInt(const spRealPoint &pt) {
	spRealPoint pt1 = Apply(pt);
	return spPoint((int) pt1.x,(int) pt1.y);
}

