/*
 *  topview.h
 *  SciPlot
 *
 *  Created by bacry on 09/08/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef _SP_TopContainer_H_
#define _SP_TopContainer_H_




class spTopContainer : public spGrid
{
	public :
	
	//
	// Basic class Tests
	//
	virtual bool IsTopContainer() {return true;};

	//
	// The associated canvas
	//
	spCanvas *canvas;
	
	//
	// Constructor/Destructor
	//
	spTopContainer(spCanvas *canvas1,const char *name=NULL);
	virtual ~spTopContainer();
	void SetCanvas(spCanvas *canvas1) {
        if (canvas) return;
        canvas = canvas1;
    }
    
    void Print(bool flagPageSetupDialog=true, bool flagPrintDialog = true, int units_per_cm=200) {
        if (canvas) canvas->Print(flagPageSetupDialog,  flagPrintDialog, units_per_cm);
    }

    void ToPS(char *filename,int x_margin_cm=0,int y_margin_cm=0) {
        if (canvas) canvas->ToPS(filename,x_margin_cm,y_margin_cm);
    }

    void ToSVG(char *filename) {
        if (canvas) canvas->ToPS(filename);
    }

	//
	// Dealing Resize.
	// The flaResizeEvent is true whenever we are dealing with a resize event
	// (this is used to keep the same xyBound if flag spFLAG_VIEW_KEEPBOUND_WHEN_RESIZED is set)
	//
	void OnResize();	
	bool flagResizeEvent;
	bool ResizeEvent() {
		return flagResizeEvent;
	};
	
	//
	// Called for a paint event
	//
	virtual void OnPaint(spRect &rect);

	//
	// Called for a leave event
	//
	void OnMouseLeave();
	void OnMouseEnter();
	
	// 
	// Redefining the UpdateGeometry method
	//
	virtual void UpdateGeometry();

  //
	// Main process event procedure
	//
	virtual bool ProcessEvent(spEvent &event);	

	// 
	// Dealing with overlay objects
	//
	spList<spObject,2,10> overlayObjectList;
	void AddOverlayObject(spObject *object);
	void RemoveOverlayObject(spObject *object); // !! This method unref the object so it can delete it

	void EraseOverlayObjects();
	void DrawOverlayObjects();
	void NotifyViewDeleted2OverlayObjects(spContainer *v);
	
	void Test();
	
	virtual const char *GetClassName() {
		return _CCHAR("spTopContainer");
	}
	
};

#endif
