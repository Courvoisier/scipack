/*
 *  interactor_move.h
 *  LastPlot
 *
 *  Created by bacry on 04/10/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef _SP_INTERACTOR_MOVE_H
#define _SP_INTERACTOR_MOVE_H

//
// A move interactor to be attached to a view
//
// This interactor can be shared
//
class spInteractorMoveView : public spInteractorN {
	
	public : 

    virtual bool OnlyOnContainers() { return true;};

	static spInteractorMoveView * Get(); 
	static spPoint curMousePt;
	
	spObject *objectToMove;
    spTimer *timer;
	
	spInteractor1 *down,*up,*drag,*time;
	
	inline void SetMove(spEventSimpleType type, unsigned long modifiers=0);
    
	spInteractorMoveView(bool flagPropagate=false,const char *name=NULL);
	virtual ~spInteractorMoveView();
};


#endif

