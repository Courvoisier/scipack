
#ifndef _SP_INTERACTOR_SELECT_H
#define _SP_INTERACTOR__SELECT_H

class spView;

//
// An interactor to select a rectangle into a view
//
class spText;
class spInteractorSelectView : public spInteractorN {

	protected : 
	
	spInteractor1 *down,*up,*drag,*time;
	
	public : 
	
  virtual bool OnlyOnViews() { return true;};

	spObject *select;
	spText *text;
  spTimer *timer;
	spInteractorCallback cbSelectUp;
	
	spRealPoint refPoint;

	bool flagYConstrained;
	inline void SetYConstained(){
		flagYConstrained = true;
	}
	bool flagXConstrained;
	inline void SetXConstained(){
		flagXConstrained = true;
	}
	
	spRealRect rectSelection;
	inline spRealRect GetSelection() {
		return rectSelection;
	}
	void UpdateSelection(spView *view);
	
	SP_PEN_FIELD(;,;);
	SP_BRUSH_FIELD();
		
	spInteractorSelectView(char constrained, bool flagPropagate, spInteractorCallback cbSelectup1=NULL,const char *name = NULL);
	virtual ~spInteractorSelectView(){};

	inline void SetSelect(spEventSimpleType type, unsigned long modifiers=0) {
		if (type == spOnDownLeft) {
			down->SetEventType(type,modifiers);
			up->SetEventType(spOnUpLeft,modifiers);
			drag->SetEventType(spOnDragLeft,modifiers);
		}
		else if (type == spOnDownMiddle) {
			down->SetEventType(type,modifiers);
			up->SetEventType(spOnUpMiddle,modifiers);
			drag->SetEventType(spOnDragMiddle,modifiers);			
		}
		else if (type == spOnDownRight) {
			down->SetEventType(type,modifiers);
			up->SetEventType(spOnUpRight,modifiers);
			drag->SetEventType(spOnDragRight,modifiers);			
		}
	};
	
	virtual const char *GetClassName() {
		return _CCHAR("spInteractorSelectView");
	}	
};


//
// Interactor which displays an object for a given time
//
typedef enum {
	spTempObjectPositionMouse = 0,
	spTempObjectPositionCenter,
	spTempObjectPositionUnchanged
} spTempObjectPosition;

class spInteractorTempObject : public spInteractorN {
	
	public : 
	
  virtual bool OnlyOnContainers() { return true;};

	spInteractor1 *display;
	int delay;
	void SetDelay(int delay1) {delay = delay1;}
	spTimer *timer;
	spObject *obj;        // The overlay object used with the cursor
	spPoint translation;
	spInteractorCallback cbAction;
	spInteractorTempObject(spObject *object, unsigned long  key, bool flagPropagate=true,spInteractorCallback cbAction = NULL,const char *name = NULL);
	~spInteractorTempObject();
	spTempObjectPosition objPosition;
	void SetPosition(spTempObjectPosition pos,spPoint translation = spPoint(0,0));
	
	inline void SetDisplay(spEventSimpleType type, unsigned long key) {display->SetEventType(type,key);};
	virtual void NotifyObjectDeleted(spObject *object);
	
	virtual const char *GetClassName() {
		return _CCHAR("spInteractorTempObject");
	}	
};


//
// A selector Interactor which selects betwen different interactors by using a key which displays a temp object
//

class spInteractorSelector : public spInteractorTempObject {	
	public :
	
	spInteractorSelector(unsigned long  key, bool flagPropagate=true,const char *name = NULL);
  int currentInteractor;
  int firstInteractor;
	virtual void Add(spInteractor *);

	virtual const char *GetClassName() {
		return _CCHAR("spInteractorSelector");		
	}	
};


#endif