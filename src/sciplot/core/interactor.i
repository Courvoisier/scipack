
// Not a module

%rename(Interactor) spInteractor;

%feature("ref")   spInteractor "$this->Ref(true);"
%feature("unref") spInteractor "$this->Unref(true);"



%feature("director") spInteractor;

// We set the kwargs for some methods
%feature("kwargs") spInteractor::spInteractor;

%DocClass(spInteractor,"Abstract base class for all interactors. The main two classes of interactors derive form his class : the sp.Interactor class and the sp.InteractorN class. Any new interactor class should deriver from one of these classes.")
class spInteractor
{
	public :

    // The string representation in python
    ObjRepr;

    // A Python method to get the full name of the object
    %extend {
        %pythoncode %{
            def fullname(self):
                """
                Same as self.name
                """
                return self.name
        %}
    }

    // A method to get the class name
    // It works for both python defined objects and C++ defined objects
    %extend {
      const char *__spclassname__() {
        Swig::Director *director = SWIG_DIRECTOR_CAST($self);
        const char *klass;
        if (director) {
            PyObject *obj = (PyObject *) director->swig_get_self();
            klass = obj->ob_type->tp_name;
        }
        else klass = self->GetClassName();
        return klass;
      }
    }

    // A method to add the interactor to a interactorN
    %extend {
        %pythoncode %{
            def AddTo(self,interactorN):
                """ 
                Add the interactor to the interactorN
                """
                interactorN.Add(self)
                return self
        %}
    }

    //
    // Dealing with attributes
    // (the _att_... commands are defined in the file _sputils.py)
    //
    //
    // Dealing with attributes
    // (the _att_... commands are defined in the file _sputils.py)
    //
    %extend {
    %pythoncode %{
    def __call__(self,get_attribute=None,update=False,**kwargs):
        """Allows to set/get attribute values of the interactor
        This function is designed so that interaction on the terminal is easy.
        --> If get_attribute is specified
        returns the value of the attribute of the interactor
        --> Otherwise, kwargs corresponds to all the attributes to be set"""
                
        if (len(kwargs)==0):
            if (get_attribute is None):
                print "Attributes of %s" % self.__repr__()
                print _att_list(self)
                return self
            else:
                return _att_get(self,get_attribute)
        else:
            return _att_set(self,False,**kwargs)
            
    def ahelp(self,attribute=None,flag_print=True,flag_short=True):
        """
        Allows to get/print help on all the attributes or a single attribute of the interactor
                    
            attribute  : if not None then just ask the help on the specified attribute
            flag_print : if True, then the help is printed (and not 'just' returned)
            flag_short : if True, a short version of the help is asked for
        """
        return _att_help(self,attribute,flag_print,flag_short)
                                
    def alist(self):
        """Allows to get the list of the attributes of the interactor"""
        return _att_list(self)
                                
            
    def config(self,*args,**kwargs):
        """Config the interactor using the .spconfig file. Moreover you can specify some attributes values using the same syntax as __call__()"""
        
        Config.config(self)
        if (len(kwargs) != 0): self(*args,**kwargs)
        return self
    %}
    }
    
    
    %extend {
        bool _IsInteractor() {return true;}
    }

    %DocMeth(IsOn,"IsOn() -> bool","Returns True if interactor is active")
	inline bool IsOn();
    %DocMeth(On,"On()","Turns the interactor on")
	inline void On();
    %DocMeth(Off,"Off()","Turns the interactor off")
	inline void Off();

    %DocMeth(CheckObjectIsValid,"CheckObjectIsValid(object) -> bool","Indicates whether the interactor can be added to the object (This method should be overloaded if needed and should never be called directly).")
	virtual bool CheckObjectIsValid(spObject *);
/*    virtual bool OnlyOnViews();
    virtual bool OnlyOnContainers();
 */
    
    %DocMeth(NotifyObjectAdded,"NotifyObjectAdded(object)","This method is called whenever the interactor is added to an object  (This method should be overloaded).")
	virtual void NotifyObjectAdded(spObject *o);
    %DocMeth(NotifyObjectRemoved,"NotifyObjectRemoved(object)","This method is called whenever the interactor is removed from an object  (This method should be overloaded).")
	virtual void NotifyObjectRemoved(spObject *o);
    %DocMeth(NotifyObjectDeleted,"NotifyObjectDeleted(object)","This method is called whenever an object whose is using the interactor is deleted  (This method should be overloaded).")
	virtual void NotifyObjectDeleted(spObject *o);

    %DocMeth(IsInteractorN,"IsInteractorN() -> bool","Returns True if interactor is an instance of sp.InteractorN")
    virtual bool IsInteractorN();

	// Basic constructor
	spInteractor(spObject *o=NULL,const char *name = NULL);
	virtual ~spInteractor();	
};

//
// nrefs field (read only)
//
%extend spInteractor {
%immutable;
    int nrefs;
%mutable;
}
%{
    int spInteractor_nrefs_get(spInteractor *s) {return s->nRefs();}
%}


//
// name field (read/write)
//
%extend spInteractor {
  char *name;
}
%{
  const char * spInteractor_name_get(spInteractor *s) {return s->GetName();}
  void spInteractor_name_set(spInteractor *s,char *str) {s->SetName(str);}
%}



%{
	//
    // This function calls the interactor callback
	//
	void callbackScriptFunc(spInteractorCallback *icb, spInteractor *i,spEvent &e)
	{
//        if (e.type == spOnChangeBound) printf("Script changebound Callback\n");
		// Get the python callback
		PyObject *callback = (PyObject *) icb->scriptFunc;
		if (callback == NULL) return; // should never happen

		// Get the proxy instance associated to object (it adds a reference)
		PyObject *self = WrapObject(i);
		if (self == NULL) return; // Should never happen

		// Make the python call, get the result, manage eventual errorin the function
        // ?? I do not need to make a new copy of the event here ... I could just suppress the SWIG_POINTER_OWN
		spEvent *e1 = new spEvent(e);
		PyObject *eventPy = SWIG_NewPointerObj(SWIG_as_voidptr(e1),SWIGTYPE_p_spEvent, SWIG_POINTER_OWN | 0 );
		PyObject *arglist = Py_BuildValue("OO", self,eventPy);
		PyObject* result = PyObject_CallObject(callback, arglist);

        e.xMin = e1->xMin;
        e.xMax = e1->xMax;
        e.yMin = e1->yMin;
        e.yMax = e1->yMax;

		Py_XDECREF(arglist);

        // So now we have to delete the reference we created calling WrapObject
        Py_XDECREF(self); // If not a director, it will Unref automatically and the object is going to be destroyed !
		Py_XDECREF(eventPy);

		if (result == NULL) {
			PrintPyError();
			return;
		}

		Py_XDECREF(result);
	};
    
    
%}

//
// The constuctor for the callback
//
extern void callbackScriptFunc(spInteractorCallback *self,spInteractor *i,spEvent &e);
%typemap(in)  spInteractorCallback {
    // Check that the the argument is a callable and set it in self->anchorFuncPtr.scriptFunc
    if (!PyCallable_Check($input)) {
        PyErr_SetString(PyExc_RuntimeError,"Argument should be a callable");
        return(NULL);
    }
    Py_XINCREF($input);
    $1.SetScriptFunc($input);

    // Then set the getFunc
	$1.SetGetFunc(callbackScriptFunc);
}



%rename(_GetEventType) GetEventType;
%rename(_SetEventType) SetEventType;

%feature("director") spInteractor1;

%rename(Interactor1) spInteractor1;

// We set the kwargs for some methods
%feature("kwargs") spInteractor1::spInteractor1;

%DocClass(spInteractor1,"The base class for all 'simple' interactors, i.e., interactors that just execute a callback when a single event is processed. The callback spInteractorCallback must be a callable of the form
- callable(interactor,event)
The flagPropagate in the constructor allows to specify whether, when an event is processed, the event can also be processed by the interactors of a container in the hierarchy of the object.")
class spInteractor1 : public spInteractor {
	
	public :


    // The string representation in python
    ObjRepr;

	// The constructor
	spInteractor1(spInteractorCallback cb,spEventType eventType, spObject *object = NULL, bool flagPropagate=false,const char *name = NULL);
	void SetCallback(spInteractorCallback cb);
	
	virtual ~spInteractor1();

    
    spEventType GetEventType();
    void SetEventType(spEventType eventType);
    %attint_def(eventType,EventType,
    """**Attribute** self.eventType = sp.EventType
    
    The eventType associated to the interactor""");
};


//
// eventType field (read/write)
//
/*%extend spInteractor1 {
  spEventType eventType;
}
%{
  spEventType *spInteractor1_eventType_get(spInteractor1 *s) {return new spEventType(s->GetEventType());}
  void spInteractor1_eventType_set(spInteractor1 *s,spEventType *et) {s->SetEventType(*et);}
%}
 */



%rename(InteractorN) spInteractorN;

%feature("director") spInteractorN;

// We set the kwargs for some methods
%feature("kwargs") spInteractorN::spInteractorN;

%DocClass(spInteractorN,"The base class for all 'complex' interactors, i.e., interactors that are defined by a set of sp.Interactor (which can be simple or complex). Let us note that in this case the interactor that is passed to the callback of the simple interactors is the complex interactor and not the corresponding simple interactor. That allows to store at the level of the complex interactor some useful fields for dealing with the consistency of the complex interactor")
class spInteractorN : public spInteractor {
	
	public :
	
    // The string representation in python
    ObjRepr;

	// Constructor
	spInteractorN(spObject *o=NULL,const char *name = NULL);

	virtual ~spInteractorN();
	
	// Method to add an spInteractor1 in the list
    %DocMeth(Add,"Add(sp.Interactor)","Adds an interactor")
	virtual void Add(spInteractor *interactor);
    
    virtual bool IsInteractorN();
};




//
// interactors Read-Only field
//
%extend spInteractorN {
    %DocMeth(interactors_,"FAKE METHOD just for indicating doc of self.interactors","Use field interactors for getting the list of the interactors inside the interactorN.")
    void interactors_() {};
}
%extend spInteractorN {
    %immutable;
    PyObject *interactors;
    %mutable;
}
%{
    PyObject *spInteractorN_interactors_get(spInteractorN *obj) {
        PyObject* list = PyList_New(obj->interactorList.Size());
        for (int i = 0; i < obj->interactorList.Size(); i++) {
            //        printf("%s\n",obj->interactorList[i]->GetClassName());
            PyObject *inter = WrapObject(obj->interactorList[i]);
            PyList_SET_ITEM(list, i, inter);
        }
        return list;
    }
    %}

//
// The ninteractors Read-only field
//
%extend spInteractorN {
    %DocMeth(ninteractors_,"FAKE METHOD just for indicating doc of self.ninteractors","Get the number of interactors in this interactorN.")
    void ninteractors_() {};
}
%extend spInteractorN {
    %immutable;
    int ninteractors;
    %mutable;
}
%{
    int spInteractorN_ninteractors_get(spInteractorN *obj) {return obj->interactorList.Size();}
    %}



//
// A simple Debug interactor that prints the received event
//

%rename(InteractorDebug) spInteractorDebug;

%feature("director") spInteractorDebug;

// We set the kwargs for some methods
%feature("kwargs") spInteractorDebug::spInteractorDebug;

%DocClass(spInteractorDebug,"A class for debugging events")
class spInteractorDebug : public spInteractor1 {
	
	public : 

    // The string representation in python
    ObjRepr;

	spInteractorDebug(spEventType eventType,bool flagPropagate=false,const char *name = NULL);
};



