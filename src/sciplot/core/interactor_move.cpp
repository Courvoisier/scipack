/*
 *  interactor_move.cpp
 *  LastPlot
 *
 *  Created by bacry on 04/10/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "sp.h"


spPoint spInteractorMoveView::curMousePt;


spInteractorMoveView * spInteractorMoveView::Get()
{
	static spInteractorMoveView * theInteractor = NULL;
	
	if (theInteractor == NULL) {
		theInteractor = new spInteractorMoveView;
		theInteractor->Ref();
	}
	return theInteractor;
}


//
// Moving the mouse outside the container
//
void CBMoveTime(spInteractor *cb, spEvent &event)
{
	spInteractorMoveView *i = (spInteractorMoveView *) cb;
	
    // CHeck this is the right timer
    if (i->timer != event.timer) return;
    
    // Get the view and its grect
	spView *view = (spView *) i->objectToMove->GetNonGroupContainer();
	spRect viewRect = view->gRect();
	
    
    // This is a maximum for the scrolling speed
	int max = 100;
    
    // If the mouse is not in the view we must scroll
	if (!viewRect.Contains(i->curMousePt)) {
        
        // First we compute the scrolling needed along X
        double sx = 0;
        if (i->curMousePt.x > viewRect.x+viewRect.width) sx = (i->curMousePt.x-viewRect.x-viewRect.width)/2;
        if (i->curMousePt.x < viewRect.x) sx = (i->curMousePt.x-viewRect.x)/2;
        sx = (sx>0 ? MIN(ceil(sx),max) : MAX(floor(sx),-max));
        
        double sy = 0;
        if (i->curMousePt.y > viewRect.y+viewRect.height) sy = (i->curMousePt.y-viewRect.y-viewRect.height)/2;
        if (i->curMousePt.y < viewRect.y) sy = (i->curMousePt.y-viewRect.y)/2;
        sy = (sy>0 ? MIN(ceil(sy),max) : MAX(floor(sy),-max));
        
		// We scroll the view
		view->ScrollX((int) -sx);
		view->ScrollY((int) -sy);
        
        // And move the object
		double x = sx/view->l2top.trX.GetFactor();
		double y = sy/view->l2top.trY.GetFactor();
		
		i->objectToMove->Move(x,y);
		
	}
    
    // Otherwise we jus stop the timer
	else i->timer->Stop();
}

//
// Moving the mouse
//
void CBMoveMotionView(spInteractor *cb, spEvent &event)
{
	spInteractorMoveView *i = (spInteractorMoveView *) cb;
    
    // Should never happen
	if (i->objectToMove == NULL) return;
	if (!i->objectToMove->IsMoveable()) return;
	
	// Compute the translation in the container's coordinate
	double x = (event.mouse.x-i->curMousePt.x)/i->objectToMove->container->l2top.trX.GetFactor();
	double y = (event.mouse.y-i->curMousePt.y)/i->objectToMove->container->l2top.trY.GetFactor();
	i->curMousePt = event.mouse;
    
    // Getting the grect of the container
	spRect containerRect = i->objectToMove->container->gRect();
	
	// If the mouse is outside the container AND if the container is a view
    // we start the timer (unless is it already running)
    // The timer will go off every 10ms
	if (!containerRect.Contains(i->curMousePt) && i->objectToMove->GetNonGroupContainer()->IsView()) {
		if (!i->timer->IsRunning()) i->timer->Start(event.object,10);
	}
    
    // Otherwise we juste move the object and stop the timer
	else {
        i->objectToMove->Move(x,y);
        i->timer->Stop();
    }
}


//
// Down mouse event
//
void CBMoveDownView(spInteractor *cb, spEvent &event)
{
	spInteractorMoveView *i = (spInteractorMoveView *) cb;
    
    // Find the object to move
	i->objectToMove = ((spView *) (event.object))->FindObjectMouse(event.mouse);
    
    // If none or if object is not moveable --> just return
	if (i->objectToMove == NULL || !i->objectToMove->IsMoveable()) {
		i->objectToMove = NULL;
		return;
	}
    
    // Otherwise we create a timer on the container
	i->timer = new spTimer();
    
    // And we remember the mouse position
	i->curMousePt = event.mouse;
}

void CBMoveUpView(spInteractor *cb, spEvent &event)
{
	spInteractorMoveView *i = (spInteractorMoveView *) cb;
	i->objectToMove = NULL;
	if (i->timer != NULL) delete i->timer;
	i->timer = NULL;
}


void spInteractorMoveView::SetMove(spEventSimpleType type, unsigned long modifiers)
{
    if (type == spOnDownLeft) {
        down->SetEventType(type,modifiers);
        up->SetEventType(spOnUpLeft,modifiers);
        drag->SetEventType(spOnDragLeft,modifiers);
    }
    else if (type == spOnDownMiddle) {
        down->SetEventType(type,modifiers);
        up->SetEventType(spOnUpMiddle,modifiers);
        drag->SetEventType(spOnDragMiddle,modifiers);
    }
    else if (type == spOnDownRight) {
        down->SetEventType(type,modifiers);
        up->SetEventType(spOnUpRight,modifiers);
        drag->SetEventType(spOnDragRight,modifiers);
    }
    spWarningf("spInteractorMoveView::SetMove()","Bad eventType, choose one of spOnDownLeft, spOnDownRight or spOnDownMiddle");
};


spInteractorMoveView::spInteractorMoveView(bool flagPropagate1,const char *name) :  spInteractorN(NULL,name)
{
	objectToMove = NULL;
	timer = NULL;
	Add(up=new spInteractor1(CBMoveUpView,spOnUpLeft,SP_MOD_ALT,flagPropagate1));
	Add(down=new spInteractor1(CBMoveDownView,spOnDownLeft,SP_MOD_ALT,false));
	Add(drag=new spInteractor1(CBMoveMotionView,spOnDragLeft,SP_MOD_ALT,flagPropagate1));
	Add(time=new spInteractor1(CBMoveTime,spOnTime,0,flagPropagate1));
}

spInteractorMoveView::~spInteractorMoveView()
{
    if (timer) delete timer;
}

