/*
 *  wxcanvas.cpp
 *  SciPlot
 *
 *  Created by bacry on 24/06/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */




#include "sp.h"



#define OPENCLIENTDC \
wxClientDC dc1(window); \
this->dc = &dc1; \
this->container->EraseOverlayObjects()

#define OPENPAINTDC \
wxPaintDC dc1(window); \
this->dc = &dc1; \
this->container->EraseOverlayObjects()

#define CLOSEDC	\
if (this->dc != NULL) this->container->DrawOverlayObjects(); \
this->dc = NULL


#define CATCH_CLOSEDC \
catch(const char *error) {\
if (error) spWarningf(NULL,"%s",error); \
ActivateDCBuffer(); \
dc = NULL; \
} \
catch (...) {\
spWarningf(NULL,"*** Unkown Error"); \
ActivateDCBuffer(); \
dc = NULL; \
}

#define CATCH \
catch(const char *error) {\
if (error) spWarningf(NULL,"%s",error); \
} \
catch (...) {\
spWarningf(NULL,"*** Unkown Error"); \
}


spList<spCanvas,10,10> spCanvas::canvasList;
spCanvas *spCanvas::curCanvas = NULL;



void SPUpdate(bool flagRedraw)
{
	for (int i=0;i<spCanvas::canvasList.Length();i++) {
		spCanvas::canvasList[i]->Update(flagRedraw);			
	}
}


void spCanvas::Ref()
{
    SPDEBUG(spDebugRef, "AddRef(Canvas(" << container->GetName() << ")) : " << _nRefs << " --> " << _nRefs+1);
    _nRefs++;
}
int spCanvas::Unref()
{
    SPDEBUG(spDebugRef, "DelRef(Canvas(" << container->GetName() << ")) : " << _nRefs << " --> " << _nRefs-1);
    if (_nRefs == 0 || --_nRefs == 0 ) {
        delete this;
        return 0;
    }
    return _nRefs;
}


void spCanvas::DesallocDCBuffer()
{
	if (dcBuffer != NULL) {
#ifndef spNOGC
		delete dcBuffer;
		memoryDC->SelectObject(wxNullBitmap);
		delete memoryDC;
#else
		dcBuffer->SelectObject(wxNullBitmap);
		delete dcBuffer;
#endif
	}	
}

void spCanvas::AllocDCBuffer(wxSize size)
{	
	DesallocDCBuffer();
	
	bitmap = wxBitmap(size.x,size.y,-1);
	wxMemoryDC *mdc = new wxMemoryDC;
	mdc->SelectObject(bitmap);
	mdc->SetBackground(*wxWHITE_BRUSH);
	mdc->Clear();				
#ifndef spNOGC
	memoryDC = mdc;
	dcBuffer = new wxGCDC(*memoryDC);
#else
	dcBuffer = mdc;
#endif
}

void spCanvas::DeactivateDCBuffer() 
{
	_dcBuffer = dcBuffer;
	dcBuffer = NULL;		
}

void spCanvas::ActivateDCBuffer() 
{
	if (dcBuffer == NULL) dcBuffer = _dcBuffer;		
}

void spCanvas::DCBufferToDC(spRect &r) 
{
	if (r.IsEmpty()) return;
#ifndef spNOGC
	dc->Blit(r.x,r.y,r.width,r.height,memoryDC,r.x,r.y);		
#else		
	dc->Blit(r.x,r.y,r.width,r.height,dcBuffer,r.x,r.y);		
#endif
}



void spCanvas::Update(bool flagRedraw)
{
	UpdateObjectsGeometries();
	if (!flagRedraw) return;
	UpdatePaint();
}

// Update coordinates of all objects that must be updated
void spCanvas::UpdateObjectsGeometries()
{
    try {
	for (int i = 0; i<objectsWhoseGeometryMustBeUpdated.Size();i++) {
		if (objectsWhoseGeometryMustBeUpdated[i]->flagGeometryMustBeUpdated)  {
		  objectsWhoseGeometryMustBeUpdated[i]->UpdateGeometryMain();
			if (!objectsWhoseGeometryMustBeUpdated[i]->IsHidden()) NotifyRectMustBeDrawn(objectsWhoseGeometryMustBeUpdated[i]->gBoundingRect());
		}
	}
    }
    CATCH;
    
	objectsWhoseGeometryMustBeUpdated.Init();
}


// Update Draw
void spCanvas::UpdatePaint()
{
//    cout << "paint " << rectToBePainted << endl;
//	cout << "paint" << endl;
//	rectToBePainted = spRect(0,0,1000,1000);
	SPDEBUG(spDebugUpdatePaint, "--> Canvas UpdatePaint " << rectToBePainted);
	if (rectToBePainted.IsEmpty()) return;
	if (dc != NULL) UpdatePaintWithDC(rectToBePainted);
	else UpdatePaintWithoutDC(rectToBePainted);
	InitRectToBePainted();
	SPDEBUG(spDebugUpdatePaint, "<-- Canvas Update Draw");
}

void spCanvas::UpdatePaintWithoutDC(spRect &r)
{
	try {

        OPENCLIENTDC;
	
        UpdatePaintWithDC(r);
	
        CLOSEDC;
    }
    CATCH_CLOSEDC;        
}

void spCanvas::UpdatePaintWithDC(spRect &r)
{
    try {
	r.Intersect(container->gBoundingRect());
	if (r.IsEmpty()) return;

/*	wxClientDC dc1(window);
	wxDC *dc2 = this->dc;
	this->dc = &dc1;
*/
	container->UseAttributes();
	container->Draw(r);
//	this->dc = dc2;
    }
    CATCH;
}

#include <stdio.h>

spCanvas::spCanvas(long id,spTopContainer *tc, const char *name1)
{
    _nRefs=0;
    SPDEBUG(spDebugRef, "Allocation(Canvas(" << (name1 ? name1 : "?") << "))");

//	cout << "C++ Canvas new" << endl;

	flagMouseIn = false;
	
    flagPrinting = false;
    
	window = wxWindow::FindWindowById(id);
    wxWindowId = id;
    
  if (window == NULL) {
//		printf("Bad ID\n");
		return;
	}
	
	flagIsReady = false;
	flagFirstResizeEventReceived = false;
	
	dc = NULL;
	
	dcBuffer = NULL;
#ifndef spNOGC
	memoryDC= NULL;
#endif
	wxSize s = window->GetSize();
	AllocDCBuffer(s);
	
	clipX = clipY = 0;
	ratioPixel = 1;
	
	canvasList.Add(this);

	window->Bind(wxEVT_MOUSEWHEEL,&spCanvas::OnMouseWheel,this);
	window->Bind(wxEVT_PAINT,&spCanvas::OnPaint,this);
	window->Bind(wxEVT_MOTION,&spCanvas::OnMouseMotion,this); 
	window->Bind(wxEVT_SIZE,&spCanvas::OnResize,this);
	window->Bind(wxEVT_LEFT_UP,&spCanvas::OnMouseUp,this);
	window->Bind(wxEVT_RIGHT_UP,&spCanvas::OnMouseUp,this);
	window->Bind(wxEVT_MIDDLE_UP,&spCanvas::OnMouseUp,this);
	window->Bind(wxEVT_LEFT_DOWN,&spCanvas::OnMouseDown,this);
	window->Bind(wxEVT_RIGHT_DOWN,&spCanvas::OnMouseDown,this);
	window->Bind(wxEVT_MIDDLE_DOWN,&spCanvas::OnMouseDown,this);
	window->Bind(wxEVT_LEFT_DCLICK,&spCanvas::OnMouseDClick,this);
	window->Bind(wxEVT_RIGHT_DCLICK,&spCanvas::OnMouseDClick,this);
	window->Bind(wxEVT_MIDDLE_DCLICK,&spCanvas::OnMouseDClick,this);
	window->Bind(wxEVT_ENTER_WINDOW,&spCanvas::OnMouseEnter,this);
	window->Bind(wxEVT_LEAVE_WINDOW,&spCanvas::OnMouseLeave,this);
	window->Bind(wxEVT_CHAR,&spCanvas::OnKeyChar,this);

    if (curCanvas == NULL) curCanvas = this;
	/*
	 wxWindowID id = GetId();
	 */

    container = NULL;
    if (tc == NULL) {
        container = new spTopContainer(this,name1);
        container->RefAndConfig();
    }
    else {
        container = tc;
        container->Ref();
        tc->SetCanvas(this);
    }
    
//    printf("NEW NEW %p\n",container);
    
}

spCanvas::~spCanvas()
{
    SPDEBUG(spDebugRef, "Desallocation(Canvas(" << container->GetName() << "))");
	if (container) container->NotifyCanvasDeleted();
    
    DesallocDCBuffer();
	
	if (container) container->canvas = NULL;
	if (container) container->Unref();
	canvasList.Remove(this);

    if (curCanvas == this) {
        if (canvasList.Size() > 0) curCanvas = canvasList[0];
        else curCanvas = NULL;
    }
    
}


void spCanvas::NotifyRectMustBeDrawn(spRect rect)
{
	if (!flagIsReady) return;
	rectToBePainted.Union(rect);
}

void spCanvas::NotifyGeometryChange(spObject *obj)
{	
	if (obj->container->flagGeometryMustBeUpdated) return;
//    cout << "update" << endl;
	objectsWhoseGeometryMustBeUpdated.Add(obj);
	obj->flagGeometryMustBeUpdated = true;
}

void spCanvas::NotifyObjectDeleted(spObject *obj)
{
  objectsWhoseGeometryMustBeUpdated.Remove(obj);
}


void spCanvas::GetSize(int &width, int &height)
{
	window->GetClientSize(&width,&height);
}


void spCanvas::SendToDC(wxDC *dc1,wxRect pageRect)
{
    int width1 = pageRect.width;
	
	int width,height;
	GetSize(width,height);
	
	// !! Problem of DC memory DC
    dc = dc1;
	ratioPixel = width1/((double) width);
	
		
//    container->UpdateGeometry();
	container->OnPaint(container->gBoundingRect());
	ratioPixel = 1;
//    container->UpdateGeometry();
	
	dc = NULL;
}

void spCanvas::Print(bool flagPageSetupDialog, bool flagPrintDialog, int units_per_cm)
{
	extern void spPrintCanvas(spCanvas *canvas,int units_per_cm,bool flagPageSetupDialog,bool flagPrintDialog);
    
    flagPrinting = true;
	spPrintCanvas(this,units_per_cm,flagPageSetupDialog,flagPrintDialog);
    flagPrinting = false;
}

void spCanvas::ToPS(char *filename,double x_margin_cm,double y_margin_cm)
{
	extern void spCanvasToPS(spCanvas *canvas,char *filename,double,double);
    
    flagPrinting = true;
	spCanvasToPS(this,filename,x_margin_cm,y_margin_cm);
    flagPrinting = false;
}

void spCanvas::ToSVG(char *filename)
{
	extern void spCanvasToSVG(spCanvas *canvas,char *filename);
    
    flagPrinting = true;
	spCanvasToSVG(this,filename);
    flagPrinting = false;
}

void spCanvas::OnPaint(wxPaintEvent& event)
{
    curCanvas = this;
    
	// After first resize was sent we accept the Draw event
	if (!flagFirstResizeEventReceived) return;
	flagIsReady = true;
	
#ifdef SP_SCRIPT
	SCRIPT_StartEvent();
#endif
	
	SPDEBUG(spDebugDraw, "--> OnPaint Canvas " << this);
	
	try {		
		
		OPENPAINTDC;	

		// We update the bounding rects
		SPUpdate(false);
				
		wxRegionIterator upd(window->GetUpdateRegion());
		
		spRect r;
		if (!rectToBePainted.IsEmpty()) {
			r = rectToBePainted;
			InitRectToBePainted();
		}
		else {
			r.x = upd.GetX();
			r.y = upd.GetY();
			r.width = upd.GetW();
			r.height = upd.GetH();			
		}
		
		while (upd) {
		  wxRect r1;
			r1.x = upd.GetX();
			r1.y = upd.GetY();
			r1.width = upd.GetW();
			r1.height = upd.GetH();
			r.Union(r1);
			upd ++ ;
		}		
		
		container->OnPaint(r);
		
		CLOSEDC;
	}
	CATCH_CLOSEDC;
	
#ifdef SP_SCRIPT
	SCRIPT_EndEvent();
#endif

	
	SPDEBUG(spDebugDraw, "<-- End OnPaint Canvas " << this);
}

void spCanvas::ProcessEvent1(spObject *object, spEvent &event)
{
	try {
		
		//
		// Process the event
		//
		OPENCLIENTDC;	
		
		object->ProcessEvent1(event);	
		SPUpdate();
		CLOSEDC;
	}
	CATCH_CLOSEDC;	
}


void spCanvas::ProcessEvent(spObject *object, spEvent &event)
{
	try {
		
		//
		// Process the event
		//
		OPENCLIENTDC;	
		
		object->ProcessEvent(event);	
		SPUpdate();
		CLOSEDC;
	}
	CATCH_CLOSEDC;	
}

void spCanvas::OnMouseMotion(wxMouseEvent& wxevent)
{	
    curCanvas = this;

	if (!flagMouseIn) return;
	if (spEvent::lastMouseMotion == spPoint(wxevent.GetX(),wxevent.GetY()) && spEvent::lastMouseCanvas == this) return;
	spEvent::lastMouseMotion = spPoint(wxevent.GetX(),wxevent.GetY());
	spEvent::lastMouseCanvas = this;
	
//	printf("Motion %p\n",this);
	
	// 
	// Prepare the event
	//
	spEvent event;	
	event.mouse = spEvent::lastMouseMotion;		
	event.modifiers = 0;
	if (wxevent.MetaDown()) event.modifiers |= SP_MOD_META;
	if (wxevent.ShiftDown()) event.modifiers |= SP_MOD_SHIFT;
	if (wxevent.AltDown()) event.modifiers |= SP_MOD_ALT;
	if (wxevent.ControlDown()) event.modifiers |= SP_MOD_CTRL;
	if (wxevent.LeftDown()) event.type = spOnMotionLeft;
	else if (wxevent.RightDown()) event.type = spOnMotionRight;
	else if (wxevent.MiddleDown()) event.type = spOnMotionMiddle;
	else event.type = spOnMotion;
	
	// If currently dragging then we change the modifiers and the button to the initial modifers/button
  if (spEvent::Dragging()) {
	  event.modifiers = spEvent::modifiersDrag;
		if (spEvent::buttonDrag == spOnDownLeft) event.type = spOnMotionLeft;
		else if (spEvent::buttonDrag == spOnDownRight) event.type = spOnMotionRight;
		else if (spEvent::buttonDrag == spOnDownMiddle) event.type = spOnMotionMiddle;
	}
	
	//
	// Let's process the event
	//
	
#ifdef SP_SCRIPT
	SCRIPT_StartEvent();
#endif
	

	ProcessEvent(container,event);
	
#ifdef SP_SCRIPT
	SCRIPT_EndEvent();
#endif

}

void spCanvas::OnMouseDown(wxMouseEvent& wxevent)
{
    curCanvas = this;

	// If currently dragging --> we do nothing
	if (spEvent::Dragging()) return;

#ifdef SP_SCRIPT
	SCRIPT_StartEvent();
#endif
	
	//
	// Prepare the event
	//
	spEvent event;
	event.mouse = spPoint(wxevent.GetX(),wxevent.GetY());
	if (wxevent.LeftDown()) event.type = spOnDownLeft;
	else if (wxevent.RightDown()) event.type = spOnDownRight;
	else if (wxevent.MiddleDown()) event.type = spOnDownMiddle;	
	event.modifiers = 0;
  if (wxevent.MetaDown()) event.modifiers |= SP_MOD_META;
  if (wxevent.ShiftDown()) event.modifiers |= SP_MOD_SHIFT;
  if (wxevent.AltDown()) event.modifiers |= SP_MOD_ALT;
  if (wxevent.ControlDown()) event.modifiers |= SP_MOD_CTRL;
	
	
	ProcessEvent(container,event);
	
	//
	// Capture the mouse
	//
	if (spEvent::Dragging()) window->CaptureMouse();

#ifdef SP_SCRIPT
	SCRIPT_EndEvent();
#endif

}


void spCanvas::OnMouseDClick(wxMouseEvent& wxevent)
{
    curCanvas = this;

	// If currently dragging --> we do nothing
	if (spEvent::Dragging()) return;
	
#ifdef SP_SCRIPT
	SCRIPT_StartEvent();
#endif
	
	//
	// Prepare the event
	//
	spEvent event;
	event.mouse = spPoint(wxevent.GetX(),wxevent.GetY());
	if (wxevent.LeftDClick()) event.type = spOnDLeft;
	else if (wxevent.RightDClick()) event.type = spOnDRight;
	else if (wxevent.MiddleDClick()) event.type = spOnDMiddle;	
	event.modifiers = 0;
  if (wxevent.MetaDown()) event.modifiers |= SP_MOD_META;
  if (wxevent.ShiftDown()) event.modifiers |= SP_MOD_SHIFT;
  if (wxevent.AltDown()) event.modifiers |= SP_MOD_ALT;
  if (wxevent.ControlDown()) event.modifiers |= SP_MOD_CTRL;
	
	ProcessEvent(container,event);
	
#ifdef SP_SCRIPT
	SCRIPT_EndEvent();
#endif

}


void spCanvas::OnMouseUp(wxMouseEvent& wxevent)
{
    curCanvas = this;

	// If not currently dragging --> we do nothing
  if (!spEvent::Dragging()) return;

	//
	// Preparing the event
	//
	spEvent event;	
	event.mouse = spPoint(wxevent.GetX(),wxevent.GetY());
	int but = wxevent.GetButton();	
	if (but == wxMOUSE_BTN_LEFT) {
		if (spEvent::buttonDrag != spOnDownLeft) return;
		event.type = spOnUpLeft;		
	}
	else if (but == wxMOUSE_BTN_RIGHT) {
		if (spEvent::buttonDrag != spOnDownRight) return;
		event.type = spOnUpRight;		
	}
	else if (but == wxMOUSE_BTN_MIDDLE) {
		if (spEvent::buttonDrag != spOnDownMiddle) return;
		event.type = spOnUpMiddle;		
	}
	event.modifiers = spEvent::modifiersDrag;
	
	// Release the mouse
	window->ReleaseMouse();
	
#ifdef SP_SCRIPT
	SCRIPT_StartEvent();
#endif
	
	try {
		
		//
		// Processing the event
		//
		OPENCLIENTDC;	
		
		container->ProcessEvent(event);
		SPUpdate();
		
		// Dragging is over
		spEvent::DragEnd();
		
		//
		// We are not over yet !
		// We must process the enter/leave events
		//
		event.type = spOnNone;
		container->ProcessEvent(event);
		CLOSEDC;
	}
	CATCH_CLOSEDC;
	
	spEvent::DragEnd();
	
#ifdef SP_SCRIPT
	SCRIPT_EndEvent();
#endif

}

#ifndef SP_SCRIPT
void spCanvas::OnPopupClick(wxCommandEvent &evt)
{
    curCanvas = this;

	int id = evt.GetId();
	
	try {
		
		OPENCLIENTDC;	
		
		spInteractorContextMenu::ProcessPopup(id);
		SPUpdate();
		CLOSEDC;
	}
	CATCH_CLOSEDC;
	
}
#endif

void spCanvas::OnResize(wxSizeEvent& event)
{
    curCanvas = this;

#ifdef SP_SCRIPT
	SCRIPT_StartEvent();
#endif
	
	try {
	flagFirstResizeEventReceived = true;
	
	int width,height;
	GetSize(width,height);
	AllocDCBuffer(wxSize(width,height));
	container->OnResize();
#ifdef WIN32
  NotifyRectMustBeDrawn(spRect(0,0,width,height));
#endif
	}
	CATCH_CLOSEDC;
	
#ifdef SP_SCRIPT
	SCRIPT_EndEvent();
#endif
    
#ifdef WIN32
  UpdatePaint();
#endif
}

void spCanvas::OnMouseWheel(wxMouseEvent &wxevent)
{
    curCanvas = this;

#ifdef SP_SCRIPT
	SCRIPT_StartEvent();
#endif
	
	spEvent event;
	
	event.type = spOnMouseWheel;
	
	event.mouse = spPoint(wxevent.GetX(),wxevent.GetY());
	
	event.modifiers = 0;
	event.wheel = - wxevent.GetWheelRotation();
  if (wxevent.MetaDown()) event.modifiers |= SP_MOD_META;
  if (wxevent.ShiftDown()) event.modifiers |= SP_MOD_SHIFT;
  if (wxevent.AltDown()) event.modifiers |= SP_MOD_ALT;
  if (wxevent.ControlDown()) event.modifiers |= SP_MOD_CTRL;
	
	ProcessEvent(container,event);
	
#ifdef SP_SCRIPT
	SCRIPT_EndEvent();
#endif

	
}


void spCanvas::OnMouseEnter(wxMouseEvent& event)
{
    curCanvas = this;

#ifdef SP_SCRIPT
	SCRIPT_StartEvent();
#endif

	flagMouseIn = true;
	
	try {
		OPENCLIENTDC;	
		container->OnMouseEnter();
		SPUpdate();
		CLOSEDC;
	}
	CATCH_CLOSEDC;
#ifdef SP_SCRIPT
	SCRIPT_EndEvent();
#endif

}

void spCanvas::OnMouseLeave(wxMouseEvent& event)
{
    curCanvas = this;

#ifdef SP_SCRIPT
	SCRIPT_StartEvent();
#endif

	try {
		OPENCLIENTDC;
		
		// We must send some leave events to the whole hierarchy
		container->OnMouseLeave();
		SPUpdate();
		
		CLOSEDC;
	}
	CATCH_CLOSEDC;
#ifdef SP_SCRIPT
	SCRIPT_EndEvent();
#endif

	flagMouseIn = false;
}

void spCanvas::OnKeyChar(wxKeyEvent& wxevent) 
{
    curCanvas = this;

    //	static int count = 0;
	//
	// we skip the menu keys
	//
	switch(wxGetOsVersion()) {
			
			__CASEMAC : 
			if (wxevent.CmdDown()) {
				wxevent.Skip();
				return;
			}
			break;
			
			__CASEWINDOWS : __CASEUNIX :			
			if (wxevent.AltDown()) {
				wxevent.Skip();
				return;
			}
			break;
			
		default :
			break;
	}
	
	spEvent event;
	event.type = spOnKeyDown;
	
	event.keycode = wxevent.GetKeyCode();
	event.mouse = window->ScreenToClient(wxGetMousePosition());
	
	event.modifiers = 0;
	
  if (wxevent.MetaDown()) event.modifiers |= SP_MOD_META;
  if (wxevent.ShiftDown()) event.modifiers |= SP_MOD_SHIFT;
  if (wxevent.AltDown()) event.modifiers |= SP_MOD_ALT;
  if (wxevent.ControlDown()) event.modifiers |= SP_MOD_CTRL;
	
#ifdef SP_SCRIPT
	SCRIPT_StartEvent();
#endif
	
	ProcessEvent(container,event);
#ifdef SP_SCRIPT
	SCRIPT_EndEvent();
#endif

}




