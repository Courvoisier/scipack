//
//  shape.h
//  sp
//
//  Created by bacry on 15/01/13.
//
//

#ifndef __sp__shape__
#define __sp__shape__



typedef enum spshape {
	spShapeEllipse = 0,
	spShapeRect,
    spShapeLine,
    spShapeInfiniteLine,
    spShapeArrow,
    spShapeDoubleArrow
} spShapeType;

class spShape : public spObject
{
	spShapeType shape;
    int arrowLength;
    double arrowAngle;
    
    unsigned int tolerance; // Tolerance in pixel for the GetDistance function
    
    double angle; // Only used for infinite line
    
	public :
	
    
	spShape(spContainer *container=NULL,spShapeType shape1=spShapeRect,const spAnchor &anchor=spAnchor(),spMargin margin=spMargin(),unsigned long flags=0,const char *name=NULL);
    
	virtual void Draw(spRect &rect);
    
    void SetShape(spShapeType sh);
    spShapeType GetShape() {return shape;}

    void SetTolerance(unsigned int tol) {tolerance = tol;}
    unsigned int GetTolerance() {return tolerance;}

    void SetArrowLength(int arrowLength);
    int GetArrowLength() {return arrowLength;}
    
    void SetArrowAngle(double arrowAngle);
    double GetArrowAngle() {return arrowAngle;}

    virtual double GetDistance(spRealPoint pt);
    
    virtual const char *GetClassName() {
		return _CCHAR("spShape");
	}
};


#endif /* defined(__sp__shape__) */
