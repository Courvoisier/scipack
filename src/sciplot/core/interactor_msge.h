/*
 *  interactor_msge.h
 *  LastPlot
 *
 *  Created by bacry on 04/10/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _SP_INTERACTOR_MSGE_H
#define _SP_INTERACTOR_MSGE_H

class spInteractorSendMsge : public spInteractor1
{
  public :
	
	SP_SMARTPTR(spObject) receiver;
	
	spMsge msge;
    
	spInteractorSendMsge(spObject *receiver, spMsge msge, spEventSimpleType e,int m=0,const char *name = NULL);

	virtual const char *GetClassName() {
		return _CCHAR("spInteractor1");
	}
};

#endif