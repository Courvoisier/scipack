/*
 *  textedit.h
 *  LastPlot
 *
 *  Created by bacry on 04/10/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


class spInteractorTextEdit;


//
//
// A window to edit some text
//
// It can be eventually linked to a textbox object, so that when it closes it sets the textbox
// It can be eventually linked to a spInteractorTextEdit and notifies it when it closes
//

class spTextEdit : public wxFrame 
{
  public :
  
	// A constuctor from a text box
	spTextEdit(spText *tb, const char *init,int x, int y, int w,int h);
	
	//A constructor from a spInteractorTextEdit
	spTextEdit(spInteractorTextEdit *teInteractor, const char *init,int x, int y, int w,int h);
	
	wxTextCtrl *tc;
	SP_SMARTPTR(spText) tb;
	spInteractorTextEdit *teInteractor;
	
	void OnClose(wxCloseEvent& event);
	
	DECLARE_EVENT_TABLE()
};


class spInteractorTextEdit : public spInteractor1
{
	static spInteractorTextEdit *theInteractor;

	public : 

  static spInteractorTextEdit *GetTheInteractor();
	
	spTextEdit *te;
	SP_SMARTPTR(spText) tb;
	
	void OnClose(const char *str);
	
	spInteractorTextEdit();
	~spInteractorTextEdit();
};
