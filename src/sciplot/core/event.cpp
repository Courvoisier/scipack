
//  SciPack
//
//  Created by bacry on 24/04/13.
//  Copyright (c) 2013 bacry. All rights reserved.
//


#include "sp.h"


spObject *spEvent::menuObject = NULL;

bool spEvent::flagDrag = false;
spEventSimpleType spEvent::buttonDrag = spOnNone;
unsigned int spEvent::modifiersDrag = 0;
spObject* spEvent::objectDrag;
spObject* spEvent::hierarchy[1000];
int spEvent::nHierarchy=0;
int spEvent::nCurHierarchy=0;
bool spEvent::flagPropagate = false;
void spEvent::DragStart(spEvent &event)
{
	buttonDrag = event.type;
	modifiersDrag = event.modifiers;
	flagDrag = true;
	objectDrag = event.object;
}


spPoint spEvent::lastMouseMotion;
spCanvas *spEvent::lastMouseCanvas = NULL;

ostream& operator << (ostream& o, spEvent &e)
{
    o << "spEvent(";
	switch(e.type) {
		case spOnNone: o << "none"; break;
		case spOnDLeft: o << "dLeft"; break;
		case spOnDRight: o << "dRight"; break;
		case spOnDMiddle: o << "dMiddle"; break;
		case spOnDownLeft: o << "downLeft"; break;
		case spOnDownRight: o << "downRight"; break;
		case spOnDownMiddle: o << "downMiddle"; break;
		case spOnUpLeft: o << "upLeft"; break;
		case spOnUpRight: o << "upRight"; break;
		case spOnUpMiddle: o << "upMiddle"; break;
		case spOnEnter: o << "enter"; break;
		case spOnLeave: o << "leave"; break;
		case spOnMotion: o << "motion"; break;
		case spOnMotionLeft: o << "motionLeft"; break;
		case spOnMotionRight: o << "motionRight"; break;
		case spOnMotionMiddle: o << "motionMiddle"; break;
		case spOnDragLeft: o << "dragLeft"; break;
		case spOnDragRight: o << "dragRight"; break;
		case spOnDragMiddle: o << "dragMiddle"; break;
		case spOnMouseWheel: o << "wheel"; break;
		case spOnTime: o << "time"; break;
		case spOnKeyDown: o << "keyDown"; break;
            /*			spOnBeforeDraw,
             spOnAfterDraw,
             spOnChangeBound,
             spOnDelete,
             spOnChangedGeometry,	*/
		default : o << e.type;
	}
    /*	if (e.object != NULL)
     o << "," << "flagDrag=" << spEvent::Dragging() << "," << e.object->name << "," << e.mouse << ",modifiers=" << e.modifiers << ",wheel=" << e.wheel << ")";
     else
     o << "," << "flagDrag=" << spEvent::Dragging() << "," << "?" << "," << e.mouse << ",modifiers=" << e.modifiers << ",wheel=" << e.wheel << ")";
     */
	return o;
}



myWxTimer::myWxTimer(spTimer *timer1)
{
	timer = timer1;
}
void myWxTimer::Notify()
{
	timer->Notify();
}

spList<spTimer,100,100> spTimer::list;


spTimer::spTimer()
{
	wxtimer = new myWxTimer(this);
	
    object = NULL;
    
	list.Add(this);
}

spTimer::~spTimer()
{
	list.Remove(this);
    if (object) object->Unref();
    
	delete wxtimer;
}

bool spTimer::Start(spObject *object1, int milliseconds, bool oneShot)
{
    if (object1) {
        object = object1;
        object->Ref();
    }
    else object = NULL;

    return wxtimer->Start(milliseconds, oneShot);
}

void spTimer::Stop()
{
    if (object) {
        object->Unref();
        object = NULL;
    }
    
    wxtimer->Stop();
}


void spTimer::Notify()
{
    if (object == NULL) return;
	spEvent event;
	event.type = spOnTime;
	event.object = object;
	event.timer = this;
	event.mouse = spEvent::lastMouseMotion;
#ifdef SP_SCRIPT
	SCRIPT_StartEvent();
#endif
	object->container->topContainer->canvas->ProcessEvent1(object,event); // Process event without processing Leave/Enter events !
#ifdef SP_SCRIPT
	SCRIPT_EndEvent();
#endif	
}


