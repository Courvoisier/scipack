
// Not a module

%rename(FramedView) spFramedView;

// We set the kwargs for some methods
%feature("kwargs") spFramedView::spFramedView;

%DocClass(spFramedView,"A container class that encapsulates a sp.View with axis (sp.AxisView), labels for the x and y axis (sp.Text), a title (sp.Text) and an info text at the bottom (sp.TextBox). This last sp.TextBox is only created if self.CreateInfoObject() is called. There are fields for accessing all of these objects directly. It actually inherits from sp.Grid. The size is 1x1 and the grid is basically used for placing the sp.View in the middle of the sp.FramedView with margins around it. You can use the following sp.View values for the flags, they will be used for the sp.View inside the sp.FramedView :
- sp.FLAG_VIEW_REVERSE_X (views only) : X axis is reversed
- sp.FLAG_VIEW_REVERSE_Y (views only) : Y axis is reversed
- sp.FLAG_VIEW_KEEPBOUND_WHEN_RESIZED (views only) : Keeps bounds of the views when resized")
class spFramedView : public spGrid
{
	public : 
	
    // The string representation in python
    ObjRepr;

	spFramedView(spContainer *ctn=NULL,const spAnchor &anchor=spAnchor(),const spMargin &margin=spMargin(), unsigned long flags=spFLAG_VIEW_REVERSE_Y | spFLAG_VIEW_KEEPBOUND_WHEN_RESIZED,const char *name=NULL);

    %DocMeth(SetInfo,"SetInfo(string)","Set the info text of the sp.TextBox")
	void SetInfo(const char *str);
	const char *GetInfo();
    %attobj_def(info,Info,
    """
    **Attribute** self.info = string
    
    The string in the info sp.TextBox at the bottom of the FramedView.
    """);


    %DocMeth(CreateInfoObject,"CreateInfoObject()","Create a sp.TextBox at the bottom of the FramedView")
	void CreateInfoObject();

};

%extend spFramedView {
%immutable;
    PyObject *viewObject;
    PyObject *titleObject;
    PyObject *labelxObject;
    PyObject *labelyObject;
    PyObject *axisObject;
    PyObject *infoObject;
%mutable;
}
%{
    PyObject *spFramedView_viewObject_get(spFramedView *fv) {return WrapObject(fv->GetViewObject());}
    PyObject *spFramedView_titleObject_get(spFramedView *fv) {return WrapObject(fv->GetTitleObject());}
    PyObject *spFramedView_labelxObject_get(spFramedView *fv) {return WrapObject(fv->GetLabelxObject());}
    PyObject *spFramedView_labelyObject_get(spFramedView *fv) {return WrapObject(fv->GetLabelyObject());}
    PyObject *spFramedView_axisObject_get(spFramedView *fv) {return WrapObject(fv->GetAxisObject());}
    PyObject *spFramedView_infoObject_get(spFramedView *fv) {return WrapObject(fv->GetInfoObject());}
%}
