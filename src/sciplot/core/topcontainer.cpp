/*
 *  topview.cpp
 *  SciPlot
 *
 *  Created by bacry on 09/08/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#include "sp.h"

void spTopContainer::AddOverlayObject(spObject *object) 
{
	overlayObjectList.Add(object);
	object->Ref();
}

void spTopContainer::RemoveOverlayObject(spObject *object) 
{
	if (overlayObjectList.Remove(object)) object->Unref();
    object->container = NULL;
}


spTopContainer::spTopContainer(spCanvas *canvas1,const char *name1) : spGrid(1,name1)
{
	canvas = canvas1;	
	topContainer = this;
	container = topContainer;
	flagResizeEvent = false;
	flagHidden = false;
	flagGeometryMustBeUpdated = true;
}

spTopContainer::~spTopContainer()
{
//	cout << "Delete topview " << this << endl;
	flagHidden = true;
	while (overlayObjectList.Size() != 0) {
		overlayObjectList[0]->DeactivateOverlay();
	}
	topContainer = NULL;
	container = NULL;
}


// We must erase first the spOVERLAY_INVERT then the spFLAG_OBJECT_OVERLAY_BUFFER
void spTopContainer::EraseOverlayObjects()
{
	// Buffer
	for(int i=overlayObjectList.Size()-1;i>=0;i--) {
		overlayObjectList[i]->EraseOverlay();
	}
}

// We must draw first the spFLAG_OBJECT_OVERLAY_BUFFER then the spOVERLAY_INVERT
void spTopContainer::DrawOverlayObjects()
{
	DeactivateDCBuffer();
	for(int i=0;i<overlayObjectList.Size();i++) {
		overlayObjectList[i]->DrawOverlay();
	}
	ActivateDCBuffer();
}

void spTopContainer::NotifyViewDeleted2OverlayObjects(spContainer *v)
{
	for(int i=0;i<overlayObjectList.Size();i++) {
    if (overlayObjectList[i]->container == v) overlayObjectList[i]->container = NULL;
	}
}

void spTopContainer::OnPaint(spRect &r)
{
	SPDEBUG(spDebugDraw, "--> Draw topcontainer " << this << " #elements : " << objectList.Size() << " Rect : " << r);
	
	if (flagGeometryMustBeUpdated) UpdateGeometryMain();
	
	if (IsHidden()) return;
	
    UseAttributes();
	spContainer::Draw(r);
/*	Clear();
	
	for (int i=0;i<objectList.Size();i++) {
		if (!objectList[i]->IsHidden()) {
			if (objectList[i]->flagGeometryMustBeUpdated) objectList[i]->UpdateGeometryMain();
		  objectList[i]->Draw(r);	
		}
	}
*/	
	SPDEBUG(spDebugDraw, "<-- End Draw topview " << this);
}

void spTopContainer::OnResize() 
{
	SPDEBUG(spDebugResize, "--> Resize topview " << this );
	flagResizeEvent = true;
	UpdateGeometryMain();
	flagResizeEvent = false;
	SPDEBUG(spDebugResize, "<-- Resize topview " << this);
}

void spTopContainer::UpdateGeometry()
{
	int width,height;
	canvas->GetSize(width,height);
	_gBoundingRect =spRect(0,0,width,height);	
	_gRect = _gBoundingRect;
	clip = gBoundingRect();
	box = spBox(spAnchor(clip.x,clip.y),spMargin(clip.width,clip.height));
	spGrid::UpdateGeometry();
}




bool spTopContainer::ProcessEvent(spEvent &ev)
{
	spEvent::nCurHierarchy = 0;
	bool res =  spContainer::ProcessEvent(ev);
	if (ev.type != spOnMotion && ev.type != spOnMotionLeft && ev.type != spOnMotionRight && ev.type != spOnMotionMiddle && 
			ev.type != spOnDragLeft && ev.type != spOnDragRight && ev.type != spOnDragMiddle) {
		SPDEBUG(spDebugEventsMouse, ev);
	}
	else {
		SPDEBUG(spDebugEventsMotion, ev);		
	}
	return res;	
}

void spTopContainer::OnMouseEnter()
{
}

void spTopContainer::OnMouseLeave()
{
	spEvent::nCurHierarchy = 0;
	ProcessPostLeaveEvent();
	spEvent::nHierarchy = 0;
}
