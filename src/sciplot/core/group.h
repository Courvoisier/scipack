//
//  group.h
//  sp
//
//  Created by bacry on 25/01/13.
//
//

#ifndef __sp__group__
#define __sp__group__



class spGroup : public spContainer
{
    bool flagProcessGeometryUpdateNow;
    
public:
    
    spGroup(spContainer *ct=NULL,unsigned long flags=0,const char *name = NULL);
    
    virtual ~spGroup();

    virtual bool IsGroup() {return true;};
    
    void NotifyObjectGeometryUpdateMain(spObject *);
    bool ProcessGeometryUpdateNow() {return flagProcessGeometryUpdateNow;}
    
    
    virtual void AttBeforeUpdate();
    virtual void AttAfterUpdate();

    
    virtual void NotifyObjectGeometryUpdate(spObject *);
    
	virtual void AddObject(spObject *object);
	virtual void FixObject(spObject *object);
	virtual void RemoveObject(spObject *object);

    spList<spObject,10> fixedObjects;
    
    spList<spRealPoint,10> objectAnchors;
    
    
    bool flagLocked;
    void Lock();
    void UnLock();
    bool IsLocked() {return flagLocked;}

    virtual const char *GetClassName() {
		return _CCHAR("spGroup");
	}

};


#endif /* defined(__sp__group__) */
