

#include "sp.h"


//
// Called when bound is changed
//
void CBCursorChangeBoundView(spInteractor *cb, spEvent &event)
{
	spInteractorCursorView *i = (spInteractorCursorView *) cb;
	spView *view = (spView *) event.object;
	
	if (!i->flagIn || !i->flagPrintInfo || (i->cursor && i->cursor->container != view)) return;

	// We find the object the cursor is close to and the corresponding coordinate
	spRealPoint pt = event.mouse;
	spObject * o = NULL;
	if (i->flagFollowMouse) o = view->FindObjectMouse(pt,true);
	if (o == NULL) o = view;

	// We print the info if needed
	if (i->flagPrintInfo) {		
		const char *str = o->ExecuteCursorCallback(i->flagFullInfo,pt);
		if (i->output) i->output->SetText(str);
		else view->container->SetText(str);
	}
	
	// If a cursor object then display it (as an overlay)
	if (i->cursor) {
		i->cursor->ActivateOverlay();
		i->cursor->SetAnchor(spRealPoint(i->cursor->container->l2top.IApply(pt+i->offset)));
	}

}

//
// Called when within the view
//
void CBCursorView(spInteractor *cb, spEvent &event)
{
	spInteractorCursorView *i = (spInteractorCursorView *) cb;
	spView *view = (spView *) event.object;

	i->flagIn = true;
	
	// We find the object the cursor is close to and the corresponding coordinate
	spRealPoint pt = event.mouse;
	spObject * o = NULL;
	if (i->flagFollowMouse) o = view->FindObjectMouse(pt,true);
	if (o == NULL) o = view;
	
	// We print the info if needed
	if (i->flagPrintInfo) {
		const char *str = o->ExecuteCursorCallback(i->flagFullInfo,pt);
		if (i->output) i->output->SetText(str);
		else view->container->SetText(str);
	}
	
	// If a cursor object then display it (as an overlay)
	if (i->cursor) {
		SetContainer(i->cursor,view);
		i->cursor->ActivateOverlay();		
		i->cursor->SetAnchor(spRealPoint(i->cursor->container->l2top.IApply(pt+i->offset)));
	}
}

//
// Called when the mouse leaves the view
//
void CBCursorLeaveView(spInteractor *cb, spEvent &event)
{
	spInteractorCursorView *i = (spInteractorCursorView *) cb;
	spView *view = (spView *) event.object;
	
	// Erase the cursor
	if (i->cursor) SetContainer(i->cursor,NULL);
	
	// Erase info
	if (i->flagPrintInfo) {		
	  if (i->output) i->output->SetText("");
		else view->container->SetText("");
	}
}

void spInteractorCursorView::Off()
{
	if (IsOn() && cursor) SetContainer(cursor,NULL);
	if (output) output->SetText("");
	spInteractor::Off();
}


void spInteractorCursorView::SetOutput(spObject *outputObject)
{
    if (output) output->Unref();
	output = outputObject;
    outputObject->Ref();
	flagPrintInfo = true;
}

spInteractorCursorView::~spInteractorCursorView()
{
	if (cursor) cursor->Unref();
}


//
// Constructor
//
spInteractorCursorView::spInteractorCursorView(spObject *cursor1, spPoint offset1, bool flagFollowMouse1, bool flagPropagate1,const char *name) :  spInteractorN(NULL,name)
{
	output = NULL;
	
	flagFollowMouse = flagFollowMouse1;	
	flagIn = false;
	
	if (cursor1 && !cursor1->IsOverlay()) THROW("spInteractorCursorView::spInteractorCursorView() : Cursor object should be an overlay object");
	cursor = cursor1;
	if (cursor) cursor->Ref();
	offset = offset1;
	flagFullInfo = true;
	flagPrintInfo = true;
	
	Add(new spInteractor1(CBCursorView,spOnAnyMotion,0,flagPropagate1));
	Add(new spInteractor1(CBCursorLeaveView,spOnLeave,0,flagPropagate1));
	Add(new spInteractor1(CBCursorChangeBoundView,spOnChangeBound));
}

