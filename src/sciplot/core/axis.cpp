/*
 *  framedview.cpp
 *  SciPlot
 *
 *  Created by bacry on 29/06/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#include "sp.h"


#define ALMOST1		.9999999
#define ALMOST1_DIV_5		.1999999
# define PXTICK 4.0

static double Round[] = { 1., 2., 2.5, 5., 10., 20. };

/* The long ticks of the axis */
static double theXLongTicks[20], theYLongTicks[20];
static int nXLongTicks, nYLongTicks;

/* The short ticks of the axis */
static double theXShortTicks[100], theYShortTicks[100];
static int nXShortTicks, nYShortTicks;


static double g_stsize(double vmin, double vmax,int * n12,double * xvmin,double * xvmax,double * rmin , int *nn0)
{
    double pstep, log10, rstep, order, power, smin, use1, vdif;
    int i, rmin_old;
	
    vdif = vmax - vmin;
    pstep = fabs(vmax - vmin) / 6;
    log10 = log(10.0);
    order = log(pstep)/log10;
	
    if(order < 0) order = order - ALMOST1;
	
    order = (int)order;
    power = pow(10.0, order);
	
    for(i = 0; i < 6; i++) {
        rstep = Round[i]*power;
        if(rstep>=pstep)
			break;
    }
	
    smin = vmin/rstep;
    if(smin < 0) smin = smin - ALMOST1_DIV_5;
    if(vmax < vmin) smin += ALMOST1_DIV_5;
    *rmin = (int)(5 * smin) / 5. ;
    rmin_old = (int)(smin) ;
    *nn0 = (int)((*rmin - rmin_old) * 5) ;
    if(*nn0 <= 0) *nn0 = - *nn0 ;
    else *nn0 = 5 - *nn0 ;
    *rmin *= rstep ;
    use1 = fabs(rstep);
	
    rstep = (vdif > 0) ? use1 : -use1;
    *xvmin = vmin - vdif * 1.e-5;
    *xvmax = vmax + vdif * 1.e-5;
	
    *n12 = (6 + 1) * (5 + 1);
	
    return (rstep / 5.);
}


static void ComputeTicks(double xMin, double xMax, double yMin, double yMax)
{
    double xvmin, xvmax;
    double value, rstep2, rmin;
    int j, n12, nn, nn0;
	
    /* The x-axis ticks */
    rstep2 = g_stsize(yMin, yMax, &n12, &xvmin, &xvmax, &rmin, &nn0);
	
    nYLongTicks = -1;
    nYShortTicks = -1;
	
    for(value = rmin, nn = nn0, j = 0; j < n12; j++) {
        if(((value-xvmax)*(value-xvmin)) <= 0) {
            if(fabs(value) < 1.e-3*MAX(fabs(xvmax),fabs(xvmin))) value = 0;
			
            /* Long tick */
            if(nn == 0) {
                nYLongTicks++;
                theYLongTicks[nYLongTicks] = value;
            }
            
            /* Short tick */
            nYShortTicks++;
			theYShortTicks[nYShortTicks] = value;
        }
        
		if(nn == 0) nn = 5;
        nn--;
        value = rmin + (j + 1) *rstep2;
    }
	
	
    /* The y-axis ticks */
    rstep2 = g_stsize(xMin, xMax, &n12, &xvmin, &xvmax, &rmin, &nn0);
	
    nXLongTicks = -1;
    nXShortTicks = -1;
	
    for(value = rmin, nn = nn0, j = 0; j < n12; j++) {
        if((value-xvmax)*(value-xvmin) <= 0) {
            if(fabs(value) < 1.e-3*MAX(fabs(xvmax),fabs(xvmin))) value = 0;
            
            /* Long tick */
			if(nn == 0) {
                nXLongTicks++;
                theXLongTicks[nXLongTicks] = value;
            }
			
            /* Short tick */
            nXShortTicks++;
			theXShortTicks[nXShortTicks] = value;
        }
        
		if(nn == 0) nn = 5;
        nn--;
        value = rmin+(j+1)*rstep2;
    }
}


void spAxisView::DrawAxis_(int x, int y, int w, int h, double xMin, double xMax, double yMin, double yMax,
                           const char *xText,const char * yText,const char * titleText,int pixelMargin, char flagTicksIn, char flagFrame,char flagReverse,spMap1d &trX,spMap1d &trY)
{
	int xAxis,yAxis,xAxis1,yAxis1,yLongTick,yLongTick1,yShortTick,yShortTick1,xLongTick,xLongTick1,xShortTick,xShortTick1,xStrTick,yStrTick,x0,y0;
	int i;
	char tempStr[80];
	int hstrMode,vstrMode;
    
    
    
	/*
	 * Draw in the x-axis
	 */
	
	/* Draw the axis line */
	if (yMax<=yMin) {
		yAxis = y+h-1+pixelMargin;
		yAxis1 = y-pixelMargin;
	}
	else {
		yAxis = y-pixelMargin;
		yAxis1 = y+h-1+pixelMargin;
	}
    if (connectedView) {
        UseBrush(connectedView->GetBrush());
        UsePen(wxPen(connectedView->GetBrushColour()));
        DrawRectangle(x-pixelMargin, yAxis+connectedView->L2TopSign().y*pixelMargin, -1+w+2*pixelMargin,-connectedView->L2TopSign().y*pixelMargin);
        UseAttributes();
    }
	DrawLine(x-pixelMargin, yAxis, x-1+w+pixelMargin, yAxis);
	if (flagFrame) {
        if (connectedView) {
            UseBrush(connectedView->GetBrush());
            UsePen(wxPen(connectedView->GetBrushColour()));
            DrawRectangle(x-pixelMargin, yAxis1, -1+w+2*pixelMargin,-connectedView->L2TopSign().y*pixelMargin);
            UseAttributes();
 
        }
        DrawLine(x-pixelMargin, yAxis1, x-1+w+pixelMargin, yAxis1);
    }
	
	
	/* Draw the long ticks and label them */
	if (yMax<=yMin) {
        vstrMode = spJustifyVUp;
        if (!flagTicksIn) {
            yLongTick = (int) (yAxis+2*PXTICK);
            yLongTick1 =(int)  (yAxis1-2*PXTICK);
            yStrTick = (int) (yLongTick + 2);
            yShortTick = (int) (yAxis+PXTICK);
            yShortTick1 = (int) (yAxis1-PXTICK);
        }
        else {
            yLongTick = (int) (yAxis-2*PXTICK);
            yLongTick1 = (int) (yAxis1+2*PXTICK);
            yStrTick = (int) (yAxis + 2);
            yShortTick = (int) (yAxis-PXTICK);
            yShortTick1 = (int) (yAxis1+PXTICK);
        }
	}
	else {
        vstrMode = spJustifyVBase;
        if (!flagTicksIn) {
            yLongTick = (int) (yAxis-2*PXTICK);
            yLongTick1 = (int) (yAxis1+2*PXTICK);
            yStrTick =(int)  (yLongTick - 2);
            yShortTick = (int) (yAxis-PXTICK);
            yShortTick1 =(int)  (yAxis1+PXTICK);
        }
        else {
            yLongTick = (int) (yAxis+2*PXTICK);
            yLongTick1 = (int) (yAxis1-2*PXTICK);
            yStrTick = (int) (yAxis - 2);
            yShortTick = (int) (yAxis+PXTICK);
            yShortTick1 = (int) (yAxis1-PXTICK);
        }
	}
	spRect lastRect;
	spFormattedText ft;
    bool flagFirst = true;
	spMap1d trXX(xMin,x,xMax,x+w,trX.GetFlagLn(),trX.GetBase());
	double arrayX[100];
	int nArrayX = 1;
	for (i = 0; i < nXLongTicks + 1; i++){
        
		x0 = (int) trXX.Apply(theXLongTicks[i]);
        if (x0<x || x0>x+w-1) continue;
        
		sprintf(tempStr, "%.8g",theXLongTicks[i]);
        
		if (!flagTicksIn) {
            if (yMax<yMin) yStrTick = yLongTick + 2;
            else yStrTick = yLongTick - 2;
        }
        else {
            if (yMax<yMin) yStrTick = yAxis + 2;
            else yStrTick = yAxis - 2;
		}
        xStrTick = x0;
        
        /* Draw the string if possible */
		ft.SetHVMode(spJustifyHMiddle,vstrMode);
		ft.SetText(tempStr);
        ft.SetAnchorPoint(spRealPoint(xStrTick,yStrTick));
		ft.Format(GetCanvas());
		if (flagFirst || !lastRect.Intersects(ft.boundingRect)) {
			DrawFormattedText(ft);
			DrawLine(x0,yLongTick,x0,yAxis);
			if (flagFrame) DrawLine(x0,yLongTick1,x0,yAxis1);
			arrayX[nArrayX] = theXLongTicks[i];
			nArrayX++;
			lastRect = ft.boundingRect;
			flagFirst = false;
		}
	}
	if (trXX.GetFlagLn()) { // Compute the short ticks
        double step = arrayX[2]/arrayX[1];
        arrayX[0] = arrayX[1]/step;
        arrayX[nArrayX] = arrayX[nArrayX-1]*step;
		nArrayX++;
		int x1 = (int) trXX.Apply(arrayX[1]);
		int x2 = (int) trXX.Apply(arrayX[2]);
		int nShort;
		if (x2-x1 > 100) nShort = 10;
		else nShort = 5;
		nXShortTicks = 0;
		for (int i = 1; i<nArrayX;i++) {
			for (int j=0;j<nShort;j++) {
				theXShortTicks[nXShortTicks] = (arrayX[i]-arrayX[i-1])*((double) j)/(nShort-1)+arrayX[i-1];
				nXShortTicks++;
			}
		}
	}
	
	
	// In case
	/* draw the short ticks */
	for (i = 0; i < nXShortTicks + 1; i++){
		x0 = (int) trXX.Apply(theXShortTicks[i]);
		if (x0>x && x0<x+w-1) {
            DrawLine(x0,yShortTick,x0,yAxis);
            if (flagFrame) DrawLine(x0,yShortTick1,x0,yAxis1);
		}
	}
	
	/* Label the x axis */
	if (xText) {
        if (yMax<=yMin) y0 = yStrTick+4;
        else y0 = yStrTick-4;
        if (xMax<=xMin) {
            hstrMode = spJustifyHLeft;
            x0 = x +10;
        }
        else {
            hstrMode = spJustifyHRight;
            x0 = x+w -10;
        }
        DrawFormattedText(xText,hstrMode,x0,vstrMode,y0);
	}
	
	
	/*
	 * Draw in the y-axis
	 */
	
	/* Draw the axis line */
	if (xMax<xMin) {
		xAxis = x+w-1+pixelMargin;
		xAxis1 = x-pixelMargin;
	}
	else {
		xAxis = x-pixelMargin;
		xAxis1 = x+w-1+pixelMargin;
	}
    if (connectedView) {
        UseBrush(connectedView->GetBrush());
        UsePen(wxPen(connectedView->GetBrushColour()));
        DrawRectangle(xAxis,y-pixelMargin+1, connectedView->L2TopSign().x*pixelMargin, h-1+2*pixelMargin-2);
        UseAttributes();
    }
    DrawLine(xAxis,y-pixelMargin, xAxis, y+h-1+pixelMargin);
	if (flagFrame) 	{
        if (connectedView) {
            UseBrush(connectedView->GetBrush());
            UsePen(wxPen(connectedView->GetBrushColour()));
            DrawRectangle(xAxis1-connectedView->L2TopSign().x*pixelMargin,y-pixelMargin+1, connectedView->L2TopSign().x*pixelMargin, h-1+2*pixelMargin-2);
            UseAttributes();
        }
        DrawLine(xAxis1,y-pixelMargin, xAxis1, y+h-1+pixelMargin);
    }
	
	/* Draw the long ticks and label them */
	if (xMax<xMin) {
        hstrMode = spJustifyHLeft;
        if (!flagTicksIn) {
            xLongTick = (int) (xAxis+2*PXTICK);
            xShortTick = (int) (xAxis+PXTICK);
            xLongTick1 = (int) (xAxis1-2*PXTICK);
            xShortTick1 = (int) (xAxis1-PXTICK);
        }
        else {
            xLongTick = (int) (xAxis-2*PXTICK);
            xShortTick = (int) (xAxis-PXTICK);
            xLongTick1 = (int) (xAxis1+2*PXTICK);
            xShortTick1 = (int) (xAxis1+PXTICK);
		}
	}
	else {
        hstrMode = spJustifyHRight;
        if (!flagTicksIn) {
            xLongTick = (int) (xAxis-2*PXTICK);
            xShortTick = (int) (xAxis-PXTICK);
            xLongTick1 = (int) (xAxis1+2*PXTICK);
            xShortTick1 = (int) (xAxis1+PXTICK);
        }
        else {
            xLongTick = (int) (xAxis+2*PXTICK);
            xShortTick = (int) (xAxis+PXTICK);
            xLongTick1 = (int) (xAxis1-2*PXTICK);
            xShortTick1 = (int) (xAxis1-PXTICK);
        }
	}
    
	flagFirst = true;
	spMap1d trYY(yMin,y,yMax,y+h,trY.GetFlagLn(),trY.GetBase());
	double arrayY[100];
	int nArrayY = 1;
	for (i = 0; i < nYLongTicks + 1; i++){
		y0 = trYY.Apply(theYLongTicks[i]);
        if (y0<y || y0>y+h-1) continue;
		
		sprintf(tempStr, "%.8g",theYLongTicks[i]);
		
        if (!flagTicksIn) {
            if (xMax<xMin) xStrTick = xLongTick + 2;
            else xStrTick = xLongTick - 2;
        }
        else {
            if (xMax<xMin) xStrTick = xAxis + 2;
            else xStrTick = xAxis - 2;
		}
        yStrTick = y0;
		
        /* Draw the string if possible */
		ft.SetHVMode(hstrMode,spJustifyVMiddleUp);
		ft.SetText(tempStr);
        ft.SetAnchorPoint(spRealPoint(xStrTick,yStrTick));
		ft.Format(GetCanvas());
		if (flagFirst || !lastRect.Intersects(ft.boundingRect)) {
			DrawFormattedText(ft);
			DrawLine(xLongTick,y0,xAxis,y0);
			if (flagFrame) DrawLine(xLongTick1,y0,xAxis1,y0);
			arrayY[nArrayY] = theYLongTicks[i];
			nArrayY++;
			lastRect = ft.boundingRect;
			flagFirst = false;
		}
	}
	
	if (trYY.GetFlagLn()) { // Compute the short ticks
        double step = arrayY[2]/arrayY[1];
        arrayY[0] = arrayY[1]/step;
        arrayY[nArrayY] = arrayY[nArrayY-1]*step;
		nArrayY++;
		int x1 = (int) trYY.Apply(arrayY[1]);
		int x2 = (int) trYY.Apply(arrayY[2]);
		int nShort;
		if (x2-x1 > 100) nShort = 10;
		else nShort = 5;
		nYShortTicks = 0;
		for (int i = 1; i<nArrayY;i++) {
			for (int j=0;j<nShort;j++) {
				theYShortTicks[nYShortTicks] = (arrayY[i]-arrayY[i-1])*((double) j)/(nShort-1)+arrayY[i-1];
				nYShortTicks++;
			}
		}
	}
    
	
	/* draw the short ticks */
	for (i = 0; i < nYShortTicks + 1; i++){
		y0 = trYY.Apply(theYShortTicks[i]);
		if (y0>y && y0<y+h-1) {
            DrawLine(xShortTick,y0,xAxis,y0);
            if (flagFrame) DrawLine(xShortTick1,y0,xAxis1,y0);
		}
	}
	
	
	/* Label the y axis */
	if (yText) {
        if (xMax<xMin) x0 = xStrTick+2;
        else x0 = xStrTick-2;
        if (yMax<=yMin) {
            vstrMode = spJustifyVUp;
            y0 = y + 10;
        }
        else {
            vstrMode = spJustifyVDown;
            y0 = y+h - 10;
        }
        DrawFormattedText(yText,hstrMode,x0,vstrMode,y0);
	}
	
	
	/*
	 * Now do the title
	 */
	/*
     if (titleText) {
     if (flagReverse & YFlagReverse) y0 = y-5;
     else y0 = y+h+5;
     WDrawString((GOBJECT) win,titleText,spJustifyVMiddle,x+w/2,spJustifyVMiddle, y0,0);
     }
	 */
}

static void CBViewChangeBound(spInteractor *i,spEvent & event)
{
    spView *view = (spView *) event.sender;
    //	cout << "CHANGEBOUND" << endl;
    view->NotifyRectMustBeDrawn(view->container->gRect());
}


spAxisView::spAxisView(spView *connectedView1, unsigned long flags,const char *name) : spObject(connectedView1->container,spAnchorNone(),spMarginInfinite(), flags,name)
{
	flagTicksIn = true;
	flagFrame = true;
	pixelMargin = 3;
	connectedView = connectedView1;
    connectedView->Ref();
    SetBrush(*wxTRANSPARENT_BRUSH);
    
    // 1- We ask the connectView to send the axis a msge whenever it changes bound
    //    In order to do so we add to the view a SendMsge interactor
    // 2- Then we add an interactor to the axis so that it responds to this message
	if (connectedView1 != NULL) {
        connectedView1->AddInteractor(new spInteractorSendMsge(this,spMsgeSimple,spOnChangeBound));
        AddInteractor(new spInteractor1(CBViewChangeBound,spOnMsge,spMsgeSimple));
    }
}

spAxisView::~spAxisView()
{
    if (connectedView) connectedView->Unref();
}

void spAxisView::Draw(spRect &rect)
{
	if (!connectedView) return;
	spRealRect r = connectedView->_GetXYBound();
    //	cout << r << endl;
	
	double xMin = (connectedView->l2g.trX.GetFlagLn() ? log(r.x)/log(connectedView->l2g.trX.GetBase()) : r.x);
	double xMax = (connectedView->l2g.trX.GetFlagLn() ? log(r.x+r.width)/log(connectedView->l2g.trX.GetBase()) : r.x+r.width);
	double yMin = (connectedView->l2g.trY.GetFlagLn() ? log(r.y)/log(connectedView->l2g.trY.GetBase()) : r.y);
	double yMax = (connectedView->l2g.trY.GetFlagLn() ? log(r.y+r.height)/log(connectedView->l2g.trY.GetBase()) : r.y+r.height);
	
	ComputeTicks(xMin,xMax,yMin,yMax);
    
	if (connectedView->l2g.trX.GetFlagLn()) {
		// We round the long ticks and compute the smallest step;
		theXLongTicks[0] = ROUND(theXLongTicks[0]);
		int j=1;
		int step = INT_MAX;
        for (int i = 1; i < nXLongTicks + 1; i++) {
			if (ROUND(theXLongTicks[i]) == theXLongTicks[j-1]) continue;
			theXLongTicks[j] = ROUND(theXLongTicks[i]);
			step = MIN(step,fabs(theXLongTicks[j]-theXLongTicks[j-1]));
			j++;
		}
		nXLongTicks = j-1;
		// Now we recompute the Long ticks
		if (step == INT_MAX) step = 1;
		int min = MIN(theXLongTicks[0],theXLongTicks[nXLongTicks])-step;
		int max = MAX(theXLongTicks[0],theXLongTicks[nXLongTicks]);
		j = 0;
		for (int i=min;i<=max+step;i+=step) {
			theXLongTicks[j] = i;
			j++;
		}
		nXLongTicks = j-1;
        for (int i = 0; i < nXLongTicks + 1; i++) {
			theXLongTicks[i] = exp(log(connectedView->l2g.trX.GetBase())*theXLongTicks[i]);
		}
		nXShortTicks = -1;
	}
	
	if (connectedView->l2g.trY.GetFlagLn()) {
		theYLongTicks[0] = ROUND(theYLongTicks[0]);
		int j=1;
		int step = INT_MAX;
        for (int i = 1; i < nYLongTicks + 1; i++) {
			if (ROUND(theYLongTicks[i]) == theYLongTicks[j-1]) continue;
			theYLongTicks[j] = ROUND(theYLongTicks[i]);
			step = MIN(step,fabs(theYLongTicks[j]-theYLongTicks[j-1]));
			j++;
		}
		nYLongTicks = j-1;
		if (step == INT_MAX) step = 1;
		int min = MIN(theYLongTicks[0],theYLongTicks[nYLongTicks])-step;
		int max = MAX(theYLongTicks[0],theYLongTicks[nYLongTicks]);
		j = 0;
		for (int i=min;i<=max+step;i+=step) {
			theYLongTicks[j] = i;
			j++;
		}
		nYLongTicks = j-1;
        for (int i = 0; i < nYLongTicks + 1; i++) {
			theYLongTicks[i] = exp(log(connectedView->l2g.trY.GetBase())*theYLongTicks[i]);
		}
		nYShortTicks = -1;
	}
	
    spRect rr = connectedView->gRect();	
    DrawAxis_(rr.x,rr.y,rr.width,rr.height,r.x,r.x+r.width,r.y,r.y+r.height,"","","",pixelMargin,flagTicksIn,flagFrame,0,connectedView->l2top.trX,connectedView->l2top.trY);
}



spView *spAxisView::GetConnectedView()
{
    return connectedView;
}
bool spAxisView::GetFlagTicksIn()
{
    return flagTicksIn;
}
void spAxisView::SetFlagTicksIn(bool flag)
{
    AttBeforeUpdate();
    flagTicksIn = flag;
    AttAfterUpdate();;
}
int spAxisView::GetPixelMargin()
{
    return pixelMargin;
}
void spAxisView::SetPixelMargin(int pixelMargin1)
{
    AttBeforeUpdate();
    pixelMargin = pixelMargin1;
    AttAfterUpdate();;
}
bool spAxisView::GetFlagFrame()
{
    return flagFrame;
}
void spAxisView::SetFlagFrame(bool flag)
{
    AttBeforeUpdate();
    flagFrame = flag;
    AttAfterUpdate();;
}



