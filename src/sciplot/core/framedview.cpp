/*
 *  framedview.cpp
 *  SciPlot
 *
 *  Created by bacry on 29/06/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#include "sp.h"


static spAnchor titleAnchorFunc(spObject *obj)
{
	spFramedView *fv = (spFramedView *) obj->container;
		  
	spRealRect r = fv->GetViewObject()->lRect();

    return spAnchor(r.x+r.width/2,20);

}

static spAnchor labelxAnchorFunc(spObject *obj)
{
	spFramedView *fv = (spFramedView *) obj->container;
		
	spRealRect r = fv->GetViewObject()->lRect();
    
    return spAnchor(r.x+r.width/2,r.y+r.height+30);
}

static spAnchor labelyAnchorFunc(spObject *obj)
{
	spFramedView *fv = (spFramedView *) obj->container;
	   
	spRealRect r = fv->GetViewObject()->lRect();
    return spAnchor(r.x-33,r.y+r.height/2);
}

static spAnchor infoAnchorFunc(spObject *obj)
{
	spFramedView *fv = (spFramedView *) obj->container;
    spTextBox *tb = (spTextBox *) obj;
	
	spRealRect r = fv->lRect();
    return spAnchor(0,r.height-tb->GetFontSize()-10);
}

static spMargin infoMarginFunc(spObject *obj)
{
	spMargin margin;
	spFramedView *fv = (spFramedView *) obj->container;
    spTextBox *tb = (spTextBox *) obj;
	
	spRealRect r = fv->lRect();
    margin.Set(r.width,tb->GetFontSize()+10);
	
	return margin;
}


spFramedView::spFramedView(spContainer *ctn,const spAnchor &anchor,const spMargin &margin,unsigned long flags,const char *name1) : spGrid(ctn,anchor,margin,flags,name1)
{
	// We create the view
	leftMarginGrid = 80;
	rightMarginGrid =30;
	upMarginGrid = 40;
	downMarginGrid = 60;
	
    
	view = new spView(this,spAnchorGrid(0,0),spMarginGrid(1,1),(spFLAG_VIEW_KEEPBOUND_WHEN_RESIZED & flags)
                    | (spFLAG_VIEW_REVERSE_Y & flags),_CCHAR("view"));
	view->RemoveFlag(spFLAG_CONTAINER_DRAW_FRAME);
    view->RefAndConfig();
	
	title = new spText(this,spAnchorFunc(titleAnchorFunc,true),spJustifyHMiddle,spJustifyVBase,0,_CCHAR("title"));
	title->SetText("This is my Title");
    title->RefAndConfig();
	
	labelx = new spText(this,spAnchorFunc(labelxAnchorFunc,true),spJustifyHMiddle,spJustifyVBase,0,_CCHAR("labelx"));
	labelx->SetText("Label X");
    labelx->RefAndConfig();

	labely = new spText(this,spAnchorFunc(labelyAnchorFunc,true),spJustifyHMiddle,spJustifyVMiddle,0,_CCHAR("labely"));
	labely->SetAngle(90);
	labely->SetText("Label Y");
    labely->RefAndConfig();

	info = NULL;
	
	axis = new spAxisView(view,0,_CCHAR("axis"));
    axis->RefAndConfig();    
}

spFramedView::~spFramedView()
{
    view->Unref();
    title->Unref();
    labelx->Unref();
    labely->Unref();
    axis->Unref();
    if (info) info->Unref();
}

void spFramedView::SetInfo(const char *str)
{
	if (info) info->SetText(str);
}

const char *spFramedView::GetInfo()
{
	if (info) return(info->GetText());
    else return "";
}

void spFramedView::CreateInfoObject()
{
	if (!(info)) {
		info = new spTextBox(this,spAnchorFunc(infoAnchorFunc),spMarginFunc(infoMarginFunc),spJustifyHLeft,0,_CCHAR("info"));
		info->SetFont(10,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD);
		info->SetText("");
		info->SetFrame(false);
        info->RefAndConfig();
	}	
}