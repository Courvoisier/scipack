
// Not a module

/*
%exception {
    Py_BEGIN_ALLOW_THREADS
    $action
    Py_END_ALLOW_THREADS
}
 */


// If an error occurs in a director method which is implemented in python
// we need code to convert the error into an error in C++
%feature("director:except") {
	if ($error != NULL) {
		PrintPyError();
        throw "";
// I DON'T forget the error after printing it        throw Swig::DirectorMethodException();
	}
}

/* Does not seem to be useful
%typemap(throws) spError %{
  PyErr_SetString(PyExc_RuntimeError, $1.error);
  SWIG_fail;
%}
*/


