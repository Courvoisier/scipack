
#ifndef _SP_INTERACTOR_H
#define _SP_INTERACTOR_H

class spObject;

//
// The virtual class for interactors
//
// The events are processed by interactors. An interactor is implement in SciPlot using the Interactor class
// The spInteractor class is a virtual class that allows to derive 2 main classes for interactors
//
//   - spInteractor1 : the callback is a simple procedure associated to a specific event
//   - spInteractorN : this corresponds to a list of spInteractor (it allows to container interactors to manage consistency between them)
//
//
class spInteractor : public spRef 
{
	bool flagOn;

	public :

    static char *answerString;
    
    virtual bool IsInteractor() {return true;};

    // Check the object the interactor is added to is of valid class
	virtual bool CheckObjectIsValid(spObject *);
    
    // Interactor is for Views only
    virtual bool OnlyOnViews() { return false;};
    
    // Interactor is for Containers only
    virtual bool OnlyOnContainers() { return false;};
    
    // Notify an object has been linked/delinked to the interactor
	virtual void NotifyObjectAdded(spObject *o) {};
	virtual void NotifyObjectRemoved(spObject *o) {};
		
    // Playing with Off and On
	inline bool IsOn() {return flagOn;}      // Is the interactor switched On ?
	inline void OnTemp() {flagOn = true;}    // Turn the interactor temporarily on
	inline void OffTemp() {flagOn = false;}  // Turn the interactor temporarily off
	virtual void Off(){flagOn = false;};     // Turn it on
	virtual void On(){flagOn = true;};       // Turn it off
	
	// virtual method to process the interactor
	virtual bool Process(spInteractor *interactor,spEvent &event){return true;};
	bool Process(spEvent &event) {return Process(this,event);};	 // non virtual method
	
    
	virtual void NotifyObjectDeleted(spObject *) {}; // Not used ??

    virtual bool IsInteractorN() {return false;}

	// Basic constructor
	spInteractor(spObject *o=NULL,const char *name = NULL);
    virtual ~spInteractor(){};
	
	virtual const char *GetClassName() {
		return _CCHAR("spInteractor");
	}
};



//
//
// The spInteractor1 class implements a simple callback
//
// The callback is of class spInteractorCallback
//

class spInteractorCallback 
{
	public :
    
#ifdef SP_SCRIPT
    // The Python callback
	void *scriptFunc;
    void SetScriptFunc(void *ptr) {scriptFunc = ptr;}
#endif

    // The callback (a C++ function that can eventually trigger to the scriptFunc)
	void (*getFunc)(spInteractor *,spEvent &);
	inline bool IsSet() {return (getFunc != NULL);}
    void SetGetFunc(void (*ptr)(spInteractor *,spEvent &)) {getFunc = ptr;}
    void SetGetFunc(void (*ptr)(spInteractorCallback *, spInteractor *,spEvent &)) {getFunc = (void (*)(spInteractor *,spEvent &)) ptr;}
    
    // The method to call the call back (either C function or the script function)
    void Call(spInteractor *, spEvent &);

    // The constuctors/destructor
	spInteractorCallback(void (*func)(spInteractor *,spEvent &)=NULL);
	~spInteractorCallback();
	spInteractorCallback(const spInteractorCallback & icb);

	// The copy operator
    spInteractorCallback& operator =(const spInteractorCallback& other);
};


class spInteractor1 : public spInteractor {

	public :

    // The event type the callback corresponds to 
	int type;
	unsigned int modifiers;     // The modifier keys or the keycode in case of a spOnKeyDown (0 if any key is accepted)

    // If true then this interactor asks for propagation of the event
	bool flagPropagate;

    // The call back and the method to execute it
	spInteractorCallback  callback;
	void SetCallback(spInteractorCallback cb);
	inline void ExecuteCallback(spInteractor *i,spEvent & event) {
		if (!IsOn()) return;
		OffTemp(); // Prevent recursion ??? FAIRE UN try --> pb si on a une erreur !!!!!
		callback.Call(i,event);
		OnTemp();
        spEvent::Propagate(flagPropagate);
	}
	
	// The main method for processing the interactor
	virtual bool Process(spInteractor *interactor,spEvent &event);	

	// Set the event corresponding to this interactor
    void SetEventType(spEventType eventType) {
		type = eventType.type;
		modifiers = eventType.modifiers;
	};
	void SetEventType(int simpleType,unsigned long modifiers1) {
		type = simpleType;
		modifiers = modifiers1;
	};
	spEventType GetEventType() {
        return spEventType(type,modifiers);
	};

	
	// The constructor
	//  cb : the callback
	//  o  : the object the interactor is attached to
	//  e  : the event type
	//  m  : the modifier keys
	//  flagPropagate : if yes then interactor propagates the event upward.
	spInteractor1(spInteractorCallback cb,spEventType eventType, spObject *o = NULL, bool flagPropagate=false,const char *name = NULL);
	spInteractor1(void (*cb)(spInteractor *,spEvent &),spObject *o, int e, unsigned long m=0, bool flagPropagate=false,const char *name = NULL); // Hardly used --> to be removed !
	spInteractor1(void (*cb)(spInteractor *,spEvent &),int e, unsigned long m=0, bool flagPropagate=false,const char *name = NULL);
	spInteractor1(int e, unsigned long m=0, bool flagPropagate=false,const char *name = NULL);
	virtual ~spInteractor1();
		
	virtual const char *GetClassName() {
		return _CCHAR("spInteractor1");
	}	
};


//
// This class implements an interactor which groups a list of spInteractor
//
class spInteractorN : public spInteractor {
	
	public :
	
	// The null terminated list of spInteractor1
	spList<spInteractor,5,10> interactorList;
	

	// The process method
	virtual bool Process(spInteractor *interactor,spEvent &event);	
	
	// Constructor
	spInteractorN(spObject *o=NULL,const char *name = NULL);
	virtual ~spInteractorN();
	
    virtual bool IsInteractorN() {return true;}

	// Method to add an spInteractor1 in the list
	virtual void Add(spInteractor *);
	
	virtual const char *GetClassName() {
		return _CCHAR("spInteractorN");
	}		
};


// A simple Debug interactor that prints the received event
//
class spInteractorDebug : public spInteractor1 {
	
	public : 
	
	spInteractorDebug(spEventType e,bool flagPropagate=false,const char *name = NULL);
	
	virtual const char *GetClassName() {
		return _CCHAR("spInteractorDebug");		
	}		
};

extern ostream& operator << (ostream& o, spEvent &e);


#endif