


#include "script.h"
#include "ref.h"
#include <iostream>
using namespace std;
#include "debug.h"
#include "error.h"

#ifdef SPDEBUG_ALLOC
spList<spRef,2,10> spRef::allocList;
#endif


// Basic (private) method to add a reference
int spRef::AddRef() {
    SPDEBUG(spDebugRef, "AddRef(" << GetName() << ") : " << _nRefs << " --> " << _nRefs+1);
    _nRefs++;
    return _nRefs;
};


// Basic (private) method to delete a reference
int spRef::DelRef() {
    SPDEBUG(spDebugRef, "RemoveRef(" << GetName() << ") : " << _nRefs << " --> " << _nRefs-1);
    _nRefs--;
    return _nRefs;
}

// Public method to add a reference
//     flagScript indicates wether the reference is required by the script language or by C++
int spRef::Ref(bool flagScript)  {
    
#ifdef SP_SCRIPT
    
    //
    // ****** IMPORTANT NOTE : nRefs corresponds ONLY to referenced held in C++
    //
    
    //
    // Case the call is made from script (flagScript == true)
    // In that case there are basically two cases
    //    a- The object is not a director object
    //       --> in that case we will just add a reference and nothing else to do
    //           (we do not care about controlling the proxy script class since
    //            it is not used in C++)
    //    b- The object is a director
    //       then Ref(flagScript==true) is called at the creation of the object in the script language
    //       i.e., for a given object flagScript==true can happen only once
    //       THEN there are two cases again
    //           i- the C++ associated object is already referenced to in C++ (i.e., nRefs > 0)
    //              --> in that case we want C++ to control the deletion of the object
    //                  thus we SCRIPT_INCREF the script object so that deletion is never made by script
    //                  (it will be SCRIPT_DECREF later if nRefs goes back to 0)
    //           ii- the C++ associated object is not referenced to in C++ (i.e., nRefs == 0)
    //              --> We don't do anything special, we let the script language handling the reference
    //
    if (flagScript) {
        
        // If director get the corresponding object
        scriptobj = SCRIPT_GET_DIRECTOR(this);
        
        // We do something specials only for directors
        if (scriptobj) {
            // If we already have some reference in C we need to SCRIPT_INCREF to prevent
            // script language to delete the object
            if (_nRefs > 0) {
                SCRIPT_INCREF(scriptobj);
                SPDEBUG(spDebugRef, "AddScriptRef(" << GetName() << ") : "  << _nRefs);
            }
            return _nRefs;
        }
    }
    
    //
    // Case the call is made from C++ (e.g., a script defined object is added to a container
    // In that case, if it is the first C++ reference asked (i.e., nRefs==0) then we must SCRIPT_INCREF
    // (as explained above) and add the reference
    //
    else if (scriptobj) {
        if (_nRefs == 0) {
            SCRIPT_INCREF(scriptobj);
            SPDEBUG(spDebugRef, "AddScriptRef(" << GetName() << ") : "  << _nRefs);
            return AddRef();
        }
    }
#endif
    
    //
    // In all other cases (100% C++ or proxy non director object) we must just add a reference
    //
    return AddRef();
}


// Public method to delete a reference
//     flagScript indicates wether the reference is deleted by the script language or by C++
int spRef::Unref(bool flagScript) {
    
#ifdef SP_SCRIPT
    
    //
    // ****** IMPORTANT NOTE : nRefs corresponds ONLY to referenced held in C++
    //
    
    
    //
    // Case of a director object
    //
    if (scriptobj) {
        // CASE 1 : If it is called from script, there should not be any reference held in C++
        // (since if there is we SCRIPT_INCREF and consequently script cannot ask for deletion)
        // --> We must delete the C object
        if (flagScript) {
            if (_nRefs>0) printf("BUG\n");
            delete this;
            return 0;
        }
        
        // CASE 2 : If it is called from C++
        // --> if it is the last reference in C++ we need to release our reference in script
        // IMPORTANT : let us note that the call to SCRIPT_DECREF might ask for deletion of the object
        // In which case Unref will be called again from script and case 1- will be run
        else {
            if (_nRefs == 1) {
                DelRef();
                SPDEBUG(spDebugRef, "RemoveScriptRef(" << GetName() << ") : "  << _nRefs);
                SCRIPT_DECREF(scriptobj);
                return _nRefs;
            }
        }
    }
    
#endif
    
    //
    // In all other cases (100% C++ or proxy non director object) we must just remove a reference
    // and eventually ask for deletion of the C++ object
    //
    if (_nRefs == 0 || DelRef() == 0 ) {
        delete this;
        return 0;
    }
    
    return _nRefs;
}


int spRef::nRefs() {return _nRefs;}


spRef::spRef(bool flagRestrictedName, const char *name1) {
#ifdef SP_SCRIPT
    scriptobj = NULL;
#endif
    _nRefs = 0;
    name = NULL;
    _SetName(name1,flagRestrictedName);
#ifdef SPDEBUG_ALLOC
    allocList.Add(this);
#endif
    SPDEBUG(spDebugRef, "Allocation(" << (name ? name : "?") << ")");
}


spRef::~spRef()
{
    SPDEBUG(spDebugRef, "Desallocation(" << GetName() << ")");
    
    if (name) delete [] name;
    
#ifdef SPDEBUG_ALLOC
    allocList.Remove(this);
#endif
};


// Get default name from class name (static method)
const char *spRef::GetDefaultName(const char *klassName)
{
    static char n[1000];
    char *n1 = n;
    strcpy(n,klassName);
    if (strlen(klassName)>=2 && !strncmp(klassName,"sp",2)) n1+=2;
    if (n1[0] >= 65 && n1[0]<=90) n1[0]+= 'a'-'A';
    return n1;
}

// Get/Set the name
const char *spRef::GetName() {
    return (name ? name : GetDefaultName(GetClassName()));
};
void spRef::_SetName(const char *name1,bool flagRestrictedName)
{
    if (name1 == NULL) {
        if (name) delete [] name;
        name = NULL;
        return;
    }
        
    const char *n1 = name1;
    while (flagRestrictedName && name1 != NULL && *n1 != '\0') {
        if (*n1 < 32 || *n1 > 126 || *n1=='.') {
            spWarningf("spRef::SetName()","Invalid character for object name in '%s'. Name is unchanged.",name1);
            return;
        }
        n1++;
    }
        
    if (name) delete [] name;
    name = NULL;
    if (name1 != NULL) {
        name = new char[strlen(name1)+1];
        strcpy(name,name1);
    }
}
void spRef::SetName(const char *name1)
{
    _SetName(name1,!IsInteractor());
}

void spRef::RefAndConfig()
{
#ifdef SP_SCRIPT
    Ref();
    SCRIPT_Config(this);
#endif
}

void InfoAllocClass()
{
#ifdef SPDEBUG_ALLOC
    spRef::InfoAllocClass();
#endif
}

#ifdef SPDEBUG_ALLOC

void spRef::InfoAllocClass()
{
    const char *classes[SPDEBUG_ALLOC_NCLASSES];
    int numC[SPDEBUG_ALLOC_NCLASSES];
#ifdef SP_SCRIPT
    int numScript[SPDEBUG_ALLOC_NCLASSES];
    int numBoth[SPDEBUG_ALLOC_NCLASSES];
#endif
    int nClasses = 0;
    if (allocList.Size() == 0) {
        printf("*** 0 Allocation\n");
        return;
    }
    for (int i = 0;i<allocList.Size();i++) {
        const char *klass = allocList[i]->GetClassName();
        int j;
        for (j=0;j<nClasses; j++) {
            if (klass == classes[j]) break;
        }
        if (j==nClasses) {
#ifdef SP_SCRIPT
            numScript[nClasses] = 0;
            numBoth[nClasses] = 0;
#endif
            numC[nClasses] = 0;
            classes[nClasses] = klass;
            nClasses++;
        }
#ifdef SP_SCRIPT
        if (allocList[i]->IsReferencedByScript() && allocList[i]->IsReferencedByC()) numBoth[j]++;
        if (allocList[i]->IsReferencedByScript()) numScript[j]++;
        if (allocList[i]->IsReferencedByC()) numC[j]++;
#else
        numC[j]++;
#endif
    }
#ifdef SP_SCRIPT
    printf("*** ALLOC DEBUG INFO  [C] [SCRIPT]  [BOTH]\n");
#else
    printf("*** ALLOC DEBUG INFO\n");
#endif
    for (int j=0;j<nClasses; j++) {
#ifdef SP_SCRIPT
        printf("%20s : %d %d %d\n",classes[j],numC[j],numScript[j],numBoth[j]);
#else
        printf("%20s : %d\n",classes[j],numC[j]);
#endif
    }
}


#endif





void InfoAllocObjects()
{
#ifdef SPDEBUG_ALLOC
    spRef::InfoAllocObjects();
#endif
}

#ifdef SPDEBUG_ALLOC

void spRef::InfoAllocObjects()
{
    printf("*** ALLOC DEBUG INFO\n");
#ifdef SP_SCRIPT
    for (int i = 0;i<allocList.Size();i++) {
        const char *name = allocList[i]->GetName();
        const char *klass = allocList[i]->GetClassName();
        int nC = allocList[i]->nRefs();
        int nS = allocList[i]->IsReferencedByScript();
        printf("%20s %20s : %2d %s\n",name,klass,nC,(nS == 0 ? "" : "[script]"));
    }
#else
    for (int i = 0;i<allocList.Size();i++) {
        const char *name = allocList[i]->GetName();
        const char *klass = allocList[i]->GetClassName();
        int nC = allocList[i]->nRefs();
        printf("%20s %20s : %2d\n",name,klass,nC);
    }
#endif
}



#endif




