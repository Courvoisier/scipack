
// Not a module

%rename(Point) spPoint;
%rename(RealPoint) spRealPoint;
%rename(Rect) spRect;
%rename(RealRect) spRealRect;
%rename(Map1d) spMap1d;
%rename(Map2d) spMap2d;


/////////////////////////////
//
//
// Point class 
//
// This class is used to store a point with integer coordinates
//
//
/////////////////////////////

class spRealPoint;

%DocClass(spPoint,"sp.Point is a data structure used to store a point with integer coordinates. This class is very rarely used and will disappear in future versions.");
class spPoint 
{
	public : 

	int x,y;
	
	%extend {
	  std::string __repr__() {return self->ToStr(false);}
	  std::string __str__() {return self->ToStr(true);}
	}
	
	%extend {
		%DocMeth(Get,"Get() -> (x,y)",
                    "Returns the (x,y) as a tuple.");
		PyObject* Get() {
			PyObject* tup = PyTuple_New(2);
			PyTuple_SET_ITEM(tup, 0, PyInt_FromLong(self->x));
			PyTuple_SET_ITEM(tup, 1, PyInt_FromLong(self->y));
			return tup;
		}
	}

	// Constructors/Destructors
    %DocMeth(spPoint(),"","");
    %DocMeth(spPoint(int x,int y),"__init__(self) or __init__(self,x,y)","sp.Point is a data structure used to store a point with integer coordinates. This class is very rarely used and will disappear in future versions.");
	spPoint();
	spPoint(int x,int y);
	~spPoint();

	// Some useful operators
	bool operator == (spPoint pt2);
	bool operator != (spPoint pt2);
	spPoint operator + (spPoint pt2);
	spPoint operator - (spPoint pt2);
	spRealPoint operator / (spRealPoint pt2);
};
	

%newgroup



/////////////////////////////
//
//
// RealPoint class 
//
// This class is used to store a point with double coordinates
//
//
/////////////////////////////

//
// A typemap in order to perform implicit conversion
//      - spPoint --> spRealPoint
//
%typemap(in) spRealPoint {
    void *ptr;
    double x,y;
    if (SWIG_IsOK(SWIG_ConvertPtr($input, &ptr,SWIGTYPE_p_spRealPoint, 0 |  0 ))) {
        $1 = *((spRealPoint *) ptr);
    } else if (SWIG_IsOK(SWIG_ConvertPtr($input, &ptr,SWIGTYPE_p_spPoint, 0 |  0 ))) {
        $1 = *((spPoint *) ptr);
    }
    else if (PyArg_ParseTuple($input,"dd",&x,&y)) {
        $1 = spRealPoint(x,y);
    }
    else {
        PyErr_SetString(PyExc_ValueError,"Expected a spRealPoint.");
        return NULL;
    }
}
%typemap(typecheck,precedence=SWIG_TYPECHECK_POINTER) spRealPoint {
    if (SWIG_IsOK(SWIG_ConvertPtr($input, NULL,SWIGTYPE_p_spRealPoint, 0)) || SWIG_IsOK(SWIG_ConvertPtr($input, NULL,SWIGTYPE_p_spPoint, 0)) ||
        (PyTuple_Check($input) && PyTuple_Size($input)==2)) $1=1;
        else $1=0;
}


%DocClass(spRealPoint,"sp.RealPoint is a data structure used to store a point with double coordinates.");
class spRealPoint 
{
	public :
	
	double x,y;

	%extend {
	  std::string __repr__() {return self->ToStr(false);}
	  std::string __str__() {return self->ToStr(true);}
	}
	
	%extend {
		%DocMeth(Get,
						"Get() -> (x,y)",
						"Returns the (x,y) as a tuple.");
		PyObject* Get() {
			PyObject* tup = PyTuple_New(2);
			PyTuple_SET_ITEM(tup, 0, PyFloat_FromDouble(self->x));
			PyTuple_SET_ITEM(tup, 1, PyFloat_FromDouble(self->y));
			return tup;
		}
	}

	// Constructors/Destructors
    %DocMeth(spRealPoint(),"","");
    %DocMeth(spRealPoint(spPoint pt),"","");
    %DocMeth(spPoint(double x,double y),"__init__(self) or __init__(self,sp.Point) or __init__(self,x,y)","sp.RealPoint is a data structure used to store a point with double coordinates.")
	spRealPoint();
	spRealPoint(double x1,double y1);
	spRealPoint(spPoint pt);
	virtual ~spRealPoint();
	
    %DocMeth(Round(),"Round()","Rounds (in place) x y to integers");
    void Round();
    
    %DocMeth(Round(),"Rotate() -> sp.RealPoint","Returns point rotated around origin with angle");
    spRealPoint Rotate (double angle);

	// Some useful operators
	bool operator == (spRealPoint pt2);
	bool operator != (spRealPoint pt2);
	spRealPoint operator + (spRealPoint pt2);
	spRealPoint operator - (spRealPoint pt2);
	spRealPoint operator / (spRealPoint pt2);
};



        
        
%newgroup


/////////////////////////////
//
//
// Rect class 
//
// This class is used to store a rectangle with integer coordinates
//
//
/////////////////////////////


%DocClass(spRect,"sp.Rect is a data structure used to store a rectangle with integer coordinates. The width and height fields are always kept positive. This class will disappear in a future release.");

class spRect
{
public:

	// Fields
	int x, y, width, height;
		
	%extend {
	  std::string __repr__(void) {return self->ToStr(false);}
	  std::string __str__(void) {return self->ToStr(true);}
	}
	
	%extend {
		%DocMeth(Get,
						"Get() -> (x,y,width,height)",
						"Returns the (x,y,width,height) as a tuple.");
		PyObject* Get() {
			PyObject* tup = PyTuple_New(4);
			PyTuple_SET_ITEM(tup, 0, PyInt_FromLong(self->x));
			PyTuple_SET_ITEM(tup, 1, PyInt_FromLong(self->y));
			PyTuple_SET_ITEM(tup, 2, PyInt_FromLong(self->width));
			PyTuple_SET_ITEM(tup, 3, PyInt_FromLong(self->height));
			return tup;
		}
	}

	// Constructors
    %DocMeth(spRect(),"","");
    %DocMeth(spRect(const spPoint& pt1, const spPoint& pt2),"","");
    %DocMeth(spRect(int xx, int yy, int ww, int hh),"__init__(self) or __init__(self,point1,point2) or __init__(self,x,y,w,h)","sp.Rect is a data structure used to store a rectangle with integer coordinates. The width and height fields are always kept positive. This class will disappear in a future release.")
	spRect(const spPoint& pt1, const spPoint& pt2);
	spRect();
	spRect(int xx, int yy, int ww, int hh);
	
	//
	// Other methods
	//
	
	%DocMeth(GetPosition,"GetPosition() -> sp.Point","Returns (x,y) corner");
	spPoint GetPosition();
	%DocMeth(SetPosition,"SetPosition(sp.Point)","Sets the corner (x+width,y+height)");
	void SetPosition( const spPoint &p);
	%DocMeth(GetPosition1,"GetPosition1() -> sp.Point","Returns the point opposite to (x,y)");
	spPoint GetPosition1();
	
	
	%DocMeth(IsEmpty,"IsEmpty() -> bool","Returns True if sp.Rect is empty");
	bool IsEmpty();
	
	%DocMeth(Inflate,"Inflate() -> sp.Rect","Returns a rectangle corresponding to self increased by dx,dy in all directions");
	spRect Inflate(int dx, int dy);	
	%DocMeth(Deflate,"Deflate() -> sp.Rect","Returns a rectangle corresponding to self decreased by dx,dy in all directions");
	spRect Deflate(int dx, int dy);
	
	%DocMeth(Intersect,"Intersect(sp.Rect) -> sp.Rect","Returns the intersection of self with a given sp.Rect");
	spRect Intersect(const spRect& rect) const;

	%DocMeth(Intersects,"Intersects(sp.Rect) -> bool","Tests whether self intersects a given rectangle");
	bool Intersects(const spRect& rect) const;

	%DocMeth(Union,"Union(sp.Rect) -> sp.Rect","Returns the union of self with a given point");
	spRect Union(const spPoint& pt);	
	%DocMeth(Union,"Union(sp.Rect) -> sp.Rect","Returns the union of self with a given sp.Rect");
	spRect Union(const spRect& rect) const;
	
	%DocMeth(Contains,"","");
	bool Contains(double x, double y) const;
	%DocMeth(Contains,"Contains(sp.RealPoint) or Contains(x,y) -> bool","Tests whether self contains a given point");
	bool Contains(const spRealPoint pt) const;
	
	// Some operators
	bool operator==(const spRect& rect) const;
	bool operator!=(const spRect& rect) const;
};

%newgroup


/////////////////////////////
//
//
// RealRect class 
//
// This class is used to store a rectangle with double coordinates
//
//
/////////////////////////////



//
// A typemap in in order to perform implicit conversion
//      - spRect --> spRealRect
//
%typemap(in) spRealRect {
    void *ptr;

    if (SWIG_IsOK(SWIG_ConvertPtr($input, &ptr,SWIGTYPE_p_spRealRect, 0 |  0 ))) {
        $1 = *((spRealRect *) ptr);
    } else if (SWIG_IsOK(SWIG_ConvertPtr($input, &ptr,SWIGTYPE_p_spRect, 0 |  0 ))) {
        $1 = *((spRect *) ptr);
    }
    else {
        PyErr_SetString(PyExc_ValueError,"Expected a spRealRect.");
        return NULL;
    }
}
%typemap(typecheck,precedence=SWIG_TYPECHECK_POINTER) spRealRect {
    if (SWIG_IsOK(SWIG_ConvertPtr($input, NULL,SWIGTYPE_p_spRealRect, 0)) || SWIG_IsOK(SWIG_ConvertPtr($input, NULL,SWIGTYPE_p_spRect, 0))) $1=1;
        else $1=0;
            }


%DocClass(spRealRect,"sp.RealRect is a  data structure used to store a rectangle with double coordinates. The fields width and height are standardized to be >=0.");
class spRealRect
{
	public : 
	
	// Fields
	double x, y, width, height;
	
	
	%extend {
	  std::string __repr__() {return self->ToStr(false);}
	  std::string __str__() {return self->ToStr(true);}
	}

	%extend {
		%DocMeth(Get,
						"Get() -> (x,y,width,height)",
						"Returns the (x,y,width,height) as a tuple.");
		PyObject* Get() {
			PyObject* tup = PyTuple_New(4);
			PyTuple_SET_ITEM(tup, 0, PyFloat_FromDouble(self->x));
			PyTuple_SET_ITEM(tup, 1, PyFloat_FromDouble(self->y));
			PyTuple_SET_ITEM(tup, 2, PyFloat_FromDouble(self->width));
			PyTuple_SET_ITEM(tup, 3, PyFloat_FromDouble(self->height));
			return tup;
		}
	}

	// Constructor
    %DocMeth(spRealRect(),"","");
    %DocMeth(spRealRect(const spRealPoint& pt1, const spRealPoint& pt2),"","");
    %DocMeth(spRealRect(spRect rect),"","");
    %DocMeth(spRealRect(double xx, double yy, double ww, double hh),"__init__(self) or __init__(self,point1,point2) or __init__(self,x,y,w,h) or __init__(self,sp.Rect)","sp.RealRect is a  data structure used to store a rectangle with double coordinates. The fields width and height are standardized to be >=0 in which case the point (x,y) is referred to as the 'base corner'.")
 	spRealRect();
	spRealRect(double xx, double yy, double ww, double hh);
	spRealRect(const spRealPoint& topLeft, const spRealPoint& bottomRight);
	spRealRect(const spRect rect);
	~spRealRect();
	
	// spRealRect --> spRect
	%DocMeth(Round,"Round() -> sp.RealRect","Returns the closest sp.Rect rectangle");
	spRect Round();
	
	
	%DocMeth(GetPosition,"GetPosition() -> sp.RealPoint","Returns the base corner.");
	spRealPoint GetPosition();
    
	%DocMeth(SetPosition,"SetPosition(sp.RealPoint)","Sets the base corner");
	void SetPosition( const spRealPoint &p);
    
	%DocMeth(GetPosition1,"GetPosition1() -> sp.RealPoint","Returns the oposite corner to the base corner");
	spRealPoint GetPosition1();
	
	%DocMeth(IsEmpty,"IsEmpty() -> bool","Returns True if sp.RealRect is empty");
	bool IsEmpty();
	
	%DocMeth(Inflate,"Inflate() -> sp.RealRect","Returns a rectangle corresponding to self increased by dx,dy in all directions");
	spRealRect Inflate(double dx, double dy);	
	%DocMeth(Inflate,"Deflate() -> sp.RealRect","Returns a rectangle corresponding to self decreased by dx,dy in all directions");
	spRealRect Deflate(double dx, double dy);

	%DocMeth(Intersect,"Intersect(sp.RealRect) -> sp.RealRect","Returns the intersection of self with a given sp.RealRect");
	spRealRect Intersect(const spRealRect& rect) const;
	
	%DocMeth(Intersects,"Intersects(sp.RealRect) -> bool","Tests whether self intersects a given rectangle");
	bool Intersects(const spRealRect& rect) const;
	
	%DocMeth(Union,"Union(sp.RealRect) -> sp.RealRect","Returns the union of self with a given point");
	spRealRect Union(const spRealPoint& pt);	
	%DocMeth(Union,"Union(sp.RealRect) -> sp.RealRect","Returns the union of self with a given point");
	spRealRect Union(const spRealRect& rect);

	%DocMeth(Contains,"","");
	bool Contains(double x, double y) const;
	%DocMeth(Contains,"Contains(sp.RealPoint) or Contains(x,y) -> bool","Tests whether self contains a given point");
	bool Contains(const spRealPoint& pt) const;
	
	// Some operators
	bool operator==(const spRealRect& rect) const;
	bool operator!=(const spRealRect& rect) const;	
};



%newgroup

/////////////////////////////	
//
// class spMap1d 
//
// This class implements the local 2 topcontainer transform for either x or y coordinates
//
// It has two possible forms
//
//    i)  linear form : ax+b
//    ii) log form : a*ln(c*x+d)/ln(base)+b
//
/////////////////////////////

%DocClass(spMap1d,
"sp.Map1d implements a change in 1d coordinate.
It has two possible forms : 
    - a linear form ax+b and 
    - a log form a*ln(c*x+d)/ln(base)+b");

class spMap1d 
{	
  public :
	
	%extend {
	  std::string __repr__() {return self->ToStr(false);}
	  std::string __str__() {return self->ToStr(true);}
	}

	%extend {
        %pythoncode %{
            def __call__(self,val):
                """Applies map to val"""
                return self.Apply(val)
            def Inv(self,val):
                """Applies inverse map to val"""
                return self.IApply(val)
        %}
    }

	%DocMeth(GetBase,"GetBase() -> base","Returns the base of the logarithm used for the log form a*ln(c*x+d)/ln(base)+b");
	inline double GetBase() const;

	%DocMeth(GetFactor,"GetFactor() -> a factor","Returns the a factor of both forms ax+b or a*ln(c*x+d)/ln(base)+b");
	inline double GetFactor() const;
	
	%DocMeth(GetFlagLn,"GetFlagLn() -> bool","Returns true if the form is the log form a*ln(c*x+d)/ln(base)+b");
	inline bool GetFlagLn() const {return flagLn;}
	
	// Constructor
	%DocMeth(spMap1d(double x1,double y1, double x2, double y2, bool flagLn=false,double baseLn=10),"","");
	%DocMeth(spMap1d(),"","");
	%DocMeth(spMap1d(double a,double b),"__init__() or __init__(a,b) or __init__(x1,y1,x2,y2)",
"sp.Map1d implements a 1 dimensionnal map. It has two possible forms :
    - a linear form ax+b and
    - a log form a*ln(c*x+d)/ln(base)+b
In the third form, the map is a linear map which maps x1 on x2 and y1 on y2.
");

	spMap1d();
	spMap1d(double a,double b);
	spMap1d(double x1,double y1, double x2, double y2, bool flagLn=false,double baseLn=10);
	~spMap1d();
	
	%DocMeth(SetLnScale(),"SetLnScale(bas=10,c=1,d=0)","Sets log form parameter");
	void SetLnScale(double base=10,double c=1,double d=0);

	%DocMeth(ComposeRight,"ComposeRight(map) -> sp.Map1d","Computes map composition (self o map)");
	spMap1d ComposeRight(const spMap1d &tr);

	%DocMeth(ComposeLeft,"ComposeLeft(map) -> sp.Map1d","Computes map composition (map o self)");
    spMap1d ComposeLeft(const spMap1d &tr);
	
	%DocMeth(operator +,"self+x","Returns translation of self");
	spMap1d operator+(const double x);
};

%newgroup


/////////////////////////////	
//
// class spMap2d 
//
// This class implements the local 2 canvas transform for both x and y coordinates
// It mainly includes two spMap1d objects
//
/////////////////////////////	


%DocClass(spMap2d,"sp.Map2d implements a 2 dimensionnal map. It is made of two spMap1d object");

class spMap2d 
{
  public :
	
	%extend {
	  std::string __repr__() {return self->ToStr(false);}
	  std::string __str__() {return self->ToStr(true);}
	}

	%extend {
        %pythoncode %{
            def __call__(self,point_or_rect):
                """Returns the map applied to the point or rect"""
                return self.Apply(point_or_rect)
            def Inv(self,point_or_rect):
                """Returns the inverse of the map applied to the point or rect"""
                return self.IApply(point_or_rect)
        %}
    }

	%DocMeth(spMap2d(const spMap1d &trx1,const spMap1d &try1),"","");
	%DocMeth(spMap2d(),"__init__(self) or __init(self,x_map1d,y_map1d)","sp.Map2d implements a 2 dimensionnal map. It is made of two spMap1d object.");
	spMap2d(const spMap1d &trx1,const spMap1d &try1);
	spMap2d();
	~spMap2d();
	
	%DocMeth(ComposeLeft,"ComposeLeft(map) -> sp.Map2d","Computes composition (map o self)");
	spMap2d ComposeLeft(const spMap2d &tr);
	
	%DocMeth(ComposeRight,"ComposeRight(map) -> sp.Map2d","Computes composition (self o map)");
	spMap2d ComposeRight(const spMap2d &tr);
	
	%DocMeth(operator +,"self+point","Returns translation of self");
	spMap2d operator+(const spRealPoint &pt);
};


//
// The x and y atributes
//

//
// name field (read/write)
//
%extend spMap2d {
%immutable;
  spMap1d x;
  spMap1d y;
%mutable;
}
%{
  spMap1d * spMap2d_x_get(spMap2d *s) {return new spMap1d(s->trX);}
  spMap1d * spMap2d_y_get(spMap2d *s) {return new spMap1d(s->trY);}
%}

