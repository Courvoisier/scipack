/*
 *  framedview.h
 *  SciPlot
 *
 *  Created by bacry on 29/06/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _SP_AXIS_H_
#define _SP_AXIS_H_


class spAxisView : public spObject
{
	private : 
	void DrawAxis_(int x, int y, int w, int h, double xMin, double xMax, double yMin, double yMax, 
								 const char *xText,const char * yText,const char * titleText,int pixelMargin, char flagTicksIn, char flagFrame,char flagReverse,spMap1d &trX,spMap1d &trY);

	bool flagTicksIn;
	int pixelMargin;
	bool flagFrame;

	spView *connectedView;

	public :
	
    
	
	spAxisView(spView *connectedView=NULL, unsigned long flags = 0,const char *name=NULL);
    virtual ~spAxisView();
    
    bool GetFlagTicksIn();
    void SetFlagTicksIn(bool flag);
    
    int GetPixelMargin();
    void SetPixelMargin(int pixelMargin);

    bool GetFlagFrame();
    void SetFlagFrame(bool flag);

    spView *GetConnectedView();

	virtual void Draw(spRect &rect);
	
	virtual double GetDistance(spRealPoint pt) {
		return -1;
	}
	
	virtual const char *GetClassName() {
		return _CCHAR("spAxisView");
	}		
};




#endif