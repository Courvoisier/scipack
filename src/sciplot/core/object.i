
// Not a module

%rename(Object) spObject;
%rename(OverlayNone) spOverlayNone;
%rename(OverlayBuffer) spOverlayBuffer;

%rename(_SetPen) SetPen;
%rename(_SetPenStyle) SetPenStyle;
%rename(_SetPenWidth) SetPenWidth;
%rename(_SetPenColour) SetPenColour;
%rename(_GetPen) GetPen;
%rename(_GetPenWidth) GetPenWidth;
%rename(_GetPenStyle) GetPenStyle;
%rename(_GetPenColour) GetPenColour;


%rename(_GetWhiteBrushPrinting) GetWhiteBrushPrinting;
%rename(_SetWhiteBrushPrinting) SetWhiteBrushPrinting;
%rename(_SetBrush) SetBrush;
%rename(_SetBrushColour) SetBrushColour;
%rename(_GetBrushColour) GetBrushColour;
%rename(_SetBrushStyle) SetBrushStyle;
%rename(_GetBrush) GetBrush;
%rename(_GetBrushStyle) GetBrushStyle;

%rename(_GetTextForeground) GetTextForeground;
%rename(_SetTextForeground) SetTextForeground;
%rename(_GetTextBackground) GetTextBackground;
%rename(_SetTextBackground) SetTextBackground;

%rename(_SetFont) SetFont;
%rename(_SetFontStyle) SetFontStyle;
%rename(_SetFontSize) SetFontSize;
%rename(_GetFontSize) GetFontSize;
%rename(_GetFontStyle) GetFontStyle;
%rename(_SetFontFamily) SetFontFamily;
%rename(_GetFontFamily) GetFontFamily;
%rename(_SetFontWeight) SetFontWeight;
%rename(_GetFontWeight) GetFontWeight;
%rename(_SetFontFaceName) SetFontFaceName;
%rename(_GetFontFaceName) GetFontFaceName;


%feature("ref")   spObject "$this->Ref(true);"
%feature("unref") spObject "$this->Unref(true);"


%DocMeth(InfoAllocClass,"InfoAllocClass()","Prints allocation info on the different graphic objects and interactors classes. For getting more info you should use InfoAllocObjects().")
extern void InfoAllocClass();

%DocMeth(InfoAllocObjects,"InfoAllocObjects()","Prints allocation info on the different graphic objects and interactors. It is much more detailed than InfoAllocClass().")
extern void InfoAllocObjects();


typedef enum spOverlayType_ {
	spOverlayNone = 0,
	spOverlayBuffer
} spOverlayType;


%feature("director") spObject;


// Defining the __str__ and __repr__ methods
%define ObjRepr
%extend {
    %pythoncode %{
        def __repr__(self):
            return self.__spclassname__()+'('+self.fullname()+')'
        def __str__(self):
            return self.__spclassname__()+'('+self.name+')'
    %}
}
%enddef




%{
	//
    // This function calls the cursor script function
	//
	const char *callbackCursorScriptFunc(spCursorCallback *icb, spObject *object, bool flag, spRealPoint &pt)
	{
		// Get the python callback
		PyObject *callback = (PyObject *) icb->scriptFunc;
		if (callback == NULL) return ""; // should never happen
        
		// Get the proxy instance associated to object (it adds a reference)
		PyObject *self = WrapObject(object);
		if (self == NULL) return ""; // Should never happen
        
        spRealPoint gpt = object->container->l2top.IApply(pt);

		// Make the python call, get the result, manage eventual errorin the function
		spRealPoint *pt1 = new spRealPoint(gpt);
		PyObject *PyPt = SWIG_NewPointerObj(SWIG_as_voidptr(pt1),SWIGTYPE_p_spRealPoint, SWIG_POINTER_OWN |  0 );
		PyObject *arglist = Py_BuildValue("OOO", self,(flag ? Py_True : Py_False),PyPt);
		PyObject* result = PyObject_CallObject(callback, arglist);
        
		Py_XDECREF(arglist);
        
        // So now we have to delete the reference we created calling WrapObject
        Py_XDECREF(self); // If not a director, it will Unref automatically and the object is going to be destroyed !
		Py_XDECREF(PyPt);
        
		if (result == NULL) {
			PrintPyError();
			return "";
		}
        
		// Extract the char * and new point from the result
		// Generate an error if it is not
		char *str;
        double x,y;
		int ok = PyArg_ParseTuple(result,"sdd",&str,&x,&y);
		Py_XDECREF(result);
		if (ok) {
            pt = object->container->l2top.ApplyInt(spRealPoint(x,y));
			return str;
		}
        
		spWarningf("","Error in sp.Object.cursorCallback : A python cursor call back function should return a a t_uple of the form (string,x,y)\n");
		PyErr_Clear();
		return "";
	};
    %}

	extern const char * callbackCursorScriptFunc(spCursorCallback *self,spObject *object, bool flag, spRealPoint &pt);
%typemap(in)  spCursorCallback {
    // Check that the argument is a callable
    if (!PyCallable_Check($input)) {
        PyErr_SetString(PyExc_RuntimeError,"Argument should be a callable");
        return(NULL);
    }
    Py_XINCREF($input);
    $1.SetScriptFunc($input);
    
    // Then set the getFunc
	$1.SetGetFunc(callbackCursorScriptFunc);
}


// We set the kwargs for some methods
%feature("kwargs") spObject::spObject;

//
// The spObject class
//
%rename(FLAG_CONTAINER_HIDE) spFLAG_CONTAINER_HIDE;
%rename(FLAG_VIEW_KEEPBOUND_WHEN_RESIZED) spFLAG_VIEW_KEEPBOUND_WHEN_RESIZED;
%rename(FLAG_CONTAINER_DRAW_FRAME) spFLAG_CONTAINER_DRAW_FRAME;
%rename(FLAG_VIEW_REVERSE_X) spFLAG_VIEW_REVERSE_X;
%rename(FLAG_VIEW_REVERSE_Y) spFLAG_VIEW_REVERSE_Y;
%rename(FLAG_OBJECT_OVERLAY_BUFFER) spFLAG_OBJECT_OVERLAY_BUFFER;

%DocClass(spObject,"This is the class any sciplot graphic objects should derive from. The bounding box of the object is defined by the anchor and the margin. The flag can be any combination of valid flags for the corresponding class. For basic class sp.Object, the only valid value is sp.FLAG_OBJECT_OVERLAY_BUFFER for specifying that the object is an overlay object. Other values are valid for other classes (see for instance the doc son sp.Container)")
class spObject
{
	public :
	
    // This is the constructor
	spObject(spContainer *container=NULL,const spAnchor &anchor=spAnchor(),const spMargin &margin=spMargin(),unsigned long flags=0x0,const char *name=NULL);
	virtual ~spObject();

    // A Python method to get the full name of the object
    %extend {
        %pythoncode %{
            def fullname(self):
                """Gets fullname of the object"""
                if (self.IsTopContainer()):
                    return self.name
                if (self.container is None):
                    return '#:'+self.name
                return self.container.fullname()+':'+self.name
        %}
    }

    // A method to get the class name
    // It works for both python defined objects and C++ defined objects
    %extend {
      const char *__spclassname__() {
        Swig::Director *director = SWIG_DIRECTOR_CAST($self);
        const char *klass;
        if (director) {
            PyObject *obj = (PyObject *) director->swig_get_self();
            klass = obj->ob_type->tp_name;
        }
        else klass = self->GetClassName();
        return klass;
      }
    }
    
    // The string representation in python
    ObjRepr;

    //
    // Dealing with attributes
    // (the _att_... commands are defined in the file _sputils.py)
    //
    %extend {
        %pythoncode %{
            def __call__(self,pattern=None,get_attribute=None,update=True,**kwargs):
                """Allows to set/get attribute values of the self object (or any other object reached from self)
                    This function is designed so that interaction on the terminal is easy.
            
                    --> If pattern is the only argument :
                      * If it ends with ':'  it refers to a container --> returns the list of objects of this container
                      * If not, the pattern refers to some objects    --> returns the attribute list of the first found object
                    --> If get_attribute is specified
                      returns the value of the attribute of the first object found matching the pattern
                    --> Otherwise, kwargs corresponds to all the attributes to be set for ALL matching opjects
                      Redraw is performed if update is True"""

                if (pattern and get_attribute is None):
                    if (pattern[0] != '^' and not (':' in pattern)):
                        get_attribute = pattern
                        pattern = None

                if (len(kwargs)==0):
                    if (get_attribute is None):
                        if (pattern is None):
                            print "Attributes of %s" % self.__repr__()
                            print _att_list(self)
                            return self
                        if (pattern!='' and pattern[-1] == ':'):
                            objs=match_obj_name(pattern+'.*',obj=self)
                            if (len(objs) == 1): return objs[0]
                            return objs
                        else:
                            objs = match_obj_name(pattern,obj=self)
                            if (len(objs) == 0): return []
                            print "Attributes of %s" % objs[0].__repr__()
                            print _att_list(objs[0])
                            return objs[0]
                    else:
                        if (pattern is None): return _att_get(self,get_attribute)
                        objs=match_obj_name(pattern,obj=self)
                        if (len(objs) == 0): return
                        return _att_get(objs[0],get_attribute)
                else:
                    if (pattern is None):
                        return _att_set(self,update,**kwargs)
                    else:
                        objs=match_obj_name(pattern,obj=self)
                        if (len(objs)==0): return
                        return _att_set(objs[0],update,**kwargs)                
                    
            def ahelp(self,attribute=None,flag_print=True,flag_short=True):
                """
                Allows to get/print help on all the attributes or a single attribute of the object
                    
                    - attribute  : if not None then just ask the help on the specified attribute
                    - flag_print : if True, then the help is printed (and not 'just' returned)
                    - flag_short : if True, a short version of the help is asked for
                """
                return _att_help(self,attribute,flag_print,flag_short)
                    
            def alist(self):
                """Allows to get the list of the attributes of the object"""
                return _att_list(self)

            def config(self,*args,**kwargs):
                """Configures the object using the config default file .spconfig"""
                Config.config(self)
                if (len(kwargs)!=0): self(update=False,**kwargs)
                return self

            def oget(self,pattern="::.*",klass=None):
                """Gets the list of objects inside self (which should be a container) whose fullname (truncated to the level hierarchy of self) matches the pattern and the klass."""
                return match_obj_name(pattern,self,klass)
            def oget1(self,pattern="::.*",klass=None):
                """Same as self.oget except that the first found element is returned"""
                objects = match_obj_name(pattern,self,klass)
                if (len(objects)>0): return objects[0]
                return None
        %}
    }

    %DocMeth(SetCursorCallback,"SetCursorCallback(spCursorCallback cb)","The callback should have 3 arguments : the object the cursor is in, a flag indicating if a short text shoult be returned and the mouse point")
    void SetCursorCallback(spCursorCallback cb);

    // Simple tests
    %DocMeth(IsContainer,"IsContainer() -> bool","Returns True if object is a sp.Container (or any class which derives from it)")
    bool IsContainer();
    %DocMeth(IsGrid,"IsGrid() -> bool", "Returns True if object is a sp.Grid (or any class which derives from it)")
    bool IsGrid();
    %DocMeth(IsView,"IsView() -> bool", "Returns True if object is a sp.View (or any class which derives from it)")
	bool IsView();
    %DocMeth(IsTopContainer,"IsTopContainer() -> bool", "Returns True if object is a sp.TopContainer (or any class which derives from it)")
	bool IsTopContainer();
    %extend {
        bool _IsInteractor() {return false;}
    }
	
    // Dealing with rects
//    %DocMeth(gBoundingRect,"gBoundingRect() -> sp.Rect", "Returns the bounding rectangle of the object corresponding the anchor+margin inflated by the margin->pixel and the pen size","")
//    %extend {spRect gBoundingRect() {return $self->gBoundingRect();}}
    %DocMeth(gRect,"gRect() -> sp.Rect", "Returns the rectangle associated to the object, i.e.,  corresponding to its anchor and margin. It is expressed in topcontainer's coordinate.")
    %extend {spRect gRect() {return $self->gRect();}}

    %DocMeth(lRect,"lRect() -> sp.RealRect", "Same as self.gRect() but the rectangle is expressed in container's coordinate.")
    spRealRect lRect();

    
    %DocMeth(GetDistance,"GetDistance(sp.RealPoint pt) -> double", "Returns the distance (computed in pixels) between object and the point pt. Returns -1 if pt is considered too far.")
	virtual double GetDistance(spRealPoint pt);
	
    %DocMeth(IsHidden,"IsHidden() -> bool", "Returns True if object is hidden.")
	bool IsHidden();

    %DocMeth(IsVisible,"IsVisible() -> bool", "Returns True if object is visible, i.e., if none of its hierarchy (including object) is hidden.")
    bool IsVisible();

    %DocMeth(Hide,"Hide() -> bool", "Hides object. Returns True if object was visible before being hidden.")
    bool Hide();
    %DocMeth(Show,"Show()", "Shows object")
	void Show();
    
    %DocMeth(IsOverlay,"IsOverlay() -> bool", "Returns True if object is an Overlay object.")
	bool IsOverlay();
    %DocMeth(IsOverlayActivated,"IsOverlayActivated() -> bool", "Returns True if object is an Overlay object and if it is activated.")
	bool IsOverlayActivated();
    %DocMeth(ActivateOverlay,"ActivateOverlay()", "Activate object if it is an Overlay object (otherwise do nothing).")
    void ActivateOverlay();
    %DocMeth(DeactivateOverlay,"DeactivateOverlay()", "Deactivate object if it is an Overlay object (otherwise do nothing).")
    void DeactivateOverlay();

    %DocMeth(IsMoveable,"IsMoveable() -> bool", "Returns True if object is moveable.")
	virtual bool IsMoveable();
    %DocMeth(Move(double dx,double dy),"Move(double dx,double dy)", "Moves object with dx and dy (expressed in container's coordinate)")
	virtual void Move(double dx,double dy);
    %DocMeth( Move(spRealPoint dv),"Move(sp.RealPoint dv)", "Moves object with dv (expressed in container's coordinate)")
	void Move(spRealPoint dv);
    
//    inline bool IsResizeable();
  
    %DocMeth(Draw,"Draw(sp.Rect rect)", "Draws object (drawing has been clipped in rect). This method should be overloaded.")
	virtual void Draw(spRect &rect);

    %DocMeth(AttBeforeUpdate,"AttBeforeUpdate()", "Method to be called before an attribute of the object is changed")
    virtual void AttBeforeUpdate();
    %DocMeth(AttAfterUpdate,"AttAfterUpdate()", "Method to be called after an attribute of the object has been changed")
    virtual void AttAfterUpdate();

/*
	bool IsMarginInfinite();
	bool IsMarginGrid();
	bool IsMarginPixelSize();
	bool IsMarginEmpty();
*/


    //    void DrawPolygon(int n, spRealPoint *points);
    
    
    %DocMeth(DrawArrowTip,"DrawArrowTip(tipPoint,angle,length=5,alpha=30)", "(To be called within the Draw method only) Draws an arrow tip at a given point and angle. The arrow has given length and an angle at the tip which is alpha. Everything is expressed in topcontainer's coordinates.")
	void DrawArrowTip(spRealPoint tip, double angle, double length = 5, double alpha=30);
    %DocMeth(DrawCEllipse(double x, double y, double w, double h),"DrawCEllipse(x,y,w,h) or DrawCEllipse(pt,w,h=-1)", "(To be called within the Draw method only) Draws a centered ellipse (topcontainer's coordinates are used).")
    void DrawCEllipse(double x, double y, double w, double h);
    %DocMeth(DrawCEllipse(spRealPoint pt, double w, double h=-1),"", "")
	void DrawCEllipse(spRealPoint pt, double w, double h=-1);
    %DocMeth(DrawLine,"DrawLine(pt1,pt2)", "(To be called within the Draw method only) Draws a line (topcontainer's coordinates are used).")
    void DrawLine(spRealPoint pt1, spRealPoint pt2);

    %DocMeth(DrawRectangle(spRect &r),"DrawRectangle(rect) or DrawRectangle(x,y,w,h)", "(To be called within the Draw method only) Draws a rectangle (topcontainer's coordinates are used).")
	void DrawRectangle(spRect &r);
    %DocMeth(DrawRectangle(double x, double y, double w, double h),"", "")
	void DrawRectangle(double x, double y, double w, double h);
    
    %DocMeth(DrawEllipse(spRect &r),"DrawEllipse(rect) or DrawEllipse(x,y,w,h)", "(To be called within the Draw method only) Draws an ellipse (topcontainer's coordinates are used).")
	void DrawEllipse(spRect &r);
    %DocMeth(DrawEllipse(double x, double y, double w, double h),"", "")
	void DrawEllipse(double x, double y, double w, double h);
    
//	void GetTextExtent(const wxString& string,int *x, int *y,int *descent = NULL,int *externalLeading = NULL,wxFont *theFont = NULL);
    
    %DocMeth(DrawText,"DrawText(string,x,y,angle=0)", "(To be called within the Draw method only) Draws text specifying coordinate of lower-left corner (topcontainer's coordinates are used).")
	void DrawText(const wxString& s,double x, double y, double angle=0);
    %DocMeth(DrawTextBase,"DrawTextBase(string,x,y,angle=0)", "(To be called within the Draw method only) Draws text specifying coordinate of base point (topcontainer's coordinates are used).")
	void DrawTextBase(const wxString& s,double x, double y, double angle);
    
    %DocMeth(DrawFormattedText,"DrawFormattedText(string,hMode,x,vMode,y,angle=0)",
    "(To be called within the Draw method only) Draws text specifying justification
        - (horizontal) hMode is one of sp.JUSTIFY_HLEFT, sp.JUSTIFY_HRIGHT, sp.JUSTIFY_HBRIGHT, sp.JUSTIFY_HMIDDLE, sp.JUSTIFY_HBMIDDLE (B stands for BLOCK, i.e. justification of several lines)
        - (vertical) vMode is one of sp.JUSTIFY_HBMIDDLE, sp.JUSTIFY_VUP, sp.JUSTIFY_VDOWN, sp.JUSTIFY_VBASE, sp.JUSTIFY_VMIDDLE, sp.JUSTIFY_VMIDDLE_UP
    relative to the point (x,y) (topcontainer's coordinates are used)")
	void DrawFormattedText(char *str, char hPositionMode, double x, char vPositionMode, double y, double angle=0);
    //	void DrawFormattedText(spFormattedText &ft);

    %DocMeth(DrawPoint,"DrawPoint(x,y)", "(To be called within the Draw method only) Draws a point (topcontainer's coordinates are used).")
	void DrawPoint(double x, double y);
    

    %DocMeth(SetClip,"SetClip(rect)", "(To be called within the Draw method only) Set the clipping rectangle (topcontainer's coordinates are used).")
	void SetClip(spRect &rect);
	
    %DocMeth(AddInteractor,"AddInteractor(interactor)", "Adds the interactor to the object.")
	void AddInteractor(spInteractor *cb);
    %DocMeth(RemoveInteractor,"RemoveInteractor(interactor)", "Removes the interactor to the object.")
	bool RemoveInteractor(spInteractor *cb);


    
    //
    //
    //   ATTRIBUTES
    //
    //
    
    // Attribute : pos
    %extend {spRealPoint _GetPos() {return $self->GetAnchor(true);}}
    %extend {void _SetPos(spRealPoint pt) {return $self->SetAnchor(pt);}}
    %attobj_def(pos,Pos,
        """
        **Attribute** self.pos = a RealPoint(x,y) representing the anchor 
        
        Any object has an anchor point it is attached to. 
        When the anchor is moved, the object is moved
        """);

    // Attribute : pos1
    %extend {spRealPoint _GetPos1() {return $self->GetAnchorOposite();}}
    %extend {void _SetPos1(spRealPoint pt) {return $self->SetAnchorOposite(pt);}}
    %attobj_def(pos1,Pos1,
        """
        **Attribute** self.pos1 = a RealPoint(x,y) representing the oposite point to the anchor
        
        Most objects are defined by a box (i.e., a rectangle) which is defined 
        by the anchor and a margin around the anchor.
        In the case the anchor is one of the corner of this rectangle, pos1, 
        corresponds to the point at the oposite corner.        
        """);

    // Attribute : size
    %extend {spRealPoint _GetSize() {return $self->GetSize();}}
    %extend {void _SetSize(spRealPoint pt) {return $self->SetSize(pt);}}
    %attobj_def(size,Size,
        """
        **Attribute** self.size = a RealPoint(width,height) representing the size of the box
        
        Most objects are defined by a box (i.e., a rectangle) which is defined 
        by the anchor and a margin around the anchor.
        In the case the anchor is one of the corner (or at the center) of this
        rectangle, size, corresponds to the distance of to the anchor.
        """);

    // Attribute : margin
    %extend {
        PyObject *_GetMargin() {
            double margin[4];
            $self->GetMargin(&margin[0],&margin[1],&margin[2],&margin[3]);
            return WrapDoubleArray(margin,4);
        }
        void _SetMargin(double xMargin, double yMargin, double mxMargin, double myMargin) {$self->SetMargin(xMargin,yMargin,mxMargin,myMargin);}
    }
    %attobj_arglist_def(margin,Margin,
        """
        **Attribute** self.margin = (xMargin,yMargin,mxMargin,myMargin)
        
        The margin around the anchor.
            
            xMargin (resp mxMargin) : the right (resp. left) margin
            yMargin (resp myMargin) : the bottom (resp. top) margin
        """);


    // Attribute : penWidth
    void SetPenWidth(int width);
    inline int GetPenWidth();
    %attobj_def(penWidth,PenWidth,
        """
        **Attribute** self.penWidth = (int) width
        """);

    // Attribute : penStyle
    void SetPenStyle(int style = wxSOLID);
    inline int GetPenStyle();
    %attobj_def(penStyle,PenStyle,
        """
        **Attribute** self.penStyle = (int) style
        
            style : one of 
                wx.SOLID, wx.DOT, wx.LONG_DASH, wx.SHORT_DASH, 
                wx.DOT_DASH, wx.TRANSPARENT, wx.BDIAGONAL_HATCH,
                wx.CROSSDIAG_HATCH, wx.FDIAGONAL_HATCH,
                wx.HORIZONTAL_HATCH
        """);

    // Attribute : pen
    void SetPen(wxColour colour, int width = 1, int style = wxSOLID);
    void SetPen(wxPen pen);
    %extend {const char * _GetPen() {return "";}}
    %attobj_arglist_def(pen,Pen,
        """
        **Attribute** self.pen = (*write_only*) colour [,width=1,style=wx.SOLID]
        
        Sets the colour, the width and the style of the pen

            colour : either a string (e.g., 'blue') or a
                    wx.Colour (e.g., wx.BLACK or wx.Colour(r,g,b,alpha)
            width  : an integer
            style : one of 
                wx.SOLID, wx.DOT, wx.LONG_DASH, wx.SHORT_DASH, 
                wx.DOT_DASH, wx.TRANSPARENT, wx.BDIAGONAL_HATCH,
                wx.CROSSDIAG_HATCH, wx.FDIAGONAL_HATCH,
                wx.HORIZONTAL_HATCH
        """);

    //    void SetPen(wxBitmap stipple, int width = 1);
//    %extend {wxPen GetPen1() {return $self->GetPen();}}

    // Attribute : penColour
    void SetPenColour(wxColour colour);
    wxColour GetPenColour();
    %attobj_def(penColour,PenColour,
        """
        **Attribute** self.penColour = wx.Colour
        
        The colour of the pen, either a string (e.g., 'blue') 
        or a wx.Colour (e.g., wx.BLACK or wx.Colour(r,g,b,alpha)
        """);

    // Attribute : colour
    %extend {
    void _SetColour(wxColour colour) {$self->SetPenColour(colour); $self->SetBrushColour(colour);}
    wxColour _GetColour() {return $self->GetPenColour();}
    }
    %attobj_def(colour,Colour,
        """
        **Attribute** self.colour = wx.Colour
        
        Allows to set at the same time the colour of the pen and of the brush.
        The colour can be either a string (e.g., 'blue')
        or a wx.Colour (e.g., wx.BLACK or wx.Colour(r,g,b,alpha)
        """);

    // Attribute : brushStyle
    void SetWhiteBrushPrinting(bool flag);
    bool GetWhiteBrushPrinting();
    %attobj_def(whiteBrushPrinting,WhiteBrushPrinting,
    """
    **Attribute** self.whiteBrushPrinting = (bool) flag
    
        If True then, when printing, the brush is forced to white
    """);
                
    // Attribute : brushStyle
    void SetBrushStyle(int style = wxSOLID);
    int GetBrushStyle();
    %attobj_def(brushStyle,BrushStyle,
        """
        **Attribute** self.brushStyle = (int) style
        
            style : one of 
                wx.SOLID, wx.DOT, wx.LONG_DASH, wx.SHORT_DASH, 
                wx.DOT_DASH, wx.TRANSPARENT, wx.BDIAGONAL_HATCH,
                wx.CROSSDIAG_HATCH, wx.FDIAGONAL_HATCH,
                wx.HORIZONTAL_HATCH
        """);


    // Attribute : brush
    void SetBrush(wxColour colour, int style = wxSOLID);
    void SetBrush(wxBrush brush);
    %extend {const char *_GetBrush() {return "";}}
    %attobj_arglist_def(brush,Brush,
        """
        **Attribute** self.brush = (*write_only*) colour [,style=wx.SOLID]
        
        Sets the colour, and the style of the pen

            colour : either a string (e.g., 'blue') or a
                    wx.Colour (e.g., wx.BLACK or wx.Colour(r,g,b,alpha)
            style : one of
                wx.SOLID, wx.DOT, wx.LONG_DASH, wx.SHORT_DASH, 
                wx.DOT_DASH, wx.TRANSPARENT, wx.BDIAGONAL_HATCH,
                wx.CROSSDIAG_HATCH, wx.FDIAGONAL_HATCH,
                wx.HORIZONTAL_HATCH
        """);

    // Attribute : brushColour
    void SetBrushColour(wxColour colour);
    wxColour GetBrushColour();
    %attobj_def(brushColour,BrushColour,
        """
        **Attribute** self.brushColour = wx.Colour
        
        The colour of the brush, either a string (e.g., 'blue') 
        or a wx.Colour (e.g., wx.BLACK or wx.Colour(r,g,b,alpha)
        """);

    
    //    const wxBitmap *GetBrushStipple();
    //    void SetBrush(wxBitmap& stipple);
    //    wxBrush GetBrush();


    // Attribute : fontColour
    %extend {void _SetFontColour(wxColour colour) {$self->SetTextForeground(colour);}}
    %extend {wxColour _GetFontColour() {return $self->GetTextForeground();}}
    %attobj_def(fontColour,FontColour,
        """
        **Attribute** self.fontColour = wx.Colour
        
        The colour of the font, either a string (e.g., 'blue')
        or a wx.Colour (e.g., wx.BLACK or wx.Colour(r,g,b,alpha)
        """);

    // Attribute : font
    void SetFont(int pointSize, wxFontFamily family=wxFONTFAMILY_DEFAULT, int style=wxFONTSTYLE_NORMAL, wxFontWeight weight=wxFONTWEIGHT_NORMAL, const bool underline = false, wxString faceName = "", wxFontEncoding encoding = wxFONTENCODING_DEFAULT);
    void SetFont(const wxFont &font1);
    %extend {const char *_GetFont() {return "";}}
    %attobj_arglist_def(font,Font,
        """
        **Attribute** self.font = (*write only*) size, [family=wx.DEFAULT, style=wx.FONTSTYLE_NORMAL,
                                weight=wx.FONTWEIGHT_NORMAL, underline=False,
                                faceName="", encoding = wx.FONTENCODING_DEFAULT]
        
            size : an integer representing the size of the font
            family : one of wx.DEFAULT, wx.DECORATIVE, wx.ROMAN,
                            wx.SCRIPT, wx.MODERN, wx.TELETYPE
            style : one of wx.FONTSTYLE_NORMAL, wx.FONTSTYLE_ITALIC,
                            wx.FONTSTYLE_SLANT, wx.FONTSTYLE_MAX
            weight : one of wx.FONTWEIGHT_NORMAL, wx.FONTWEIGHT_BOLD,
                            wx.FONTWEIGHT_LIGHT, wx.FONTWEIGHT_MAX
            underline : False or True
            faceName  : a string (e.g., 'Lucida Grande')
            encoding : many possible values (all of the form wx.FONTENCODING....)                            
        """);
    
    // Attribute : fontSize
    void SetFontSize(int size);
    int GetFontSize();
    %attobj_def(fontSize,FontSize,
        """
        **Attribute** self.fontSize = size

            size : an integer representing the size of the font
        """);

    // Attribute : fontStyle
    void SetFontStyle(int style = wxFONTSTYLE_NORMAL);
    int GetFontStyle();
    %attobj_def(fontStyle,FontStyle,
        """
        **Attribute** self.fontStyle = style
        
            style : one of wx.FONTSTYLE_NORMAL, wx.FONTSTYLE_ITALIC,
                            wx.FONTSTYLE_SLANT, wx.FONTSTYLE_MAX
        """);
    
    // Attribute : fontWeight
    void SetFontWeight(int font);
    int GetFontWeight() {return font.GetWeight();}
    %attobj_def(fontWeight,FontWeight,
        """
        **Attribute** self.fontWeight = weight
        
            weight : one of wx.FONTWEIGHT_NORMAL, wx.FONTWEIGHT_BOLD,
                            wx.FONTWEIGHT_LIGHT, wx.FONTWEIGHT_MAX
        """);

    // Attribute : fontFaceName
    void SetFontFaceName(char *faceName);
    char *GetFontFaceName();
    %attobj_def(fontFaceName,FontFaceName,
        """
        **Attribute** self.fontFaceName = faceName

            faceName  : a string (e.g., 'Lucida Grande')
        """);

    // Attribute : fontFamily
    void SetFontFamily(int family);
    int GetFontFamily() {return font.GetFamily();}
    %attobj_def(fontFamily,FontFamily,
        """
        **Attribute** self.font = family

            family : one of wx.DEFAULT, wx.DECORATIVE, wx.ROMAN,
                            wx.SCRIPT, wx.MODERN, wx.TELETYPE
        """);
    

    //    wxFont &GetFont() {return font;};

};


//
// nrefs field (read only)
//
%extend spObject {
%immutable;
    int nrefs;
%mutable;
}
%{
    int spObject_nrefs_get(spObject *s) {return s->nRefs();}
%}


//
// name field (read/write)
//
%extend spObject {
  char *name;
}
%{
  const char * spObject_name_get(spObject *s) {return s->GetName();}
  void spObject_name_set(spObject *s,char *str) {s->SetName(str);}
%}

//
// Get/Set object container
//
%extend spObject {
    %DocMeth(container_,"FAKE METHOD just for indicating doc of self.container","Use field container for setting/getting the container of the object.")
    void container_() {};
}
%extend spObject {
    PyObject *container;
}
%{
    PyObject *spObject_container_get(spObject *object) {return WrapObject(object->container);}
    void spObject_container_set(spObject *object,PyObject *container) {
        if (container == Py_None) {
            SetContainer(object,NULL);
            return;
        }

        spContainer *ct;
        void *ptr = (void *) ct;
        int succeed = SWIG_ConvertPtr(container, (void **) &ct,SWIGTYPE_p_spContainer, 0 |  0 );
        if (!SWIG_IsOK(succeed)) {
            PyErr_SetString(PyExc_RuntimeError, "The container field should be of class sp.Container");
        }
        else {
            SetContainer(object,ct);
        }
    }
%}

%extend spObject {
    %DocMeth(container_non_group_,"FAKE METHOD just for indicating doc of self.container_non_group","Use field container_non_group for getting the first container in the object's hierarchy which is not a group.")
    void container_non_group_() {};
}
%extend spObject {
    %immutable;
    PyObject *container_non_group;
    %mutable;
}
%{
    PyObject *spObject_container_non_group_get(spObject *object) {return WrapObject(object->GetNonGroupContainer());}
%}
                
//
// interactors Read-Only field
//
%extend spObject {
    %DocMeth(interactors_,"FAKE METHOD just for indicating doc of self.interactors","Use field interactors for getting the list of interactors attached to the object.")
    void interactors_() {};
}
%extend spObject {
    %immutable;
    PyObject *interactors;
%mutable;
}
%{
PyObject *spObject_interactors_get(spObject *obj) {
    PyObject* list = PyList_New(obj->interactorList.Size());
    for (int i = 0; i < obj->interactorList.Size(); i++) {
//        printf("%s\n",obj->interactorList[i]->GetClassName());
        PyObject *inter = WrapObject(obj->interactorList[i]);
        PyList_SET_ITEM(list, i, inter);
    }
    return list;
}
%}

//
// The ninteractors Read-only field
//
%extend spObject {
    %DocMeth(ninteractors_,"FAKE METHOD just for indicating doc of self.ninteractors","Use field ninteractors for getting the number of interactors attached to the object.")
    void ninteractors_() {};
}
%extend spObject {
    %immutable;
    int ninteractors;
    %mutable;
}
%{
    int spObject_ninteractors_get(spObject *obj) {return obj->interactorList.Size();}
%}

                


%rename(CursorCrossHair) spCursorCrossHair;

%feature("director") spCursorCrossHair;

%DocClass(spCursorCrossHair,"An object that draws a cross cursor")
class spCursorCrossHair : public spObject
{
	public : 
	
    ObjRepr;

	spCursorCrossHair();
//	virtual void Draw(spRect &rect);
};








