/*
 *  interactor_msge.cpp
 *  LastPlot
 *
 *  Created by bacry on 04/10/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */




#include "sp.h"



//
// An interactor that sends a particular message to a receiver
//

static void CBMsge(spInteractor *cb,spEvent &event) 
{
	spInteractorSendMsge *i = (spInteractorSendMsge *) cb;
	
	if (!i->receiver) return;
    event.object->SendMsge(i->receiver,i->msge);
}

spInteractorSendMsge::spInteractorSendMsge(spObject *receiver1, spMsge msge1, spEventSimpleType e, int m,const char *name) : spInteractor1(CBMsge,NULL,e,m,false,name)
{
	receiver = receiver1;
    msge = msge1;
}
