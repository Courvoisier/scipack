
%rename(Text) spText;

%rename(_SetText) SetText;
%rename(_GetText) GetText;
%rename(_SetFrameMargin) SetFrameMargin;
%rename(_GetFrameMargin) GetFrameMargin;
%rename(_SetHMode) SetHMode;
%rename(_SetVMode) SetVMode;
%rename(_GetHMode) GetHMode;
%rename(_GetVMode) GetVMode;
%rename(_SetAngle) SetAngle;
%rename(_GetAngle) GetAngle;

%feature("director") spText;

// We set the kwargs for some methods
%feature("kwargs") spText::spText;

%DocClass(spText,"Object that displays a text which is justified relative to the anchor")
class spText : public spObject
{
	public : 
	
    // The string representation in python
    ObjRepr;

	virtual void Draw(spRect &rect);

	spText(spContainer *container=NULL,const spAnchor &anchor=spAnchor(), int hmode=spJustifyHLeft, int vmode=spJustifyVBase, unsigned long flags=0,const char *name=NULL);

    //
    //
    //   ATTRIBUTES
    //
    //

    %attobj_remove(pos1)
    %attobj_remove(margin)
    %attobj_remove(size)

    // Attribute : frameMargin
	void SetFrameMargin(spRealPoint margin=spRealPoint(5,2));
	spRealPoint GetFrameMargin();
    %attobj_def(frameMargin,FrameMargin,
        """
        **Attribute** self.frameMargin = margin
        
            margin : if -1 then no frame is drawn around the text
                     if > 0 then the frame is drawn at a distance of
                     'margin' pixels from the text
        """);


    // Attribute : hMode
	void SetHMode(int hmode);
	int GetHMode();
    %attobj_def(hMode,HMode,
        """
        **Attribute** self.hMode = hMode
        
        An integer that indicates how the text is justify horizontally.
        One of JUSTIFY_HLEFT, JUSTIFY_HRIGHT, JUSTIFY_HBRIGHT, 
        JUSTIFY_HMIDDLE, JUSTIFY_HBMIDDLE (the letter B stands for BLOCK justification, i.e., multilines)
        """);

    // Attribute : vMode
	void SetVMode(int hmode);
	int GetVMode();
    %attobj_def(vMode,VMode,
        """
        **Attribute** self.vMode = vMode
        
        An integer that indicates how the text is justify vertically.
        One of JUSTIFY_VUP, JUSTIFY_VDOWN, JUSTIFY_VBASE, 
        JUSTIFY_VMIDDLE, JUSTIFY_VMIDDLE_UP (the letter B stands for BLOCK justification, i.e., multilines)
        """);
	
    // Attribute : text
	virtual void SetText(const char *str);
	const char *GetText();
    %attobj_def(text,Text,
        """
        **Attribute** self.text = string
        
        The string to be displayed
        """);
    
    // Attribute : angle
	void SetAngle(double angle);
	double GetAngle();
    %attobj_def(angle,Angle,
        """
        **Attribute** self.angle = angle
        
        The angle at which the text is disaplyed (in degrees)
        """);    
};
