
#include "sp.h"



class spInteractorObjectSet : public spInteractor1
{
	public :
	
	spObjectSet *objectSet;
	
	spInteractorObjectSet(spObjectSet *set);
};

static void CBObjectSet(spInteractor *i,spEvent &e)
{
	spInteractorObjectSet *inter = (spInteractorObjectSet *) i;
	
	inter->objectSet->RemoveObject(e.object);
}

spInteractorObjectSet::spInteractorObjectSet(spObjectSet *set) : spInteractor1(CBObjectSet,spOnDelete)
{
	objectSet=set;
}


spObjectSet::~spObjectSet()
{
	for (int i = 0;i<objectList.Size();i++) {
		objectList[i]->RemoveInteractor(inter);
	}
	inter->Unref();
}

spObjectSet::spObjectSet()
{
	inter = new spInteractorObjectSet(this);
	inter->Ref();
}

void spObjectSet::AddObject(spObject *receiver)
{
  objectList.Add(receiver);
	receiver->AddInteractor(inter);
}


void spObjectSet::RemoveObject(spObject *receiver)
{
  if (objectList.Remove(receiver)) receiver->RemoveInteractor(inter);
}



