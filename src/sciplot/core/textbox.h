//
//  textbox.h
//  sp
//
//  Created by bacry on 15/01/13.
//
//

#ifndef __sp__textbox__
#define __sp__textbox__

class spTextBox : public spShape
{
	bool flagFrame;
	spFormattedText ft;
	int hMode;

	public :
	
	spTextBox(spContainer *container=NULL,const spAnchor &anchor=spAnchor(),const spMargin &margin=spMargin(), int hmode1=spJustifyHLeft, unsigned long flags=0,const char *name=NULL);
	    
	virtual void Draw(spRect &rect);
    
    virtual void SetText(const char * str1);
	virtual const char *GetText();
	virtual inline void SetFrame(bool flag) {flagFrame = flag;}
	virtual inline bool GetFrame() {return flagFrame;}
    virtual void SetHMode(int hMode);
    virtual int GetHMode() {return ft.hMode;}

	virtual void UpdateGeometry();
    
    virtual const char *GetClassName() {
		return _CCHAR("spTextBox");
	}
};




#endif /* defined(__sp__textbox__) */
