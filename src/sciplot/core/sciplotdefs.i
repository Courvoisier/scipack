
// Not a module



////////////////////////////////////////////
//
// when events are executed we should get the GIL
//
////////////////////////////////////////////

%{
	PyGILState_STATE _gstate;
	
	void SCRIPT_StartEvent() 
	{
		_gstate = PyGILState_Ensure();
	}
	void SCRIPT_EndEvent() 
	{
		PyGILState_Release(_gstate);
	}
%}



%{
	void *SCRIPT_GET_DIRECTOR(spRef * ref)
	{
        Swig::Director *director = SWIG_DIRECTOR_CAST(ref);
        if (director) return (void *) director->swig_get_self();
        else return NULL;
	}
%}




%{
    extern PyObject *WrapObject(spRef *object);
    extern 	void PrintPyError();
	void SCRIPT_Config(spRef *ref)
	{
        PyObject *obj = WrapObject(ref);
        PyObject *result = PyObject_CallMethod(obj,_CCHAR("config"),NULL);
        Py_XDECREF(obj);
        if (result == NULL) {
			PrintPyError();
			return;
		}
        Py_XDECREF(result);
	}
%}


///////////////////////////////////////////////
//
// Wrapper to get an object
// $descriptor(std::vector<int> *)
///////////////////////////////////////////////

%{
	PyObject *WrapObject(spRef *object)
	{
        if (object == NULL) {
            Py_INCREF(Py_None);
            return Py_None;
        }
        PyObject * result;
        Swig::Director *director = SWIG_DIRECTOR_CAST(object);
        if (director) {
            result = director->swig_get_self();
            Py_INCREF(result);
        } else {
            static char className[1000];
            strcpy(className,"_p_");
            strcat(className,object->GetClassName());
            result = SWIG_NewPointerObj(SWIG_as_voidptr(object),SWIG_TypeQuery(className),  SWIG_POINTER_OWN |  0 );
            // The SWIG_NewPointerObj function does not call Ref, so we have to do it
            object->Ref(true);
        }
        if (result == NULL) {
            Py_INCREF(Py_None);
            return Py_None;
        }
        return result;
    }    
%}




//
// attribute for spObjects (read/write)
//



%define %attobj_remove(attName)
%extend {
%pythoncode %{
    try:
        _attobj_remove_list
    except NameError:
        _attobj_remove_list = []
    _attobj_remove_list += ['attName']
%}
}
%enddef


%define %attobj_def(attname,attName,doc)
%extend {
%pythoncode %{
    @property
    def  attname (self):
        doc
        return self._Get ## attName ()
    @ attname ## .setter
    def attname (self,val):
        doc
        self._Set ##  attName (val)
%}
}
%enddef

%define %attobj_arglist_def(attname,attName,doc)
%extend {
    %pythoncode %{
        @property
        def  attname (self):
            doc
            return self._Get ## attName ()
        @ attname ## .setter
        def attname (self,val):
            doc
            if (not isinstance(val, (list, tuple))): self._Set ##  attName (val)
            else: self._Set ##  attName (*val)
    %}
}
%enddef


//
// attribute for spInteractors (read/write)
//

%define %attint_def(attname,attName,doc)
%extend {
    %pythoncode %{
        @property
        def  attname (self):
            doc
            return self._Get ## attName ()
        @ attname ## .setter
        def attname (self,val):
            doc
            self._Set ##  attName (val)
        %}
}
%enddef

%define %attint_arglist_def(attname,attName,doc)
%extend {
    %pythoncode %{
        @property
        def  attname (self):
            doc
            return self._Get ## attName ()
        @ attname ## .setter
        def attname (self,val):
            doc
            if (not isinstance(val, (list, tuple))): self._Set ##  attName (val)
            else: self._Set ##  attName (*val)
    %}
}
%enddef
