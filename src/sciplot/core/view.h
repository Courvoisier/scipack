
#ifndef _SP_View_H_
#define _SP_View_H_


class spView : public spContainer
{
	public :
	
	//
  // Simple class tests	
	//
	virtual bool IsView() {return true;};
	

	
	//
  // Destructor/Constructor and initialization
	//
	void InitView();
	spView(spContainer *ctn=NULL,const spAnchor &anchor=spAnchor(),const spMargin &margin=spMargin(),unsigned long flags=0,const char *name=NULL);
  virtual ~spView();	

	//
	// Dealing with maps
	//
	// l2g : Conversion from local coordinates inside the view to coordinates (point 0,0 is the corner point)
	// l2top : Conversion from local to canvas coordinate (the ones that are used to draw on the window)
	//
	void Setl2g(spMap2d l2g1);
	void Setl2gX(spMap1d l2gx);
	void Setl2gY(spMap1d l2gy);
	void SendChangeBoundEvent();
    void Recomputel2g();
	
	//
	// Dealing with (xMin,xMax,yMin,yMax) xyBound
	//

	spRealRect xyBound;  // The current xyBound
	
	// Different ways to get the xyBound
	void RecomputeXYBound();
	inline spRealRect _GetXYBound() {
		return xyBound;
	};                                 // Get the current xyBound as is (xMin>xMax if the x axis is reversed)
	spRealRect GetXYBound();                                  // Same as above but xMin<xMax and yMin<yMax
	spRealRect ComputeMinXYBound();                           // Computes the smallest xyBound that contains all the objects and returns it
	spRealRect ComputeMinXYBound(double xMin, double xMax);   // Same as above but force xMin and xMax
	
	// Different ways to set xyBound
	void SetXYBound(spRealRect r);                                          // Set xyBound using a rect
	void SetXYBound(double xmin,double xmax, double ymin, double ymax);   // Set xyBound using xMin,xMax,yMin,yMax with xMin<xMax and yMin<yMax
	void _SetXYBound(double xmin,double xmax, double ymin, double ymax);    // Set xyBound using xMin,xMax,yMin,yMax with xMin>xMax if x axis is reversed and yMin>yMax if y axis is reversed

	
	//
	// redefinition of the UpdateGeometryMain method (it calls the same methods for all the objects inside the view)
	//	
    virtual void UpdateGeometry();
	
	//
	// Scrolling methods
	//
	virtual void ScrollX(int change,bool flagUpdateYScale=false);
	virtual void ScrollY(int change);
	
	//
	// Zoom methods
	//
	virtual void Zoom(spRealPoint pt, int change);
	virtual void ZoomX(spRealPoint pt, int change);
	virtual void ZoomY(spRealPoint pt, int change);

	void SetLogScaleY(bool flag = true);
	void SetLogScaleX(bool flag = true);
	bool GetLogScaleY();
	bool GetLogScaleX();

  virtual const char *GetClassName() {
		return _CCHAR("spView");
	}
};



#endif
