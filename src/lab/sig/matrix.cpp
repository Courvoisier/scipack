//
//  matrix.cpp
//  sp
//
//  Created by bacry on 04/03/13.
//
//

#include "sig.h"
#include "matrix.h"

double MATRIX::_temp_double = 0;


//
// Dealing with numerical recipees matrices and vectors
//


FMATRIX Matrix::FMatrix() DECLARETHROW
{
    if (nCols != nRows) THROW("Matrix expected to be squared");
    if (nCols == 0) THROW("Matrix expected to be non empty");
    
    FMATRIX cm = NewFMatrix(nCols);
    
    for (unsigned long k=0,i=1;i<=nCols;i++) {
        memcpy(cm->data[i]+1,data+k,nCols*sizeof(double));
        k+=nCols;
    }
    
    return(cm);
}

void Matrix::SetFMatrix(FMATRIX m)
{
    SetSize(m->size,m->size);
    unsigned long k,i;
    for (k=0,i=1;i<=nCols;i++) {
        memcpy(data+k,m->data[i]+1,nCols*sizeof(double));
        k+=nCols;
    }
}


Matrix::Matrix(unsigned long nRows, unsigned long nCols) : nRows(nRows), nCols(nCols)
{
    count = 0;
    if (nRows*nCols == 0) data = NULL;
    else data = new double[nRows*nCols];
}


double & Matrix::operator()(unsigned long i, unsigned long j)
{
    return data[i * nCols + j];
}

double Matrix::operator()(unsigned long i, unsigned long j) const
{
    return data[i * nCols + j];
}

double & MATRIX::operator()(unsigned long i, unsigned long j)
{
    if (matrix == NULL) return _temp_double;
    return (*matrix)(i,j);
}

double MATRIX::operator()(unsigned long i, unsigned long j) const
{
    if (matrix == NULL) return 0;
    return (*matrix)(i,j);
}


void Matrix::SetSize(unsigned long nRows1, unsigned long nCols1)
{
    if (nRows*nCols <nRows1*nCols1) {
        delete [] data;
        if (nRows1*nCols1 == 0) data = NULL;
        else data = new double[nRows1*nCols1];
    }
    nRows = nRows1;
    nCols = nCols1;
}

MATRIX Matrix::Copy()
{
    Matrix *res = new Matrix(nRows,nCols);
    memcpy(res->GetData(),data,sizeof(double)*nRows*nCols);
    return res;
}


MATRIX Matrix::Inverse(Matrix *out) DECLARETHROW
{
    if (nCols != nRows) THROW("Matrix should be squared to compute inverse");
    if (nCols == 0) THROW("Matrix should not be of size 0 to compute inverse");

    if (out == NULL) out = this;
    
    FMATRIX m = FMatrix();
    
    try {InverseFMatrix(m);}
    catch (const char *str) {
        DeleteFMatrix(m);
        THROW(str);
    }
    out->SetFMatrix(m);
    DeleteFMatrix(m);

	return out;
}


SIGNAL Matrix::SymDiagonalize(Matrix *eigenVectors) DECLARETHROW
{
    if (nCols != nRows) THROW("Matrix should be squared and symetric to diagonolize");
    if (nCols == 0) THROW("Matrix should not be of size 0 to diagonalize");

    FMATRIX a = FMatrix();
    FVECTOR eigenValuesFVector = SymDiagonalizeFMatrix(a);
    
    if (eigenVectors) eigenVectors->SetFMatrix(a);

    SIGNAL eigenValues = new Signal();
    eigenValues->SetFVector(eigenValuesFVector);
    
    DeleteFMatrix(a);
    DeleteFVector(eigenValuesFVector);
    
    return eigenValues;
}



