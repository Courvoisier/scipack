

//
// This is the file to include when you want to use the sig module in another module.
//
// You should add in your .i module definition file
//
// %include "sigdefs.i"
//

// Here are some specific C++ functions that are needed for the signal typemaps
%{
extern  PyObject *_Signal1DArray2PyObject(SIGNAL_1D_ARRAY &array);
extern  PyObject *_Signal2DArray2PyObject(SIGNAL_2D_ARRAY &array);
extern 	Signal *PySignal_Check(PyObject *obj);
extern Signal *_PyObject2Signal(PyObject *obj);
extern bool myPyArray_Check(PyObject *obj);
extern double *_NumpyDoubleArray2CDoubleArray(PyObject *obj, unsigned long &size);

%}
