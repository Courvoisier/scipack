//
//  nr_linear_algebra.h
//  sp
//
//  Created by bacry on 04/03/13.
//
//

#ifndef __sp__nr_linear_algebra__
#define __sp__nr_linear_algebra__

// Square matrix numerical recipee
typedef struct fmatrix {
    double **data;
    unsigned long size;
} *FMATRIX;

// Integer Vector structure
typedef struct ivector {
    int *data;
    unsigned long size;
} *IVECTOR;

/* Floating Vector structure */
typedef struct fvector {
    double *data;
    unsigned long   size;
} *FVECTOR;



extern IVECTOR NewIVector(unsigned long size);

extern void DeleteIVector(IVECTOR ov);

extern FVECTOR NewFVector(unsigned long size);

extern void DeleteFVector(FVECTOR ov);

extern void DeleteFMatrix(FMATRIX m);

extern FMATRIX NewFMatrix(unsigned long size);

extern void InverseFMatrix(FMATRIX m) DECLARETHROW;

extern FVECTOR SymDiagonalizeFMatrix(FMATRIX a) DECLARETHROW;


#endif /* defined(__sp__nr_linear_algebra__) */
