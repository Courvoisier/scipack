//
//  sp_fredholm.cpp
//  SciPack
//
//  Created by bacry on 08/04/13.
//  Copyright (c) 2013 bacry. All rights reserved.
//



extern "C" double system_fredin(double x, int k, int N, int n, double a, double b, double t[], double f[],double w[], double (*g)(int,double), double (*ak)(int,int,double, double));
extern "C" void system_fred2(int N, int n, double a, double b, double t[], double gg[], double w[],double (*g)(int,double), double (*ak)(int,int,double, double));

#include "../sig.h"




//
// Scipack interface to solver of multi-dimensionnal Fredholm equation
//
// The 1d basic algo is taken from numerical recipee
//
//
//
// The system is
//
//              f_k(t) = \int_a^b K_kl(t,s)f_l(s)ds + g_k(t)
//
// N        : dimension
// f_k      : unknwown (1<=k<=N)
// K_kl     : kernel function (1<=k<=N and (1<=l<=N)
// g_k      : given functions
//
//
// Quadrature is used for computing the integral.
//


//
// FredholmSolver(...) : Solver of the above system
//
//
// Paramaters to the method below are :
//
// N            : dimension of the equation
// n            : number of quadrature points
// a,b          : bounds of the integral
// x,weights    : abscissa/weights quadrature points (there are n of them)
// g            : the g functions
// kernel       : the kernel functions
//
// quad_x       : the quadrature points
// quad_w       : the quadrature weights
//
// Returns :
//
//      an array of double where all the solutions f_k have been concatenated (n points each)
//


SPEXPORT double *FredholmSolver(int N, int n, double a, double b, double (*g)(int,double), double (*kernel)(int,int,double, double),SIGNAL quad_x, SIGNAL quad_w)
{
    // Allocation of the solution
    double *solution = new double[n*N];

    // Allocation of output signals
    quad_x->SetSize(n);
    quad_w->SetSize(n);
	
    // Solve the system
	system_fred2(N,n,a,b,quad_x->_Yarray()-1,solution-1,quad_w->_Yarray()-1,g,kernel);
	
    
    // Check that reconstruction works for the first K quadrature points of f_1
    int K = 10;
    for (int kk=0;kk<K;kk++) {
        double x = quad_x->Y(kk);
        double s = solution[kk];
        double s1 = system_fredin(x,1,N,n,a,b,quad_x->_Yarray()-1,solution-1,quad_w->_Yarray()-1,g,kernel);
        printf("test %g %g\n",s,s1);
    }
    
	return solution;
}



//
// FredholmInterpolation(...) : Interpolation of the solution given the result of the solver
//
//
// Paramaters to the method below are :
//
// N            : dimension of the equation
// n            : number of quadrature points
// a,b          : bounds of the integral
// x,weights    : abscissa/weights quadrature points (there are n of them)
// g            : the g functions
// kernel       : the kernel functions
//
// quad_x       : the quadrature points
// quad_w       : the quadrature weights
//
// Returns :
//
//      an array of double where all the solutions f_k have been concatenated (n points each)
//

SPEXPORT double *FredholmInterpolation(double dx,SIGNAL x, int N, double a, double b, double (*g)(int,double), double (*kernel)(int,int,double, double), SIGNAL quad_x, SIGNAL quad_w,double *solution)
{
	if (dx > 0) {
		int nSamples = (int) (quad_x->Y(quad_x->Size()-1)/dx);
        x->SetSize(nSamples);
        for (int k=0;k<nSamples;k++) x->Y(k) = k*dx;
    }

    double *solution1 = new double[x->Size()*N];

    for (int k=0;k<N;k++) {
        for (int n=0;n<x->Size();n++) {
            solution1[n+k*x->Size()] = system_fredin(x->Y(n),k+1,N,quad_x->Size(),a,b,quad_x->_Yarray()-1,solution-1,quad_w->_Yarray()-1,g,kernel);
        }
    }
	
	return solution1;
}

extern "C" void gauleg(double x1,double x2,double x[],double w[], int n);


SIGNAL_1D_ARRAY GQuadrature(double a, double b, int n)
{
    SIGNAL points = new Signal(n);
    SIGNAL weights = new Signal(n);
    double *p = points->_Yarray();
    double *w = weights->_Yarray();
    gauleg(a,b,p-1,w-1,n);
    
    SIGNAL_1D_ARRAY res;
    
    res.resize(2);
    res[0] = points;
    res[1] = weights;
    
    return res;
}

