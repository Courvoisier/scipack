//
//  rand.h
//  sp
//
//  Created by bacry on 01/03/13.
//
//

#ifndef __RAND__
#define __RAND__


class SPEXPORT Rand {
    
    static int iff;
    static long int idum;

    public :
    
    static int UniformInt(int a, int b);
    static void Init(long int idum1);
    static double Uniform();
    static double Gaussian(double sigma = 1.0);
    static double Exponential(double intensity);
    static double Stable(double alpha, double sigma,double beta, double mu);
    static double Poisson(double lambda);
};


// Used for thread safe generation
class TSRand
{
private :  // The seed
    int iff;
    long int idum;
    static long nTSRand;
    
public : // Basic constructor and Init
    
    TSRand(long int idum=-1);
    void Init(long int idum);
    
private : // Uniform
    int inext,inextp;
    long ma[56];
public :
    double Uniform();
    
private: // gaussian
    int iset;
    double gset;
public:
    double Gaussian(double sigma);

private : // Poisson
    double sq,alxm,g,oldm;
public:
    double Poisson(double xm);

public: // Stable
    double Stable(double alpha, double sigma,double beta, double mu);
    
public: // Exponential
    double Exponential(double intensity);

public: // UniformInt
    int UniformInt(int a, int b);
};



#endif
