//  SciPack
//
//  Created by bacry on 24/04/13.
//  Copyright (c) 2013 bacry. All rights reserved.
//


#ifndef _SIG_H_
#define _SIG_H_

#define _USE_MATH_DEFINES
#include <cmath>
#include <stdio.h>

#include <complex>

#include <string.h>

#include "scipackdefs.h"

#include "error.h"

#ifdef WIN32
template<typename T> bool isfinite(T arg)
{
    return arg == arg &&
    arg != std::numeric_limits<T>::infinity() &&
    arg != -std::numeric_limits<T>::infinity();
}
#else
using std::isfinite;
using std::isinf;
using std::isnan;
#endif

typedef std::complex<double> Complex;


#define ROUND(x) ((int) (x >= 0 ? floor(x+0.5) : ceil(x-0.5)))
#define MAX(x,y)  ((x) > (y) ? (x) : (y))
#define MIN(x,y)  ((x) < (y) ? (x) : (y))


// The different types for the signal's values
typedef enum {
    sigTypeUnknown = 0,
    sigTypeReal,
    sigTypeComplex
} sigTypeValue;

// Coding whether X is an array or not
typedef enum {
    sigXYTypeUnknown = 0,
    sigYType,
    sigXYType
} sigXY_Type;

// The different interpolation mode
typedef enum {
    sigInterNone = 0,
    sigInterDirac,
    sigInterConstRight,
    sigInterConstLeft,
    sigInterLinear
} sigInterMode;

/* The different border effects */
typedef enum {
    sigBorderNone = 0,
    sigBorderPer,
    sigBorderMir,
    sigBorderMir1,
    sigBorderCon,
    sigBorder0
} BorderType;

#define SIG_Factor_IncrAlloc 1.5

/*
 * The class definition
 */

class SPEXPORT Signal
{
	private :
	
    static double _tempDouble;
    static Complex _tempComplex;
    
	static double FactorIncrAlloc;
	
    long count;

	// The size of the signal
	unsigned long size;
    
	// The Y-array
	bool flagComplex;
	void *Yarray;
	unsigned long sizeMallocY; 		// The total allocation of the array (always greater than the size)
#ifdef SP_SCRIPT
	void *Yowner;
	public :
	void SetYowner(void *owner,void *ptr, unsigned long size);
	void GiveYownership(void *array);
	void *GetYowner(){return Yowner;}
#endif
    
	// The X array
	double *Xarray; 		          // The X-array itself
	unsigned long sizeMallocX; 		// The total allocation of the array (always greater than the size)
	double dx,x0;
#ifdef SP_SCRIPT
	void *Xowner;
	public :
	void GiveXownership(void *array);
	void *GetXowner(){return Xowner;}
#endif
    
	// Clear the arrays
	void ClearX();
	void ClearY();
	
	/* Forbid copy constructor */
	Signal(Signal & v) {};
	
	/* Forbid allocation method */
	void operator = (Signal v) {};
	
	public :
    
	// Reference count
	inline long ref() {return count++;};
	inline long getcount() {return count;};
	inline long unref() {
		if (count == 0 || --count == 0 ) {
			delete this;
			return 0;
		}
		return count;
	}
    
    std::string name;
    
    sigInterMode interMode;
    
    
    void Clear() {ClearX();ClearY();};
    
	inline sigXY_Type GetXYType() {return (IsXY() ? sigXYType : sigYType);};
	inline bool IsXY() {return Xarray != NULL;};
	inline sigTypeValue GetValueType() {return (flagComplex ? sigTypeComplex : sigTypeReal);};
	inline bool IsComplex() {return flagComplex;};
	inline unsigned long Size() {return size;};
	void SetSize(unsigned long size, bool flagRemember = false, sigXY_Type XYType = sigXYTypeUnknown, sigTypeValue type = sigTypeUnknown);
	void CastComplex() {if (!IsComplex()) SetSize(size,true,sigXYTypeUnknown,sigTypeComplex);}
	
	inline double X0() {return x0;};
	inline void SetX0(double x0_) {x0 = x0_;ClearX();};
	inline double Dx() {return dx;};
	inline void SetDx(double dx_) {dx = fabs(dx_); ClearX();};
	inline unsigned long SizeMallocY() {return sizeMallocY;};
	inline unsigned long SizeMallocX() {return sizeMallocX;};
    
	inline sigTypeValue NewValueType(Signal *s) {return (s->IsComplex() || IsComplex() ? sigTypeComplex : sigTypeReal);}
	inline sigXY_Type NewXYType(Signal *s) {return (s->IsXY() || IsXY() ? sigXYType : sigYType);}
    
	inline double *_Xarray() {return Xarray;};
	inline double *_Yarray() {return (double *) Yarray;};
	inline Complex *_Ycarray() {return (Complex *) Yarray;};

	inline double &Y(unsigned long i) {return (((double *) Yarray)[i]);}
	inline Complex Yc(unsigned long i) {return (flagComplex ? ((Complex *) Yarray)[i] : ((double *) Yarray)[i]);};

	inline void SetY(unsigned long i,Complex y) {if (flagComplex) ((Complex *) Yarray)[i] = y; else ((double *) Yarray)[i] = real(y);}
	inline void SetY(unsigned long i,double y) {if (flagComplex) ((Complex *) Yarray)[i] = y; else ((double *) Yarray)[i] = y;}
    
	inline double X(unsigned long i) {return (IsXY() ? Xarray[i] : x0+dx*i);};
	inline double SetX(unsigned long i,double x) {return (IsXY() ? Xarray[i]=x : x);};
	
	// Constructors
	Signal(unsigned long allocSize=0,sigXY_Type sigXY_Type=sigYType,sigTypeValue type = sigTypeReal);
    
	// Destructor
	virtual ~Signal();
	
	// Sort according to X
	void XSort();
    
	// Setting X using the X of another signal
	void SetXArray(Signal *sig);
	
	// Append
	void Append(Signal *sig);
	void Append(double dbl);
	void Append(Complex c);
    void Append(double x, double y);
    void Append(double x, Complex y);
    
	// To_string
	char *ToStr(bool flagShort=false);
	
	Signal *Conj();
	Signal *Real();
	Signal *Imag();
	Signal *Phase(bool flagRad = true);
	Signal *Modulus();
    
	// Various operations
	Signal *Add(Signal *sig,Signal *res=NULL)  DECLARETHROW;
	Signal *Add(Complex c,Signal *res=NULL);
	Signal *Add(double dbl,Signal *res=NULL);
	Signal *Sub(Signal *sig,Signal *res=NULL)  DECLARETHROW;
	Signal *Sub(Complex c,Signal *res=NULL);
	Signal *Sub(double dbl,Signal *res=NULL);
	Signal *RSub(Complex c,Signal *res=NULL);
	Signal *RSub(double dbl,Signal *res=NULL);
	Signal *Mul(Signal *sig,Signal *res=NULL)  DECLARETHROW;
	Signal *Mul(Complex c,Signal *res=NULL);
	Signal *Mul(double dbl,Signal *res=NULL);
	Signal *Div(Signal *sig,Signal *res=NULL)  DECLARETHROW;
	Signal *Div(Complex c,Signal *res=NULL);
	Signal *Div(double dbl,Signal *res=NULL);
	Signal *RDiv(Complex c,Signal *res=NULL);
	Signal *RDiv(double dbl,Signal *res=NULL);
	Signal *FDiv(Signal *sig,Signal *res=NULL) DECLARETHROW;
	Signal *FDiv(double dbl,Signal *res=NULL) DECLARETHROW;
	Signal *RFDiv(double dbl,Signal *res=NULL) DECLARETHROW;
	Signal *Mod(Signal *sig,Signal *res=NULL) DECLARETHROW;
	Signal *Mod(double dbl,Signal *res=NULL) DECLARETHROW;
	Signal *RMod(double dbl,Signal *res=NULL) DECLARETHROW;
	Signal *Less(Signal *sig,Signal *res=NULL) DECLARETHROW;
	Signal *Less(double dbl,Signal *res=NULL) DECLARETHROW;
	Signal *RLess(double dbl,Signal *res=NULL) DECLARETHROW;
	Signal *LessEq(Signal *sig,Signal *res=NULL) DECLARETHROW;
	Signal *LessEq(double dbl,Signal *res=NULL) DECLARETHROW;
	Signal *RLessEq(double dbl,Signal *res=NULL) DECLARETHROW;
	Signal *Greater(Signal *sig,Signal *res=NULL) DECLARETHROW;
	Signal *Greater(double dbl,Signal *res=NULL) DECLARETHROW;
	Signal *RGreater(double dbl,Signal *res=NULL) DECLARETHROW;
	Signal *GreaterEq(Signal *sig,Signal *res=NULL) DECLARETHROW;
	Signal *GreaterEq(double dbl,Signal *res=NULL) DECLARETHROW;
	Signal *RGreaterEq(double dbl,Signal *res=NULL) DECLARETHROW;
	Signal *Eq(Signal *sig,Signal *res=NULL)  DECLARETHROW;
	Signal *Eq(Complex c,Signal *res=NULL);
	Signal *Eq(double dbl,Signal *res=NULL);
	Signal *NEq(Signal *sig,Signal *res=NULL)  DECLARETHROW;
	Signal *NEq(Complex c,Signal *res=NULL);
	Signal *NEq(double dbl,Signal *res=NULL);
	Signal *Neg(Signal *res=NULL);
	Signal *And(Signal *sig,Signal *res=NULL) DECLARETHROW;
	Signal *And(double dbl,Signal *res=NULL);
	Signal *Or(Signal *sig,Signal *res=NULL) DECLARETHROW;
	Signal *Or(double dbl,Signal *res=NULL);
	Signal *Abs(Signal *res=NULL) DECLARETHROW;
	Signal *Pow(Signal *sig,Signal *res=NULL) DECLARETHROW;
	Signal *Pow(double dbl,Signal *res=NULL);
	Signal *Pow(Complex dbl,Signal *res=NULL);
	Signal *RPow(double dbl,Signal *res=NULL);
	Signal *RPow(Complex dbl,Signal *res=NULL);
    
	
	/*
     Signal *Sqrt(Signal *res) DECLARETHROW;
     Signal *Log(Signal *res) DECLARETHROW;
     */
    /*	Signal *Sub(Signal *sig,Signal *res)  throw(char *);
     Signal *Sub(double dbl,Signal *res,bool flagSwap=false);
     Signal *Div(Signal *sig,Signal *res)  throw(char *);
     Signal *Div(double dbl,Signal *res,bool flagSwap=false);
     Signal *Mul(Signal *sig,Signal *res)  throw(char *);
     Signal *Mul(double dbl,Signal *res);
     Signal *Eq(Signal *sig,Signal *res)  throw(char *);
     Signal *Eq(double dbl,Signal *res);
     Signal *NEq(Signal *sig,Signal *res)  throw(char *);
     Signal *NEq(double dbl,Signal *res);
     Signal *GreaterEq(Signal *sig,Signal *res)  throw(char *);
     Signal *GreaterEq(double dbl,Signal *res,bool flagSwap=false);
     Signal *SmallerEq(Signal *sig,Signal *res)  throw(char *);
     Signal *SmallerEq(double dbl,Signal *res,bool flagSwap=false);
     Signal *Greater(Signal *sig,Signal *res)  throw(char *);
     Signal *Greater(double dbl,Signal *res,bool flagSwap=false);
     Signal *Smaller(Signal *sig,Signal *res)  throw(char *);
     Signal *Smaller(double dbl,Signal *res,bool flagSwap=false);
     Signal *Or(Signal *sig,Signal *res)  throw(char *);
     Signal *Or(double dbl,Signal *res);
     Signal *And(Signal *sig,Signal *res)  throw(char *);
     Signal *And(double dbl,Signal *res);
     Signal *Pow(Signal *sig,Signal *res)  throw(char *);
     Signal *Pow(double dbl,Signal *res,bool flagSwap=false);
     */
	private :
	long int DichX_(double x,int iMin,int iMax);
	long int DichX(double x); // Get the index associated to a x_value (returns -1 if x out of bound)
    
	public :
	double X2FIndex(double xValue);
	Complex X2Yc(double x, sigInterMode im, BorderType bt, bool flagIndex=false) DECLARETHROW;
	double X2Y(double x, sigInterMode im, BorderType bt, bool flagIndex=false) DECLARETHROW {return real(X2Yc(x,im,bt,flagIndex));}; 
	unsigned long X2Index(double xValue);
	double Index2X(long int index);
    
    void SetFVector(struct fvector *);
};


class SPEXPORT SIGNAL
{
public:
	// The signal it points to
	Signal *signal;
	
	public :
	
    operator Signal *() {return signal;}
    
	// Constructor from a Signal
	SIGNAL(Signal *signal1=NULL) {
		signal = signal1;
		if (signal) signal->ref();
	}
    
	// Constructor with copy
	SIGNAL(const SIGNAL &ptr) {
		signal = ptr.signal;
		if (signal) signal->ref();
	}
	
    // Destructor
	~SIGNAL() {
		if (signal) signal->unref();
	}
	
	// assignation
	SIGNAL operator=(SIGNAL rhs) {
		if (rhs.signal) rhs.signal->ref();
            if (signal) signal->unref();
                signal = rhs.signal;
		return rhs;
	}
    
    bool operator == (SIGNAL rhs) {
        if (!(rhs)) {
            if (!signal) return true;
            return false;
        }
        return signal == &(*(rhs));
    }
    
	// Pointer operator
	inline Signal * operator ->() {
		return signal;
	}
	
	// The * unary operator
	Signal& operator*() {
		return *signal;
	}
	
	// cast operator to boolean 
	operator bool() const {
		return signal != NULL;
	}
	
};


class SignalIter
{
    SIGNAL sig;
    unsigned long index;
    
public :
    
    SignalIter(SIGNAL sig1) {sig = sig1; index = -1;}
    virtual ~SignalIter() {};
    
    inline bool IsComplex() {return sig->IsComplex();};
    
    inline double Next() DECLARETHROW {
        index+=1;
        if (index >= sig->Size()) THROW("");
        return (sig->_Yarray())[index];
    }

    inline Complex Nextc() DECLARETHROW {
        index+=1;
        if (index >= sig->Size()) THROW("");
        return (sig->_Ycarray())[index];
    }

};


extern SIGNAL CovPointProcess(SIGNAL y, SIGNAL z, double delta, double m, double M, long N, int oversampling=1, bool flagM=true);

extern SIGNAL Corr(SIGNAL in1, SIGNAL in2, double dxmin, double dxmax, bool flagNormalized=true) DECLARETHROW;
    
#include <vector>

typedef std::vector<SIGNAL> SIGNAL_1D_ARRAY;
typedef std::vector< std::vector<SIGNAL> > SIGNAL_2D_ARRAY;


extern SIGNAL_1D_ARRAY GQuadrature(double a, double b, int n);

extern    double coucou(double *nparray,unsigned long int size);

#endif
