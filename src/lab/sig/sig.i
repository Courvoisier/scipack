
%module sig


%feature("autodoc", "1");  // 0 == no param types, 1 == show param types


%{
// Includes corresponding to the sig package
#include "sig.h"

// We are using numpy so we have to include some stuffs
#include "numpy/arrayobject.h"
#include "numpy/npy_math.h"	
%}

// C-Initialization of numpy
%init %{
	import_array();
%}



// Include string interface
%include "std_string.i"

// This is the scipack defs
%include "scipackdefs.i"


/////////////////////////////////////////////////////////
//
// The different types for the signal's values
//
/////////////////////////////////////////////////////////
%rename(TYPE_REAL) sigTypeReal;
%rename(TYPE_COMPLEX) sigTypeComplex;
%rename(TYPE_UNKNOWN) sigTypeUnknown;
typedef enum {
    sigTypeUnknown = 0,
    sigTypeReal,
    sigTypeComplex
} sigTypeValue;

/////////////////////////////////////////////////////////
//
// Coding whether X is an array or not
//
/////////////////////////////////////////////////////////
%rename(SIG_Y) sigYType;
%rename(SIG_XY) sigXYType;
%rename(SIG_XY_UNKNOWN) sigXYTypeUnknown;
typedef enum {
    sigXYTypeUnknown = 0,
    sigYType,
    sigXYType
} sigXY_Type;



/////////////////////////////////////////////////////////
//
// The different interpolation mode
//
/////////////////////////////////////////////////////////
%rename(INTER_NONE) sigInterNone;
%rename(INTER_DIRAC) sigInterDirac;
%rename(INTER_CONST_RIGHT) sigInterConstRight;
%rename(INTER_CONST_LEFT) sigInterConstLeft;
%rename(INTER_LINEAR) sigInterLinear;
typedef enum {
    sigInterNone = 0,
    sigInterDirac,
    sigInterConstRight,
    sigInterConstLeft,
    sigInterLinear
} sigInterMode;


/////////////////////////////////////////////////////////
//
// The different border effects
//
/////////////////////////////////////////////////////////
%rename(BORDER_NONE) sigBorderNone;
%rename(BORDER_PER) sigBorderPer;
%rename(BORDER_MIR) sigBorderMir;
%rename(BORDER_MIR1) sigBorderMir1;
%rename(BORDER_CON) sigBorderCon;
%rename(BORDER_0) sigBorder0;

typedef enum {
    sigBorderNone = 0,
    sigBorderPer,
    sigBorderMir,
    sigBorderMir1,
    sigBorderCon,
    sigBorder0
} BorderType;


/////////////////////////////////////////////////////////
//
// Conversion from C Complex to C-Python Complex and vice-versa
//
/////////////////////////////////////////////////////////







/////////////////////////////////////////////////////////
//
// Type Checking
//
/////////////////////////////////////////////////////////

// Test whether a PyObject is an instance of class Signal and returns it if it is
// returns NULL otherwise.
%{
	SPEXPORT Signal *PySignal_Check(PyObject *obj)
	{
		void *argp1;
		if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			return (Signal *) argp1;
		}
		return NULL;
	}
%}

%{
// Checking object is an instance of the class numpy.array.
SPEXPORT bool myPyArray_Check(PyObject *obj)
{
    return PyArray_CheckExact(obj);
}
%}


/////////////////////////////////////////////////////////
//
// Conversions
//
/////////////////////////////////////////////////////////


// Converts a numpy double array into a C double array
%{
SPEXPORT double *_NumpyDoubleArray2CDoubleArray(PyObject *obj, unsigned long &size)
{
    if (!PyArray_CheckExact(obj))
        throw("_NumpyDoubleArray2CDoubleArray(): Failed converting numpy.array to C array (the numpy array should be double only)");
    
    // Get C contiguous data
    PyArrayObject *array = PyArray_GETCONTIGUOUS((PyArrayObject*) obj);
    if (array == NULL) throw("_NumpyDoubleArray2CDoubleArray(): Numpy array should be contiguous data");
    
    // Check dimension is 1
    if (PyArray_NDIM(array) != 1) {
        Py_DECREF(array);
        throw("_NumpyDoubleArray2CDoubleArray(): Numpy array should be 1-dimensional");
    }
    
    // Get size and type
    size = PyArray_DIM(array,0);
/*    if (size != 0 && size1 != size)
        throw("_NumpyDoubleArray2CDoubleArray(): Numpy array not of the expected size");
*/
    if (PyArray_TYPE(array) != NPY_DOUBLE) throw("_NumpyDoubleArray2CDoubleArray() : Numpy array should be an array of double");
    int itemsize = PyArray_ITEMSIZE(array);
    if (itemsize != sizeof(double)) throw("_NumpyDoubleArray2CDoubleArray() : Numpy array should be an array of double with the right sizeof(double)");
    
    return (double *) PyArray_DATA(array);
}
%}

%typemap(in) (double *nparray, unsigned long size) {
    try {
    $1 = _NumpyDoubleArray2CDoubleArray($input,$2);   /* get the array */
    }
    catch(char *str) {
        PyErr_SetString(PyExc_ValueError,str);
        SWIG_fail;
    }
}
%typemap(in) (double *nparray1, unsigned long size1) = (double *nparray, unsigned long size);
%typemap(in) (double *nparray2, unsigned long size2) = (double *nparray, unsigned long size);
%typemap(in) (double *nparray3, unsigned long size3) = (double *nparray, unsigned long size);
%typemap(in) (double *nparray4, unsigned long size4) = (double *nparray, unsigned long size);
%typemap(in) (double *nparray5, unsigned long size5) = (double *nparray, unsigned long size);
%typemap(in) (double *nparray6, unsigned long size6) = (double *nparray, unsigned long size);


////////////////////////////////////////////////////////////////////////////////////////

// Typemap in for SIGNAL
%typemap(in) SIGNAL {
    try {
        Signal *s = _PyObject2Signal($input);
        $1 = SIGNAL(s);
    }
    catch(char *str) {
        PyErr_SetString(PyExc_ValueError,str);
        SWIG_fail;
    }
}




// Tries to convert an object which is converted to a signal
%{
SPEXPORT Signal *_PyObject2Signal(PyObject *obj)
{
    extern Signal *_ListOrTuple2Signal(PyObject *obj);
	extern Signal *_Array2Signal(PyObject *obj);
    
    Signal *s;
    if (PyList_Check(obj) || PyTuple_Check(obj)) {
        try {
            s = _ListOrTuple2Signal(obj);
        }
        catch(...) {
            throw("Failed converting list or tuple to Signal");
        }
    }
    else if ((s = PySignal_Check(obj))) {}
    else if (myPyArray_Check(obj)) {
		try {
            s = _Array2Signal(obj);
        }
		catch(...) {
            throw("Failed converting numpy.array to Signal (should be int, double or complex numpy.array only)");
        }
    }
    else {
        throw("Failed converting argument to Signal");
    }

    return s;
}
%}

%{
    SPEXPORT PyObject *_Signal1DArray2PyObject(SIGNAL_1D_ARRAY &array)
    {
        unsigned int i;
        
        PyObject *result = PyList_New(array.size());
        PyObject *o;
        for (i = 0; i < array.size(); i++) {
            if (!(array[i])) {
                o=Py_None;
                Py_INCREF(o);
            }
            else {
                Signal *sig = &(*(array[i]));
                o = SWIG_NewPointerObj(SWIG_as_voidptr(sig),SWIG_TypeQuery("_p_Signal"),  SWIG_POINTER_OWN |  0 );
                sig->ref();
            }
            PyList_SetItem(result,i,o);
        }
        return result;
    }

    SPEXPORT PyObject *_Signal2DArray2PyObject(SIGNAL_2D_ARRAY &array)
    {
        PyObject *result = PyList_New(array.size());
        
        for (unsigned int i = 0;i<array.size();i++) {
            PyObject *o = _Signal1DArray2PyObject(array[i]);
            PyList_SetItem(result,i,o);
        }

        return result;
    }
%}


%{
    //
	//
	// Convert an array to a signal --> can share the data
	//
	// throw spError
	//
    //
	Signal *_Array2Signal(PyObject *obj)
	{
		// Get C contiguous data
		PyArrayObject *array = PyArray_GETCONTIGUOUS((PyArrayObject*) obj);
		if (array == NULL) throw("Numpy array could not get contiguous data");
		
		// Check dimension is 1
		if (PyArray_NDIM(array) != 1) {
			Py_DECREF(array);
			throw("Numpy array should be 1-dimensional when converted to signal");
		}

		// Get size and type
		npy_intp size = PyArray_DIM(array,0);
		int typenum = PyArray_TYPE(array);
		
		// Case of a double array
		if (PyTypeNum_ISFLOAT(typenum)) {
			int itemsize = PyArray_ITEMSIZE(array);
			if (itemsize == sizeof(double)) {
			  double *ptr = (double *) PyArray_DATA(array);
			  Signal *sig = new Signal(0,sigYType,sigTypeReal);
			  sig->SetYowner(array,ptr,size);
			  Py_DECREF(array);
			  return sig;					
			}
		}

		// Case of a complex array
		else if (PyTypeNum_ISCOMPLEX(typenum)) {
			int itemsize = PyArray_ITEMSIZE(array);
			if (itemsize == sizeof(Complex)) {
			  Complex *ptr = (Complex *) PyArray_DATA(array);
			  Signal *sig = new Signal(0,sigYType,sigTypeComplex);
			  sig->SetYowner(array,ptr,size);
			  Py_DECREF(array);
			  return sig;					
			}
		}

		// Case of a char
		else if (PyTypeNum_ISINTEGER(typenum) && PyArray_ITEMSIZE(array) == sizeof(char) && PyTypeNum_ISSIGNED(typenum)) {
            char *ptr = (char *) PyArray_DATA(array);
            Signal *sig = new Signal(size,sigYType,sigTypeReal);
            for (long i=0;i<size;i++) {
                sig->Y(i) = ptr[i];
			}
            return sig;
		}

		// Case of a signed short
		else if (PyTypeNum_ISINTEGER(typenum) && PyArray_ITEMSIZE(array) == sizeof(short) && PyTypeNum_ISSIGNED(typenum)) {
            short *ptr = (short *) PyArray_DATA(array);
            Signal *sig = new Signal(size,sigYType,sigTypeReal);
            for (long i=0;i<size;i++) {
                sig->Y(i) = ptr[i];
			}
            return sig;
		}
        
		// Case of a signed int
		else if (PyTypeNum_ISINTEGER(typenum) && PyArray_ITEMSIZE(array) == sizeof(int) && PyTypeNum_ISSIGNED(typenum)) {
            int *ptr = (int *) PyArray_DATA(array);
            Signal *sig = new Signal(size,sigYType,sigTypeReal);
            for (long i=0;i<size;i++) {
                sig->Y(i) = ptr[i];
			}
            return sig;
		}

		// Case of a signed long
		else if (PyTypeNum_ISINTEGER(typenum) && PyArray_ITEMSIZE(array) == sizeof(long) && PyTypeNum_ISSIGNED(typenum)) {
            long *ptr = (long *) PyArray_DATA(array);
            Signal *sig = new Signal(size,sigYType,sigTypeReal);
            for (long i=0;i<size;i++) {
                sig->Y(i) = ptr[i];
			}
            return sig;
		}

		// Case of a unsigned short
		else if (PyTypeNum_ISINTEGER(typenum) && PyArray_ITEMSIZE(array) == sizeof(short) && !PyTypeNum_ISSIGNED(typenum)) {
            unsigned short *ptr = (unsigned short *) PyArray_DATA(array);
            Signal *sig = new Signal(size,sigYType,sigTypeReal);
            for (long i=0;i<size;i++) {
                sig->Y(i) = ptr[i];
			}
            return sig;
		}

		// Case of a char
		else if (PyTypeNum_ISINTEGER(typenum) && PyArray_ITEMSIZE(array) == sizeof(unsigned char) && !PyTypeNum_ISSIGNED(typenum)) {
            unsigned char *ptr = (unsigned char *) PyArray_DATA(array);
            Signal *sig = new Signal(size,sigYType,sigTypeReal);
            for (long i=0;i<size;i++) {
                sig->Y(i) = ptr[i];
			}
            return sig;
		}

		// Case of a unsigned int
		else if (PyTypeNum_ISINTEGER(typenum) && PyArray_ITEMSIZE(array) == sizeof(unsigned int) && !PyTypeNum_ISSIGNED(typenum)) {
            unsigned int *ptr = (unsigned int *) PyArray_DATA(array);
            Signal *sig = new Signal(size,sigYType,sigTypeReal);
            for (long i=0;i<size;i++) {
                sig->Y(i) = ptr[i];
			}
            return sig;
		}

		// Case of a unsigned long
		else if (PyTypeNum_ISINTEGER(typenum) && PyArray_ITEMSIZE(array) == sizeof(unsigned long) && !PyTypeNum_ISSIGNED(typenum)) {
            unsigned long *ptr = (unsigned long *) PyArray_DATA(array);
            Signal *sig = new Signal(size,sigYType,sigTypeReal);
            for (long i=0;i<size;i++) {
                sig->Y(i) = ptr[i];
			}
            return sig;
		}

		Py_DECREF(array);
		
		throw("Numpy array should be either int, long, float or complex to be converted to signal");
	}
	
    
    //
	//
	// Convert a signal to an array
	// We give the ownership of the allocation to the newly allocated array.
	//
    //
	PyObject *_Signal2Array(Signal *sig,bool flagY) 
	{
		if (sig->Size() == 0) throw "Sorry, cannot convert an empty signal to an array";

		npy_intp dims[1];
		dims[0] = sig->Size();
		
        PyArrayObject *array;

        // Case we ask for Y signal or we ask for X (which is an array)
        // We must build an array
		if (flagY || sig->IsXY()) {
			array = (PyArrayObject *) PyArray_SimpleNewFromData(1, dims,(sig->IsComplex() && flagY ? NPY_COMPLEX128 : NPY_DOUBLE), (flagY ? (void *) sig->_Yarray() : sig->_Xarray()));
            // If data is already owned by somebody else we should infor the newly created array
            if (flagY && sig->GetYowner() || !flagY && sig->GetXowner()) {

#if (NPY_API_VERSION >= 7)
                PyArray_SetBaseObject(array,(PyObject *) sig->GetYowner());
                Py_INCREF(array);
#else
                array->base = (PyObject *) (flagY ? sig->GetYowner() : sig->GetXowner()); // We inform the newly created array of the owner of the data
                Py_INCREF(array->base);
#endif

            }
		
            // Otherwise the new array should own the data
            else {
                PyArray_FLAGS(array) |= NPY_OWNDATA;
                if (flagY) sig->GiveYownership(array);
                else sig->GiveXownership(array);
            }
            return (PyObject *) array;
		}
        
        // This is the case we ask for X and it is uniformely sampled
		PyObject *start = PyFloat_FromDouble(sig->X0());
		PyObject *stop = PyFloat_FromDouble(sig->X0()+(sig->Size()-1+.5)*sig->Dx());
		PyObject *step = PyFloat_FromDouble(sig->Dx());
		PyObject *o = PySlice_New(start,stop,step);
		Py_DECREF(start);
		Py_DECREF(stop);
		Py_DECREF(step);
		return o;
	}		

%}

/////////////////////////////////////////////////////////
//
// Typemaps
//
/////////////////////////////////////////////////////////

// Typemap out for SIGNAL
%typemap(out) SIGNAL {
    if (!($1)) $result=Py_None;
    else {
        Signal *sig = &(*($1));
        $result = SWIG_NewPointerObj(SWIG_as_voidptr(sig),SWIG_TypeQuery("_p_Signal"),  SWIG_POINTER_OWN |  0 );
        sig->ref();
    }
}

%typemap(typecheck,precedence=SWIG_TYPECHECK_POINTER) SIGNAL {
    if (PySignal_Check($input) || myPyArray_Check($input)) $1 = 1;
    else $1=0;
}

// Typemap in for SIGNAL
%typemap(in) SIGNAL {
    try {
        Signal *s = _PyObject2Signal($input);
        $1 = SIGNAL(s);
    }
    catch(char *str) {
        PyErr_SetString(PyExc_ValueError,str);
        SWIG_fail;
    }
}


// Typemap for SIGNAL_1D_ARRAY
%typemap(out) SIGNAL_1D_ARRAY {
    return _Signal1DArray2PyObject($1);
}
%typemap(typecheck,precedence=SWIG_TYPECHECK_POINTER) SIGNAL_1D_ARRAY {
    if (PyList_Check($input)) $1 = 1;
    else $1=0;
}
%typemap(in) SIGNAL_1D_ARRAY {
    if (!PyList_Check($input)) {
        PyErr_SetString(PyExc_ValueError,"Failed converting argument to 1d-list of Signals");
        SWIG_fail;
    }
    unsigned long nRows = PyList_Size($input);
    $1.resize(nRows);
    for (unsigned long i=0;i<nRows;i++) {
        PyObject *obj = PyList_GetItem($input,i);
        Signal *s = PySignal_Check(obj);
        if (s == NULL) {
            PyErr_SetString(PyExc_ValueError,"Failed converting list to 2d-list of Signals");
            SWIG_fail;
        }
        $1[i] = s;
    }
}

// Typemap for SIGNAL_2D_ARRAY
%typemap(out) SIGNAL_2D_ARRAY {
    return _Signal2DArray2PyObject($1);
}
%typemap(typecheck,precedence=SWIG_TYPECHECK_POINTER) SIGNAL_2D_ARRAY {
    if (PyList_Check($input)) $1 = 1;
    else $1=0;
}
%typemap(in) SIGNAL_2D_ARRAY {
    if (!PyList_Check($input)) {
        PyErr_SetString(PyExc_ValueError,"Failed converting argument to 2d-list of Signals");
        SWIG_fail;
    }
    unsigned long nRows = PyList_Size($input);
    unsigned long nCols = 0;
    $1.resize(nRows);
    for (unsigned long i=0;i<nRows;i++) {
        PyObject *obj = PyList_GetItem($input,i);
        if (!PyList_Check(obj)) {
            PyErr_SetString(PyExc_ValueError,"Failed converting list to 2d-list of Signals");
            SWIG_fail;
        }
        if (nCols == 0) nCols = PyList_Size(obj);
        else if (nCols != PyList_Size(obj)) {
            PyErr_SetString(PyExc_ValueError,"Failed converting list to 2d-list of Signals");
            SWIG_fail;
        }
        $1[i].resize(nCols);
        for (unsigned long j=0;j<nCols;j++) {
            PyObject *obj1 = PyList_GetItem(obj,j);
            Signal *s = PySignal_Check(obj1);
            if (s == NULL) {
                PyErr_SetString(PyExc_ValueError,"Failed converting list to 2d-list of Signals");
                SWIG_fail;
            }
            $1[i][j] = s;
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////
//
// The class itself
//
/////////////////////////////////////////////////////////////////////////////////

%rename(Signal) Signal;


// The Signal class is reference counted
%feature("ref")   Signal "$this->ref();"
%feature("unref") Signal "$this->unref();"



//
//
// Get info from a slice object. 
//
//
//      so          : the slice object
//      in          : NULL if the slice is not used for an extraction otherwise the corresponding signal
//      flagIndex   : true only if extraction (in!=NULL) using index and not abscissa
//
//  --> Returns : start,stop,step and size
//	
//  If extaction with flagIndex==false and unspecified step slice then *step = 0
//
// throw error
//


%{
	void  _SliceGetInfo(PySliceObject *so,Signal *in, bool flagIndex, double *start, double *stop, double *step, unsigned long *size)
	{
		if (in == NULL && (so->start == Py_None || so->stop == Py_None)) throw("When slices are used in constructor, start and stop should be specified");
		if (so->start != Py_None && !PyNumber_Check(so->start) || so->stop != Py_None && !PyNumber_Check(so->stop) || so->step != Py_None && !PyNumber_Check(so->step)) throw("Start, stop and step of slices should be numbers");
		if (PyComplex_Check(so->start) || PyComplex_Check(so->stop)) throw("Start and stop of slices cannot be complex numbers");

        //
        // Here we compute either 
        //   jstep : the step is pure imaginary number it indicates the number of samples in the slice
        //    step : the step is real it corresponds to the step of the slice
        //
		double jstep;
		bool stepSpecified = (so->step != Py_None);
		bool stepComplex = false;
        
        // Case a complex step is specified
		if (stepSpecified && PyComplex_Check(so->step)) {
			stepComplex = true;  
			Py_complex cstep = PyComplex_AsCComplex(so->step);
//			Complex cstep = PY2C_Complex(so->step);
			if (cstep.real != 0) throw("The step in a slice can be complex only if the real part is 0");
			jstep = cstep.imag;
			if (jstep != (long) jstep || jstep <= 0) throw("The step in a slice can be complex only if the imaginary part is a strictly positive integer");
		} 

        // Case a real step is specified
		else if (stepSpecified) {
            *step = (double) PyFloat_AsDouble(so->step);
            if (*step == 0) throw("The step in a slice cannot be 0");
		}
        
        // Case no step is specified
		else {
            if (!flagIndex && in != NULL) *step = 0; //  If extaction with flagIndex==false and unspecified step slice then *step = 0
            else *step = 1;  // Otherwise *step = 1
        }


        //
		// Taking care of start
        //

        // Case no start is specified
		if (so->start == Py_None) {
            if (!flagIndex) { // Abscissa case
                if (*step > 0) *start = in->X(0);
                else *start = in->X(in->Size()-1);
            }
            else {  // Index case
                if (!stepComplex) {
                    if (*step > 0) *start = 0;
                    else *start = in->Size()-1;
                }
                else *start = 0;
            }
		}

        // Case a start is specified
		else {
            *start = (double) PyFloat_AsDouble(so->start);
            if (flagIndex && in != NULL && *start < 0) *start += in->Size();
        }


        //
		// Taking care of stop
        //

        // Case no stop is specified
		if (so->stop == Py_None) {
            if (!flagIndex) { // Abscissa case
                if (*step < 0) *stop = in->X(0);
                else *stop = in->X(in->Size()-1);
            }
            else { // Index case
                if (!stepComplex) {
                    if (*step > 0) *stop = in->Size();
                    else *stop = -1;
                }
                else *stop = in->Size();
            }
		}

        // Case a stop is specified
		else {
            *stop = (double) PyFloat_AsDouble(so->stop);
            if (flagIndex && in != NULL && *stop < 0) *stop += in->Size();
        }

        //
        // Computing size and step
        //

		if (!stepComplex) { // case step was specified
            if (*step != 0) {
                double h = (*stop-*start)/(*step);
                if (flagIndex) *size = (long) floor((h-1e-3)+1);
                else *size = (long) floor((h+1-1e-12));
                if (*size <= 0) throw("Inconsistent slice");
            } 
            else {
                 unsigned long i1 = in->X2Index(*start);
                 unsigned long i2 = in->X2Index(*stop);
//                printf("%ld %ld\n",i1,i2);
                *size = i2-i1+1;
            }

		}
		else { // case step is not specified but jstep is
            if (flagIndex) {
                *step = (*stop-*start)/jstep;
                *size = (long) jstep;
            }
            else {
                *step = (*stop-*start)/jstep;
/*                if (in) {
                    unsigned long i1 = in->X2Index(*start);
                    unsigned long i2 = in->X2Index(*stop);
                    *size = i2-i1+1;
                } else */{
                    *size = (unsigned long) jstep;
                }
            }
		}
		
	}
%}

//
//
// void _SignalGetIndexModificator(PyObject *index,)
// 	
//
//
// throw error
//
//

%{
typedef struct sigIndexModificator {
   BorderType bt;
   sigInterMode im;
   bool flagY;
   bool flagX;
   bool flagIndex;
   bool flagNoLimit;
} SigIndexModificator;

    // This function should not be called directly
    SigIndexModificator _SignalGetIndexModificator(PyObject *index, unsigned long &i)
    {
        SigIndexModificator sim;
        sim.bt = sigBorderNone;
        sim.flagIndex = true;
        sim.im = sigInterNone;
        sim.flagX = false;
        sim.flagY = false;
        sim.flagNoLimit = false;

        bool flag_tuple = PyTuple_Check(index);
        bool flag_str = PyString_Check(index);
        
        if (flag_tuple || flag_str) {
			unsigned long n = 1;
            if (flag_tuple) n = PyObject_Length(index);
			for (;i<n;i++) {
				PyObject *index1;
                if (flag_tuple) index1 = PyTuple_GET_ITEM(index,i);
                else index1 = index;
                if (!PyString_Check(index1)) break;
                char *str = PyString_AS_STRING(index1);
                if (!strcmp(str,"b0")) sim.bt = sigBorder0;
                else if (!strcmp(str,"bmir")) sim.bt = sigBorderMir;
                else if (!strcmp(str,"bmir1")) sim.bt = sigBorderMir1;
                else if (!strcmp(str,"bcon")) sim.bt = sigBorderCon;
                else if (!strcmp(str,"bper")) sim.bt = sigBorderPer;
                else if (!strcmp(str,"nolimit")) sim.flagNoLimit = true;
                else if (!strcmp(str,"x")) sim.flagIndex = false;
                else if (!strcmp(str,"xlin")) {sim.flagIndex = false; sim.im = sigInterLinear;}
                else if (!strcmp(str,"xD")) {sim.flagIndex = false; sim.im = sigInterDirac;}
                else if (!strcmp(str,"x_")) {sim.flagIndex = false; sim.im = sigInterConstRight;}
                else if (!strcmp(str,"_x")) {sim.flagIndex = false; sim.im = sigInterConstLeft;}
                else if (!strcmp(str,"X")) sim.flagX = true;
                else if (!strcmp(str,"Y")) sim.flagY = true;
                else throw("Bad string modificator in index");
			}
        }
        return sim;
    }
%}


//
//
// void _GetEstimatedSize(PyObject *obj, unsigned long &size, bool &flagComplex, Signal *sig)
//
// Get the estimated size and the flagComplex of a signal that is described as a
// Python object : made of (recursive) t-uple or a list of slices,signals,arrays or numbers
//
//   --> Returns size and flagComplex
//
// If sig != NULL, then used for extraction
//	
// throw error
//
//

%{
	// This function should not be called directly
	void __GetEstimatedSize(PyObject *obj, unsigned long &size, bool &flagComplex, Signal *sig)
	{
		// Case of a list
		if (PyList_Check(obj)) {
			unsigned long n = PyList_Size(obj);
			for (unsigned long i=0;i<n;i++) {
				PyObject *o1 = PyList_GetItem(obj,i);
				__GetEstimatedSize(o1,size,flagComplex,sig);
			}
			return;			
		}

		// Case of a t-uple
		if (PyTuple_Check(obj)) {
			unsigned long n = PyObject_Length(obj);
			for (unsigned long i=0;i<n;i++) {
				PyObject *o1 = PyTuple_GET_ITEM(obj,i);
				__GetEstimatedSize(o1,size,flagComplex,sig);
			}
			return;			
		}
		

	  // Case of a signal
		void *argp1;
		if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
		  Signal *sig = reinterpret_cast< Signal * >(argp1);
		  size+=sig->Size();
			flagComplex |= sig->IsComplex();				
			return;			
		}
		
        // Case of an array
        if (PyArray_CheckExact(obj)) {
            PyArrayObject *array = (PyArrayObject*) obj;
			if (PyArray_NDIM(array) != 1) THROW("Numpy array should be 1-dimensional when converted to signal");
			int typenum = PyArray_TYPE(array);
			if (!PyTypeNum_ISFLOAT(typenum) && !PyTypeNum_ISCOMPLEX(typenum) && !PyTypeNum_ISINTEGER(typenum)) {
                THROW("Numpy array should be either made of int, long, double or complex numbers when converted to signal");
            }

			size += PyArray_DIM(array,0);
			if (PyTypeNum_ISCOMPLEX(typenum)) flagComplex = true;
			return;			
		}

		// Case of a slice
		if (PySlice_Check(obj)) {
            PySliceObject *so = (PySliceObject *) obj;
			double start,stop,step;
			unsigned long size1;
			_SliceGetInfo(so,sig,true,&start,&stop,&step,&size1);
			size+=size1;	
			return;			
		}
			// Case of a number
		if (PyNumber_Check(obj)) {
            size++;
			if (PyComplex_Check(obj)) flagComplex = true;
			return;			
		}
			

				
		throw("Unexpected type for signal constructor");
	}

	// This is the main function
	void _GetEstimatedSize(PyObject *obj, unsigned long index, unsigned long &size, bool &flagComplex,Signal *sig)
	{
		size = 0;
		flagComplex = false;
        
        // Case of a t-uple
		if (PyTuple_Check(obj)) {
			unsigned long n = PyObject_Length(obj);
			for (unsigned long i=index;i<n;i++) {
				PyObject *o1 = PyTuple_GET_ITEM(obj,i);
				__GetEstimatedSize(o1,size,flagComplex,sig);
			}
			return;			
		}

        // Case of a regular object
        __GetEstimatedSize(obj,size,flagComplex,sig);
	}
%}


//
//
// Fill up a double or Complex pointer ptr with values in obj
// The pointer has already been allocated with the right size
//
//

%{
	void _ArrayBuildFromList(PyObject *obj, void **ptr, bool flagComplex)
	{
		// Case of a list
		if (PyList_Check(obj)) {
			unsigned long n = PyList_Size(obj);
			for (unsigned long i=0;i<n;i++) {
				PyObject *o1 = PyList_GetItem(obj,i);
				_ArrayBuildFromList(o1,ptr,flagComplex);
			}
			return;
		}
				
		// Case of a t-uple
		if (PyTuple_Check(obj)) {
			unsigned long n = PyObject_Length(obj);
			for (unsigned long i=0;i<n;i++) {
				PyObject *o1 = PyTuple_GET_ITEM(obj,i);
				_ArrayBuildFromList(o1,ptr,flagComplex);
			}
			return;
		}

		// Case of an array
		if (PyArray_CheckExact(obj)) {
            PyArrayObject *array = PyArray_GETCONTIGUOUS((PyArrayObject*) obj);
			int typenum = PyArray_TYPE(array);
			unsigned long size1 = PyArray_DIM(array,0);
			if (PyTypeNum_ISFLOAT(typenum) && !flagComplex || PyTypeNum_ISCOMPLEX(typenum) && flagComplex) {
			  if (flagComplex) memcpy(*ptr,PyArray_DATA(array),sizeof(Complex)*size1);
			  else memcpy(*ptr,PyArray_DATA(array),sizeof(double)*size1);
			} 
			else if (PyTypeNum_ISFLOAT(typenum) || PyTypeNum_ISCOMPLEX(typenum)) {
				double *ptr1 = (double *) PyArray_DATA(array);
				for (unsigned long i=0;i<size1;i++) ((Complex *) *ptr)[i] = ptr1[i];
			}
            else { // Case of Integer
                Signal *sig = _Array2Signal(obj);
                if (flagComplex) {
                    for (unsigned long i=0;i<sig->Size();i++) ((Complex *) *ptr)[i] = sig->_Yarray()[i];
                }
                else {
                    memcpy(*ptr,(void *) sig->_Yarray(),sizeof(double)*sig->Size());
                }
            }
			if (flagComplex) *ptr = ((Complex *) *ptr) + size1;
			else *ptr = ((double *) *ptr) + size1;
			Py_DECREF(array);
			return;
		}

		// Case of a slice
		if (PySlice_Check(obj)) {
            PySliceObject *so = (PySliceObject *) obj;
			double start,stop,step;
			unsigned long size;
			 _SliceGetInfo(so,NULL,false,&start,&stop,&step,&size);
			unsigned long i;
			double d;
			if (flagComplex) {
				for (d=start,i=0;i<size;d+=step,i++) {
					*((Complex *) *ptr) = 	d;
					*ptr = ((Complex *) *ptr)+1;						
				}
			}
			else {
				for (d=start,i=0;i<size;d+=step,i++) {
					*((double *) *ptr) = 	d;
					*ptr = ((double *) *ptr)+1;					
				}					
			}
			return;
		}	
			
		// Case of a signal
		void *argp1;
		if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
		  Signal *sig = reinterpret_cast< Signal * >(argp1);
			bool flagComplexSig = sig->IsComplex();
			if (flagComplexSig == flagComplex) {
				if (flagComplex) memcpy(*ptr,(void *) sig->_Yarray(),sizeof(Complex)*sig->Size());
				else memcpy(*ptr,(void *) sig->_Yarray(),sizeof(double)*sig->Size());
			}
			else {
				for (unsigned long i=0;i<sig->Size();i++) ((Complex *) *ptr)[i] = sig->_Yarray()[i];
			}
			if (flagComplex) *ptr = ((Complex *) *ptr) + sig->Size();
			else *ptr = ((double *) *ptr) + sig->Size();
		}

		// Case it is a complex number
		if (PyComplex_Check(obj)) {
			*((Py_complex *) *ptr) = PyComplex_AsCComplex(obj); 
			*ptr = ((Py_complex *) *ptr)+1;
			return;
		}
			
		// Case of a float
		if (PyNumber_Check(obj)) {
		  double d=PyFloat_AsDouble(obj);
			if (flagComplex) {
				*((Complex *) *ptr) = 	d;
				*ptr = ((Complex *) *ptr)+1;
			}
			else {
				*((double *) *ptr) = 	d;
				*ptr = ((double *) *ptr)+1;					
			}
			return;
		}

		return;
	}


	//
	// Set a Y-signal using a list or t-uple
	//
	// throw error
	//
	void _SignalBuildFromList(Signal *sig, PyObject *obj, bool flagX) {
		unsigned long size = 0;
		bool flagComplex;
	
        // Get estimated size and complex/real flag
        try {
            _GetEstimatedSize(obj,0,size,flagComplex,NULL);
        }
        catch (char *str) {
            throw(str);
        }

        if (flagX && sig->Size() != size) THROW("Inconsistent size between X and Y array");
        if (flagX && flagComplex) THROW("X array cannot be complex");

        // We set the size of the signal to the estimated size
		if (!flagX) sig->SetSize(size,false,sigYType,(flagComplex ? sigTypeComplex : sigTypeReal));
        else sig->SetSize(size,true,sigXYType);
        
        // We get the array and set it.
        void *ptr;
        if (flagX) ptr = sig->_Xarray();
        else ptr = (void *) sig->_Yarray();
        try {
            _ArrayBuildFromList(obj,&ptr,flagComplex);
        }
        catch (char *str) {
            throw(str);
        }
	}


    Signal *_ListOrTuple2Signal(PyObject *obj) {
        Signal *sig = new Signal();
        try {
            _SignalBuildFromList(sig,obj,false);
        }
        catch (char *str) {
            delete sig;
            throw(str);
        }
        return sig;
    }
%}


//
// void _SignalGetIndex(PyObject *index, Signal *in,Signal *out)
// 	
//
// Managing GetIndex of a signal : it manages the command 
//
//      	out = in[obj]
//
// throw spError()
//
%{

    // Get the value of a single extraction and put it in (x,y)
    // returns false if index is out of range and 'nolimit'
    // otherwise it either throws an error or return true
	bool _SignalGetItemNum(double d, Signal *in, SigIndexModificator &sim,double &x, Complex &y) {

        if (sim.flagIndex && d != (long) d) throw("Indices should be integers");
        if (sim.flagIndex && sim.bt == sigBorderNone && d < 0 && -d<=in->Size()) d+=in->Size();
		if (sim.flagIndex && sim.bt == sigBorderNone && (d < 0 || d>=in->Size())) {
            if (sim.flagNoLimit) return false;
            else throw("Index is out of range");
        }

        if (!sim.flagX) y = in->X2Yc(d,sim.im,sim.bt,sim.flagIndex);
        if (sim.flagIndex) x = in->Index2X((long int) d);
        else x = d;
        if (sim.flagX) y = x;
        return true;
    }

    // Same as above but put the result in a signal
	void _SignalGetItemNum_InSig(double d, Signal *in, SigIndexModificator &sim,Signal *out,unsigned long &index)
    {
        Complex y;
        double x;

        if (!_SignalGetItemNum(d,in,sim,x,y)) return;

        if (out->SizeMallocY() <= index) THROW("Badly estimated size");
        if (out->IsComplex()) out->SetY(index, y);
        else out->Y(index) = real(y);

        if (out->IsXY()) out->SetX(index,x);
        
        index++;
    }


	void __SignalGetItemSlice(double xMin, double xStep, double xMax, Signal *in, SigIndexModificator &sim, Signal *out,unsigned long &outSize, double &x0)
    {
        long indexMin,indexMax;
        int step;

        // Case extraction between abscissa [xMin,xMax] must be performed
        // We will perform a loop between [indexMin,indexMax] by step of 1
        if (!sim.flagIndex && xStep == 0) {
            indexMin = in->X2Index(xMin);
            if (in->Index2X(indexMin) < xMin) indexMin++;
            indexMax = in->X2Index(xMax);
            if (in->Index2X(indexMax) <= xMax) indexMax++;
            step = 1;
            sim.flagIndex = true;
        }
        
        // Case extraction between abscissa [xMin,xMax] must be performed using an xStep
        // We take care of this case entirely here
        else if (!sim.flagIndex) {
        
            // We loop on the x
            bool flagX0 = true;
            for (double x = xMin;x<xMax;x+=xStep) {
            
                // Case of an extraction of the X signal --> This is stupid !
                if (sim.flagX) {
                    if (out->SizeMallocY() <= outSize) THROW("Badly evaluated size");
                    if (out->IsComplex()) out->SetY(outSize, x);
                    else out->Y(outSize)=x;
                    outSize++;
                    if (flagX0) {
                        x0 = 0;
                        flagX0 = false;
                    }
                    continue;
                }

                // Case of an extraction of the signal itself
                Complex y;
                try {
                    y = in->X2Yc(x,sim.im,sim.bt,sim.flagIndex);
                }
                catch (char *str) {
                    if (sim.flagNoLimit) continue;
                    THROW("Abscissa is out of range");
                }
                if (out->SizeMallocY() <= outSize) THROW("Badly estimated size");
                if (out->IsComplex()) out->SetY(outSize,y);
                else out->Y(outSize) = real(y);
                if (flagX0) {
                    x0 = x;
                    flagX0 = false;
                }
                outSize++;
                continue;
            }
            return;
        }
        
        // Case of a flagIndex
        else {
            if (xMin != (long) xMin || xMax != (long) xMax || xStep != (long) xStep) {
                THROW("Indices should be integers");
            }
            indexMin = (long) xMin;
            indexMax = (long) xMax;
            step = (long) xStep;
        }

        bool flagX0 = true;
        for (long index = indexMin; step*index<step*indexMax; index+=step) {

            long index1 = index;
            if (sim.flagIndex && sim.bt == sigBorderNone && index1 < 0 && -index1 <= (unsigned long) in->Size()) index1 += in->Size();
            if (sim.flagIndex && sim.bt == sigBorderNone && (index1 < 0 || index1>= (unsigned long) in->Size())) {
                if (sim.flagNoLimit) continue;
                THROW("Index is out of range");
            }
            if (out->SizeMallocY() <= outSize) THROW("Badly estimated size");
            if (sim.flagX) {
                if (in->IsComplex()) out->SetY(outSize,in->Index2X(index1));
                else out->Y(outSize) = in->Index2X(index1);
                if (flagX0) {
                    x0 = 0;
                    flagX0 = false;
                }
            }
            else {
                Complex y = in->X2Yc(index1,sim.im,sim.bt,sim.flagIndex);
                if (in->IsComplex()) out->SetY(outSize, y);
                else out->Y(outSize) = real(y);
                if (in->IsXY() && !sim.flagY) out->SetX(outSize,in->Index2X(index1));
                if (flagX0) {
                    x0 = in->Index2X(index1);
                    flagX0 = false;
                }
            }
            outSize++;
        }
    }



	// This function should not be called directly
	void __SignalGetItem(PyObject *obj, Signal *in,Signal *out,unsigned long &index,SigIndexModificator &sim)
	{
		// Case of a list
		if (PyList_Check(obj)) {
			unsigned long n = PyList_Size(obj);
			for (unsigned long i=0;i<n;i++) {
				PyObject *o1 = PyList_GetItem(obj,i);
				__SignalGetItem(o1,in,out,index,sim);
			}
			return;
		}
		
		// Case of a t-uple
		if (PyTuple_Check(obj)) {
			unsigned long n = PyObject_Length(obj);
			for (unsigned long i=0;i<n;i++) {
				PyObject *o1 = PyTuple_GET_ITEM(obj,i);
				__SignalGetItem(o1,in,out,index,sim);
			}
			return;
		}
				
		// Case of a number
		if (PyNumber_Check(obj)) {
			double d=PyFloat_AsDouble(obj);
            _SignalGetItemNum_InSig(d, in, sim,out,index);
			return;
		}
		
		// Case of a slice
		if (PySlice_Check(obj)) {
            PySliceObject *so = (PySliceObject *) obj;
			double start,stop,step;
			unsigned long size;
            _SliceGetInfo(so,in,sim.flagIndex,&start,&stop,&step,&size);
            double x0;
            __SignalGetItemSlice(start,step,stop,in,sim,out,index,x0);
			return;
		}	
		
		// Case of a signal
		void *argp1;
		if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
            Signal *sig = reinterpret_cast< Signal * >(argp1);
            if (sig->IsComplex()) throw("Indices should be real numbers");
            for (unsigned long i=0;i<sig->Size();i++) {
                double d = sig->Y(i);
                _SignalGetItemNum_InSig(d, in, sim,out,index);
            }
			return;
		}
		
		throw "Bad types for indices";
	}
	
	// This is the main function
	void _SignalGetItem(PyObject *obj,unsigned long index, Signal *in,Signal *out, SigIndexModificator &sim)
	{
		unsigned long outSize = 0;

        // Case of a t-uple
		if (PyTuple_Check(obj)) {
			unsigned long n = PyObject_Length(obj);
			for (unsigned long i=index;i<n;i++) {
				PyObject *o1 = PyTuple_GET_ITEM(obj,i);
				__SignalGetItem(o1,in,out,outSize,sim);
			}
            out->SetSize(outSize,true);
			return;
		}

		__SignalGetItem(obj,in,out,outSize,sim);
        out->SetSize(outSize,true);
	}
%}

//
// void _SignalSetItem(PyObject *index, Signal *out,PyObject *val)
//
// Managing SetIndex of a signal
//
//    out[index] = sval or dval
//	
// throw spError()
//
%{

		// This function should not be called directly
		void __SignalSetItem(PyObject *obj, Signal *out,Signal *sval,Complex cval, unsigned long &index)
		{
			// Case of a list
			if (PyList_Check(obj)) {
				unsigned long n = PyList_Size(obj);
				for (unsigned long i=0;i<n;i++) {
					PyObject *o1 = PyList_GetItem(obj,i);
					__SignalSetItem(o1,out,sval,cval,index);
				}
				return;
			}
			
			// Case of a t-uple
			if (PyTuple_Check(obj)) {
				unsigned long n = PyObject_Length(obj);
				for (unsigned long i=0;i<n;i++) {
					PyObject *o1 = PyTuple_GET_ITEM(obj,i);
					__SignalSetItem(o1,out,sval,cval,index);
				}
				return;
			}
			
			// Case of a number
			if (PyNumber_Check(obj)) {
				double d=PyFloat_AsDouble(obj);
				if (d != (long) d) throw("Indices should be integers");
				if (d < 0 && -d<=out->Size()) d+=out->Size();
				if (d < 0 || d>=out->Size()) throw("Index is out of range");
				if (out->IsComplex()) out->SetY((long) d, (sval ? sval->Yc(index) : cval));
				else out->Y((long) d) = (sval ? sval->Y(index) : real(cval));
				index++;
				return;
			}
			
			// Case of a slice
			if (PySlice_Check(obj)) {
				PySliceObject *so = (PySliceObject *) obj;
				double start,stop,step;
				unsigned long size;
				_SliceGetInfo(so,out,true,&start,&stop,&step,&size);
				unsigned long i;
				double d;
				if (out->IsComplex()) {
					for (d=start,i=0;i<size;d+=step,i++) {
						if (d != (long) d) throw("Indices should be integers");
						if (d < 0 && -d<=out->Size()) d+=out->Size();
						if (d < 0 || d>=out->Size()) throw("Index is out of range");
						out->SetY((long) d, (sval ? sval->Yc(index) : cval));
						index++;
					}
				}
				else {
					for (d=start,i=0;i<size;d+=step,i++) {
						if (d != (long) d) throw("Indices should be integers");
						if (d < 0 && -d<=out->Size()) d+=out->Size();
						if (d < 0 || d>=out->Size()) throw("Index is out of range");
						out->Y((long) d) = (sval ? sval->Y(index) : real(cval));
						index++;
					}					
				}
				return;
			}	
			
			// Case of a signal
			void *argp1;
			if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
				Signal *sig = reinterpret_cast< Signal * >(argp1);
				if (out->IsComplex()) {
					for (unsigned long i=0;i<sig->Size();i++) {
						double d;
						if (sig->IsComplex()) {
							if (imag(sig->Yc(i)) != 0) throw("Indices should be real integers");
							d = real(sig->Yc(i));
						}
						else d = sig->Y(i);
						if (d != (long) d) throw("Indices should be integers");
						if (d < 0 && -d< (unsigned long) out->Size()) d+=out->Size();
						if (d < 0 || d>=out->Size()) throw("Index is out of range");
						out->SetY((long) d, (sval ? sval->Yc(index) : cval));
						index++;
					}
				}
				else {
					for (unsigned long i=0;i<sig->Size();i++) {
						double d;
						if (sig->IsComplex()) {
							if (imag(sig->Yc(i)) != 0) throw("Indices should be real integers");
							d = real(sig->Yc(i));
						}
						else d = sig->Y(i);
						if (d != (long) d) throw("Indices should be integers");
						if (d < 0 && -d<(unsigned long) out->Size()) d+=out->Size();
						if (d < 0 || d>=out->Size()) throw("Index is out of range");
						out->Y((long) d) = (sval ? sval->Y(index) : real(cval));
						index++;
					}
				}
				return;
			}
			
			throw "Bad types for indices";
		}

//
// This is the main function
//
// out[index] = val
//
void _SignalSetItem(PyObject *index, Signal *out,PyObject *val)
{
	void *argp1;
	Signal *sval = NULL;
	Complex cval;
	Py_complex *pcval = (Py_complex *) &cval;
	
	//
	// Case value is a signal --> we just check everything is allright and build sval (the signal corresponding to the value)
	//
	if (SWIG_IsOK(SWIG_ConvertPtr(val, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
		sval = (Signal *) argp1;
		
		// Check size
		unsigned long size;
		bool flagComplex;
        try {
            _GetEstimatedSize(index,0,size,flagComplex,out);
        }
        catch (char *str) {
            throw(str);
        }
		if (size != sval->Size()) throw("Bad assignement : signal on the left and right hand side have different sizes");
		
		// Cast if necessary
		if (sval->IsComplex()) out->CastComplex();
	}
	
	//
	// Case value is a number --> cval
	//
	else if (PyNumber_Check(val)) {
		if (PyComplex_Check(val)) {
            *pcval = PyComplex_AsCComplex(val);
            out->CastComplex();
		}
        else {
            cval=PyFloat_AsDouble(val);
		}
	}
	
	//
	// Other case --> error
	//
	else throw("Right value should be a number or a signal");
	
	// Call subroutine
	unsigned long i = 0;
	__SignalSetItem(index,out,sval,cval,i);	
}

%}




	
//
//
// The signal class
//
//

%feature("python:slot", "tp_getitem", functype="getitemfunc") Signal::__getitem__;
%feature("python:slot", "tp_setitem", functype="setitemfunc") Signal::__setitem__;
%feature("python:slot", "tp_str", functype="reprfunc") Signal::__str__;
%feature("python:slot", "tp_repr", functype="reprfunc") Signal::__repr__;

%DocClass(Signal,
"""This class is used to store a 1d signal. It is made of a Y-array (made of double OR if necessary of complex numbers) 
and either a X-array (if the signal is not uniformely sampled) or two fields dx and x0 (if the signal 
is uniformely sampled). The Y-array can share data with the numpy arrays. So all core numpy functions 
that work on arrays have been translated on Signals. The new functions work on both numpy arrays and/or 
Signals without duplicating any data. One very signifiant difference with numpy managment of arrays is that 
you can increase size of a signal in-place (see the method Append and SetSize). 
Moreover if necessary the Y-array can be automatically casted from double to Complex.

The constructors are

    - __init__([item1,...,itemN])    : concatenating the items where each item can be :  a number, slice, signal, array
    - __init__(size[,val=0])         : To build a constant val signal
    - __init__(numpy_array)          : To build from a numpy array (with either int,float or complex numbers)
    - __init__(x,y)                  : To build a non uniformely sampled signal slices, signals, arrays

Let us point out that in these forms the slices cannot be built using the start:stop:step form. They must be built using the slice() constructor.
The slice does not build the stop value (following the Python convention).
step can be negative in which case stop<start.
However, if step is pure complex, then it indicates a number of points to be built instead of a step.

You should look at the Y and XY shortcuts. They make building signals very easily.

The fields the class Signal are
    - x0 : First abscissa if uniformely sampled signal
    - dx : sampling period if uniformely sampled signal
    - size : number of points of the signal (read-only)
    - sizeMallocX : size of the allocation of the X array (read-only)
    - sizeMallocY : size of the allocation of the Y array (read-only)
    - interMode : one of INTER_CONST_RIGHT, INTER_CONST_LEFT,INTER_LINEAR, INTER_DIRAC it specifies the interpolation scheme
    - name : name of the signal
A lot of operators are defined
    - +, -, *, /, **, %, //
    - +=, -=, *=, /=, **=, %=, //=
    - >, <, >=, <=, and, or (which return a Signal made of 0 and 1)
    - unary + and - 
    - abs(), len(), pow()
    - isreal(), iscomplex(), isinf(), isnan(), isfinite() (which return a Signal made of 0 and 1)

Sophisticated extraction is available : see the methods __getitem__ and __setitem__

Signal is an iterator class.

**Most of the numpy functions have been overloaded to accept both numpy arrays or Signals. The documentation on these functions (see below) is the original documentation of numpy. Though these functions accept Signal as input.**
    """
)
class Signal
{
	public :
	
	%extend {
		std::string __repr__() {return $self->ToStr(false);}
		std::string __str__() {return $self->ToStr(true);}
	}
    
    std::string name;

    sigInterMode interMode;

    %DocMeth(Clear,"Clear()","Clear all the arrays (freeing any allocation)")
    void Clear();
    
    %DocMeth(IsXY,"IsXY() -> bool","Returns True if the signal is not uniformely sampled (i.e., a X-array is coded)")
	bool IsXY();
    
    %DocMeth(GetValueType,"GetValueType() -> TYPE_REAL or TYPE_COMPLEX","Returns the type of the values of the Y-array")
	sigTypeValue GetValueType();
    %DocMeth(IsComplex,"IsComplex() -> bool","Returns True if Y-array is Complex")
	bool IsComplex();
	
    
    %DocMeth(SetSize,"SetSize(size,flagRemember=False,XYType=SIG_XY_UNKNOWN,sigType=TYPE_UNKNOWN)","Sets a new size to the signal. By default the old values are not remembered and the same XY type and sigType values are used. Optional parameters are
    - flagRemember : if True, then values of the signal are kept
    - XYType : One of SIG_Y (no X-array), SIG_XY (X and Y arrays) or SIG_XY_UNKNOWN (old XYType is used).
    - sigType : One of TYPE_REAL (Y-array is made of double's), TYPE_COMPLEX (Y-array is made of Complex's), TYPE_UNKNOWN (Y-array keeps the same type)")
	void SetSize(unsigned long size, bool flagRemember = false, sigXY_Type XYType = sigXYTypeUnknown, sigTypeValue sigType = sigTypeUnknown);
	
	// Destructor
	virtual ~Signal();
	
	// Various operations
    %DocMeth(X2FIndex,"X2FIndex(x) -> float index","Returns a float index corresponding to the abscissa x")
	double X2FIndex(double xValue);
    
    %extend {
        %DocMeth(X2Y,"X2Y(x, iMode =INTER_CONST_RIGHT, bType=BORDER_NONE,index=False) -> number",
        """Get the Y value corresponding to abscissa (or index) x. The parameters are : 
            - x : an abscissa (if index == False) otherwise an index
            - iMode : One of INTER_CONST_RIGHT, INTER_CONST_LEFT,INTER_LINEAR, INTER_DIRAC it specifies the interpolation used
            - bType : One of BORDER_NONE, BORDER_PER (periodic), BORDER_MIR (mirror), BORDER_MIR1 (mirror with extreme repeated), BORDER_CON (constant), BORDER_0 (zero), it specified the border effects (used if x is out of the range the X values)
            - index : if True then x is an index (integer)
        """)

        PyObject * X2Y(double x, sigInterMode iMode=sigInterConstRight, BorderType bType=sigBorderNone, bool index=false) DECLARETHROW {
            PyObject * obj;
            if ($self->IsComplex()) {
							Complex c = $self->X2Yc(x,iMode,bType,index);
                obj = PyComplex_FromCComplex(*((Py_complex *) &c));
            }
            else {
                double d = $self->X2Y(x,iMode,bType,index);
                obj = PyFloat_FromDouble(d);
            }
            return obj;
        }
    }

    %DocMeth(X2Index,"X2Index(x) -> integer index","Returns an integer index corresponding to the abscissa x")
    long int X2Index(double xValue);
//	double Index2X(long int index);

	// Sort according to X
    %DocMeth(XSort,"XSort()","Sorts both (X,Y) according to X")
	void XSort();
};


//	
// Add a new attributes to Signal : dx,x0,size,sizeMallocY,sizeMallocX
//
%extend Signal {
  double dx,x0;
	const unsigned long size;
	const unsigned long sizeMallocY,sizeMallocX;
}
%{
  double Signal_dx_get(Signal *s) {return s->Dx();}
  void Signal_dx_set(Signal *s,double dx) {s->SetDx(dx);}
  double Signal_x0_get(Signal *s) {return s->X0();}
  void Signal_x0_set(Signal *s,double x0) {s->SetX0(x0);}
  const unsigned long Signal_size_get(Signal *s) {return s->Size();}
  const unsigned long Signal_sizeMallocY_get(Signal *s) {return s->SizeMallocY();}
  const unsigned long Signal_sizeMallocX_get(Signal *s) {return s->SizeMallocX();}
%}


/*
 An array interface
%extend Signal {
	PyObject *__array_interface__;
}
%{
	void Signal___array_interface___set(Signal *s,PyObject *o){		
	} 
	PyObject *Signal___array_interface___get(Signal *s) 
	{
		PyObject *dict = PyDict_New();
		PyObject *shape = PyTuple_Pack(1,PyLong_FromLong(s->Size()));		
		PyDict_SetItemString(dict,"shape",shape);
		Py_DECREF(shape);
		char str[10];
		if (s->IsComplex()) sprintf(str,"<c%d",(int) sizeof(Complex));
		else sprintf(str,"<f%d",(int) sizeof(double));
		PyObject *typestr = PyString_FromString(str);
		PyDict_SetItemString(dict,"typestr",typestr);
		Py_DECREF(typestr);
	  PyObject *data = PyTuple_Pack(2,PyLong_FromLong((unsigned long) s->_Yarray()),Py_False);		
		PyDict_SetItemString(dict,"data",data);
		Py_DECREF(data);
		PyDict_SetItemString(dict,"version",PyLong_FromLong(3));
		return dict;		
	}
%}	
*/
	
	

EXCEPTION_ON

//
// Signal[] method
//	
%extend Signal {
    %DocMeth(Yarray,"Yarray() -> numpy array","Returns a numpy array built from the Y-values of the signal")
	PyObject * Yarray() {
	  return _Signal2Array(self,true);	
	}
    %DocMeth(Xarray,"Xarray() -> numpy array","Returns a numpy array built from the abscissa of the signal")
	PyObject * Xarray() {
        if (self->IsXY()) return _Signal2Array(self,false);
        SIGNAL sig = new Signal();
        sig->SetSize(self->Size());
        for (unsigned long i=0; i< self->Size();i++) sig->Y(i) = self->X(i);
        return _Signal2Array(&(*sig),true);
	}

    %DocMeth(__getitem__,"__getitem__(modifier_string1,...,modifier_stringN,*indices) -> Signal or number",
"""This is the extraction mechanism of a Signal. The first arguments are optional they are string modifiers. The available modifiers are 
    - 'b0' : 0 border effect
    - 'bmir' : mirror border effect
    - 'bmir1' : mirror border effect repeating the last and first points
    - 'bcon' : constant border effect
    - 'bper' : periodic border effect
    - 'nolimit' : no error even if no border effects and indices are out of range
    - 'x' : indices are real abscissa values.
    - 'xlin' : Only if 'x' is set :  Signal is piecewise linear.
    - 'x_' : Only if 'x' is set :  Signal is piecewise constant (right continuous).
    - '_x' : Only if 'x' is set :  Signal is piecewise constant (left continuous).
    - 'xD' : Only if 'x' is set :  Signal is made of Dirac's.
    - 'X' : extraction is only performed on X
    - 'Y' : extraction is only performed on Y
Then *indices is a list of
    - real numbers (integers if no 'x' modifier')
    - list of real numbers
    - real valued Signals
    - numpy real valued arrays
    - slices

**Examples**
    
>>> import * from scipack.lab.sig
>>> s = Y[1,2,3,6j]
>>> s
<1,2,3,6j>
>>> s[2:4]
<x0=2,dx=1;3,6j>
>>> s[2:6]  # Generates an error
>>> s['nolimit',2:6]
<x0=2,dx=1;3,6j>
>>> s['bper',2:6]
<x0=2,dx=1;3,6j,1,2>
>>> s['bper','Y',2:6]
<3,6j,1,2>
    
To perform a linear interpolation on x in [2,3[ using 4 points
    
>>> s['xlin',2:3:4j]
<x0=2,dx=0.25;3,2.25+1.5j,1.5+3j,0.75+4.5j>

To perform a piecewise interpolation on a uniform x-grid of period 0.5
    
>>> s['x_',::.5]
<size=6,x0=0,dx=0.5;1,1,2,2,3,3>
""")
    PyObject * __getitem__(PyObject *obj) {

        // Read the Modificators
        unsigned long i = 0;
        SigIndexModificator sim = _SignalGetIndexModificator(obj,i);

        // Case of just 'X' or 'Y'
        if (PyString_Check(obj) && (sim.flagX || sim.flagY)) {
            Signal *out;
            PyObject *result;
            if (sim.flagY || sim.flagX && $self->IsXY()) {
                out = new Signal();
                result = SWIG_NewPointerObj(SWIG_as_voidptr(out),SWIGTYPE_p_Signal, 1 |  0 );
                out->ref();
            }
            if (sim.flagY) {
                out->SetSize($self->Size(),false,sigYType,$self->GetValueType());
                if ($self->IsComplex()) memcpy((void *) out->_Yarray(),(void *) $self->_Yarray(),sizeof(Complex)*$self->Size());
                else memcpy((void *) out->_Yarray(),(void *) $self->_Yarray(),sizeof(double)*$self->Size());
            }
            else if (sim.flagX && $self->IsXY()) {
                out->SetSize($self->Size(),false,sigYType,sigTypeReal);
                memcpy((void *) out->_Yarray(),$self->_Xarray(),sizeof(double)*$self->Size());
            }
            else {
                PyObject *start = PyFloat_FromDouble($self->X0());
                PyObject *stop = PyFloat_FromDouble($self->X0()+$self->Size()*$self->Dx());
                PyObject *step = PyFloat_FromDouble($self->Dx());
                result = PySlice_New(start,stop,step);
                Py_DECREF(start);
                Py_DECREF(stop);
                Py_DECREF(step);
            }
            return result;
        }

        //
		// Case of a number
        //
        PyObject *num = NULL;
        if (PyNumber_Check(obj)) num = obj;
        else if (PyTuple_Check(obj)) {
            if (PyObject_Length(obj) == (long) i+1) {
                num = PyTuple_GET_ITEM(obj,i);
                if (!PyNumber_Check(num)) num = NULL;
            }
        }

		if (num) {
            
            if (PyComplex_Check(num)) THROW("Indices should be real numbers");
			
            double d=PyFloat_AsDouble(num);
            Complex y;
            double x;
            
            // Get the corresponding values 
            bool ans=_SignalGetItemNum(d,$self,sim,x,y);
            if (!ans) return Py_None;
            if ($self->IsComplex()) {
							return PyComplex_FromCComplex(*((Py_complex *) &y));
						}
            else return PyFloat_FromDouble(real(y));
        }


        //
		// Case of a slice
        //
        PyObject *obj1 = NULL;
        PySliceObject *slice = NULL;
        if (PySlice_Check(obj)) slice = (PySliceObject *) obj;
        else if (PyTuple_Check(obj)) {
            if (PyObject_Length(obj) == (long) i+1) {
                obj1 = PyTuple_GET_ITEM(obj,i);
                if (PySlice_Check(obj1)) slice = (PySliceObject *) obj1;
            }
        }
        if (slice != NULL) {
            double start,stop,step;
            unsigned long size;

            // Get info on the slice
            _SliceGetInfo((PySliceObject *) slice,$self,sim.flagIndex,&start,&stop,&step,&size);
//           printf("%g %g %g %ld\n",start,stop,step,size);
            
            // Allocate output signal
            unsigned long outSize = 0;
            Signal *out = new Signal();
            sigXY_Type xy = $self->GetXYType();
            sigTypeValue vt = $self->GetValueType();
            if (sim.flagX) {
                xy = sigYType;
                vt = sigTypeReal;
            }
            if (sim.flagY) xy = sigYType;
            if (!sim.flagIndex && step != 0) xy = sigYType;

            // Set its size
            out->SetSize(size,false,xy,vt);

            // Fill it up
            double x0;
            try {
                __SignalGetItemSlice(start,step,stop,$self,sim,out,outSize,x0);
            }
            catch (char *str) {
                delete out;
                throw str;
            }
            out->SetSize(outSize,true);

            // Now we have to setup the dx and x0 field if necessary
            if (!out->IsXY() && !sim.flagX && !sim.flagY) {
                if (step > 0) {
                    out->SetX0(x0);
                    if (sim.flagIndex) out->SetDx(step*$self->Dx());
                    else out->SetDx(step);
                }
                else {
                    out->SetX0(x0);
//                                               out->SetX0($self->X0());
                    out->SetDx($self->Dx());
                }
            }

            PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(out),SWIGTYPE_p_Signal, 1 |  0 );
            out->ref();
            return result;
        }

        //
		// General case
        //
        
        // Get the estimated size
        unsigned long size;
        bool flagComplex;
		_GetEstimatedSize(obj,i,size,flagComplex,$self);


		// We prepare the output signal
        sigXY_Type xy = $self->GetXYType();
        sigTypeValue vt = $self->GetValueType();
        if (sim.flagX) {
            xy = sigYType;
            vt = sigTypeReal;
        }
        if (sim.flagY) xy = sigYType;
		Signal *out = new Signal();
		out->SetSize(size,false,xy,vt);
		
		// We then loop to get the signal
        try {
		   _SignalGetItem(obj,i,$self,out,sim);
        }
        catch (char *str) {
           delete out;
           throw(str);
        }

		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(out),SWIGTYPE_p_Signal, 1 |  0 );
		out->ref();
		
        // We prepare the output signal
        return result;
	}
};

	//
	// Signal[]= method
	//	
	%extend Signal {
        %DocMeth(__setitem__,"__setitem__(*indices,value)",
"""
Set the Y values of a signal using indices. The indices can be
        - integers
        - list of integers
        - signals (integer valued)
        -slices
        
**Examples**

>>> import * from scipack.lab.sig
>>> s = Y[1,2,3,6j]
>>> s
<1,2,3,6j>
>>> s[2:] = 4
>>> s
<1,2,4,4>        
""")
		PyObject * __setitem__(PyObject *obj,PyObject *val) {
			
			// We then loop
			_SignalSetItem(obj,$self,val);
			
            Py_INCREF(Py_None);
			return Py_None;
		}
	};
	

	//
	// sig + other method
	//	
%feature("python:slot", "tp_add", functype="addfunc") Signal::__add__;
	%extend Signal {
        %DocMeth(__add__,"__add__(signal or number) -> Signal","Performs addition")
		PyObject * __add__(PyObject *obj) {
			
			Signal *res = NULL;
			void *argp1;
			
			// sig + number
			if (PyNumber_Check(obj)) {
				if (PyComplex_Check(obj)) 
				{
					Py_complex pc = PyComplex_AsCComplex(obj);
					res = $self->Add(*((Complex *) &pc));					
				}
				else res = $self->Add(PyFloat_AsDouble(obj));
			}
				
			// sig + sig1
			else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
				Signal *sig1 = reinterpret_cast< Signal * >(argp1);
				res = $self->Add(sig1);
			}
			
			else THROW("Operator + for signals only used with numbers or signals");
			
			PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
			res->ref();

			return result;
		}
	};
%feature("python:slot", "tp_radd", functype="raddfunc") Signal::__radd__;
%extend Signal {
    %DocMeth(__radd__,"__radd__(signal or number) -> Signal","Performs addition")
	PyObject * __radd__(PyObject *obj) {
		return Signal___add__(self,obj);
	}
};
%feature("python:slot", "tp_iadd", functype="iaddfunc") Signal::__iadd__;
%extend Signal {
    %DocMeth(__iadd__,"__iadd__(signal or number) -> Signal","Performs in-place addition")
	PyObject * __iadd__(PyObject *obj) {
		
		Signal *res = NULL;		
		void *argp1;

		// sig + number
		if (PyNumber_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			if (PyComplex_Check(obj)) res = $self->Add(*((Complex *) &pc),$self);
			else res = $self->Add(PyFloat_AsDouble(obj),$self);
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Add(sig1,$self);
		}
		
		else THROW("Operator + for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};

//
// sig - other method
//	
%feature("python:slot", "tp_sub", functype="subfunc") Signal::__sub__;
%extend Signal {
	PyObject * __sub__(PyObject *obj) {
		
		Signal *res = NULL;		
		void *argp1;


		// sig - number
		if (PyNumber_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			if (PyComplex_Check(obj)) res = $self->Sub(*((Complex *) &pc));
			else res = $self->Sub(PyFloat_AsDouble(obj));
		}
		
		// sig - sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Sub(sig1);
		}
		
		else THROW("Operator - for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};
%feature("python:slot", "tp_isub", functype="isubfunc") Signal::__isub__;
%extend Signal {
	PyObject * __isub__(PyObject *obj) {
		
		Signal *res = NULL;		
		void *argp1;

		// sig - number
		if (PyNumber_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			if (PyComplex_Check(obj)) res = $self->Sub(*((Complex *) &pc),$self);
			else res = $self->Sub(PyFloat_AsDouble(obj),$self);
		}
		
		// sig - sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Sub(sig1,$self);
		}
		
		else THROW("Operator - for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};
%feature("python:slot", "tp_rsub", functype="rsubfunc") Signal::__rsub__;
%extend Signal {
	PyObject * __rsub__(PyObject *obj) {

		Signal *res = NULL;
		
		// number-sig
		if (PyNumber_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			if (PyComplex_Check(obj)) res = $self->RSub(*((Complex *) &pc));
			else res = $self->RSub(PyFloat_AsDouble(obj));
		}
		
		else THROW("Operator - for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};
//
// sig * other method
//	
%feature("python:slot", "tp_mul", functype="mulfunc") Signal::__mul__;
%extend Signal {
	PyObject * __mul__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig + number
		if (PyNumber_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			if (PyComplex_Check(obj)) res = $self->Mul(*((Complex *) &pc));
			else res = $self->Mul(PyFloat_AsDouble(obj));
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Mul(sig1);
		}
		
		else THROW("Operator * for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		return result;
	}
};
%feature("python:slot", "tp_imul", functype="imulfunc") Signal::__imul__;
%extend Signal {
	PyObject * __imul__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig + number
		if (PyNumber_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			if (PyComplex_Check(obj)) res = $self->Mul(*((Complex *) &pc),$self);
			else res = $self->Mul(PyFloat_AsDouble(obj),$self);
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Mul(sig1,$self);
		}
		
		else THROW("Operator * for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		return result;
	}
};
%feature("python:slot", "tp_rmul", functype="rmulfunc") Signal::__rmul__;
%extend Signal {
	PyObject * __rmul__(PyObject *obj) {
		return Signal___mul__(self,obj);		
	}
};

//
// sig / other method
//	
%feature("python:slot", "tp_div", functype="divfunc") Signal::__div__;
%extend Signal {
	PyObject * __div__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig + number
		if (PyNumber_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			if (PyComplex_Check(obj)) res = $self->Div(*((Complex *) &pc));
			else res = $self->Div(PyFloat_AsDouble(obj));
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Div(sig1);
		}
		
		else THROW("Operator / for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};
%feature("python:slot", "tp_idiv", functype="idivfunc") Signal::__idiv__;
%extend Signal {
	PyObject * __idiv__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig + number
		if (PyNumber_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			if (PyComplex_Check(obj)) res = $self->Div(*((Complex *) &pc),$self);
			else res = $self->Div(PyFloat_AsDouble(obj),$self);
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Div(sig1,$self);
		}
		
		else THROW("Operator / for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};


%feature("python:slot", "tp_rdiv", functype="rdivfunc") Signal::__rdiv__;
%extend Signal {
	PyObject * __rdiv__(PyObject *obj) {
		
		Signal *res = NULL;
		
		// number/sig
		if (PyNumber_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			if (PyComplex_Check(obj)) res = $self->RDiv(*((Complex *) &pc));
			else res = $self->RDiv(PyFloat_AsDouble(obj));
		}
		
		else THROW("Operator / for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};



//
// sig // other method
//	
%feature("python:slot", "tp_floordiv", functype="floordivfunc") Signal::__floordiv__;
%extend Signal {
	PyObject * __floordiv__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig + number
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::FDiv : operates on real signals only");
			else res = $self->FDiv(PyFloat_AsDouble(obj));
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->FDiv(sig1);
		}
		
		else THROW("Operator / for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};
%feature("python:slot", "tp_ifloordiv", functype="ifloordivfunc") Signal::__ifloordiv__;
%extend Signal {
	PyObject * __ifloordiv__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig + number
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::FDiv : operates on real signals only");
			else res = $self->FDiv(PyFloat_AsDouble(obj),$self);
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->FDiv(sig1,$self);
		}
		
		else THROW("Operator / for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};

%feature("python:slot", "tp_rfloordiv", functype="rdivfunc") Signal::__rfloordiv__;
%extend Signal {
	PyObject * __rfloordiv__(PyObject *obj) {
		
		Signal *res = NULL;
		
		// number/sig
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::FDiv : operates on real signals only");
			else res = $self->RFDiv(PyFloat_AsDouble(obj));
		}
		
		else THROW("Operator / for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};

//
// sig % other method
//	
%feature("python:slot", "tp_mod", functype="modfunc") Signal::__mod__;
%extend Signal {
	PyObject * __mod__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig + number
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::Mod : operates on real signals only");
			else res = $self->Mod(PyFloat_AsDouble(obj));
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Mod(sig1);
		}
		
		else THROW("Operator / for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};
%feature("python:slot", "tp_imod", functype="imodfunc") Signal::__imod__;
%extend Signal {
	PyObject * __imod__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig + number
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::Mod : operates on real signals only");
			else res = $self->Mod(PyFloat_AsDouble(obj),$self);
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Mod(sig1,$self);
		}
		
		else THROW("Operator / for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};

%feature("python:slot", "tp_rmod", functype="rmod") Signal::__rmod__;
%extend Signal {
	PyObject * __rmod__(PyObject *obj) {
		
		Signal *res = NULL;
		
		// number/sig
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::Mod : operates on real signals only");
			else res = $self->RMod(PyFloat_AsDouble(obj));
		}
		
		else THROW("Operator / for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};


//
// sig ** other method
//	
%feature("python:slot", "tp_pow", functype="powfunc") Signal::__pow__;
%extend Signal {
	PyObject * __pow__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig ** number
		if (PyNumber_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			if (PyComplex_Check(obj)) res = $self->Pow(*((Complex *) &pc));
			else res = $self->Pow(PyFloat_AsDouble(obj));
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Pow(sig1);
		}
		
		else THROW("Operator ** for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};
%feature("python:slot", "tp_ipow", functype="ipowfunc") Signal::__ipow__;
%extend Signal {
	PyObject * __ipow__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig ** number
		if (PyNumber_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			if (PyComplex_Check(obj)) res = $self->Pow(*((Complex *) &pc),$self);
			else res = $self->Pow(PyFloat_AsDouble(obj),$self);
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Pow(sig1,$self);
		}
		
		else THROW("Operator ** for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};

%feature("python:slot", "tp_rpow", functype="rpowfunc") Signal::__rpow__;
%extend Signal {
	PyObject * __rpow__(PyObject *obj) {
		
		Signal *res = NULL;
		
		// number/sig
		if (PyNumber_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			if (PyComplex_Check(obj)) res = $self->RPow(*((Complex *) &pc));
			else res = $self->RPow(PyFloat_AsDouble(obj));
		}
		
		else THROW("Operator ** for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};



//
// sig <,<=,>,>= other method
//	
%feature("python:slot", "tp_lt", functype="ltfunc") Signal::__lt__;
%extend Signal {
	PyObject * __lt__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig + number
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::Less : operates on real signals only");
			else res = $self->Less(PyFloat_AsDouble(obj));
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Less(sig1);
		}
		
		else THROW("Operator < for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};

%feature("python:slot", "tp_rless", functype="rless") Signal::__rless__;
%extend Signal {
	PyObject * __rless__(PyObject *obj) {
		
		Signal *res = NULL;
		
		// number/sig
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::Less : operates on real signals only");
			else res = $self->RLess(PyFloat_AsDouble(obj));
		}
		
		else THROW("Operator < for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};

%feature("python:slot", "tp_le", functype="lefunc") Signal::__le__;
%extend Signal {
	PyObject * __le__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig + number
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::Less : operates on real signals only");
			else res = $self->LessEq(PyFloat_AsDouble(obj));
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->LessEq(sig1);
		}
		
		else THROW("Operator < for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};

%feature("python:slot", "tp_rle", functype="rlefunc") Signal::__rle__;
%extend Signal {
	PyObject * __rle__(PyObject *obj) {
		
		Signal *res = NULL;
		
		// number/sig
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::Less : operates on real signals only");
			else res = $self->RLessEq(PyFloat_AsDouble(obj));
		}
		
		else THROW("Operator < for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};

%feature("python:slot", "tp_gt", functype="gtfunc") Signal::__gt__;
%extend Signal {
	PyObject * __gt__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig + number
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::Less : operates on real signals only");
			else res = $self->Greater(PyFloat_AsDouble(obj));
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Greater(sig1);
		}
		
		else THROW("Operator < for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};

%feature("python:slot", "tp_rgt", functype="rgt") Signal::__rgt__;
%extend Signal {
	PyObject * __rgt__(PyObject *obj) {
		
		Signal *res = NULL;
		
		// number/sig
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::Less : operates on real signals only");
			else res = $self->RGreater(PyFloat_AsDouble(obj));
		}
		
		else THROW("Operator < for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};


%feature("python:slot", "tp_ge", functype="gefunc") Signal::__ge__;
%extend Signal {
	PyObject * __ge__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig + number
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::Less : operates on real signals only");
			else res = $self->GreaterEq(PyFloat_AsDouble(obj));
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->GreaterEq(sig1);
		}
		
		else THROW("Operator < for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};

%feature("python:slot", "tp_rge", functype="rge") Signal::__rge__;
%extend Signal {
	PyObject * __rge__(PyObject *obj) {
		
		Signal *res = NULL;
		
		// number/sig
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::Less : operates on real signals only");
			else res = $self->RGreaterEq(PyFloat_AsDouble(obj));
		}
		
		else THROW("Operator < for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};

%feature("python:slot", "tp_eq", functype="eqfunc") Signal::__eq__;
%extend Signal {
	PyObject * __eq__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;

		// sig + number
		if (PyNumber_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			if (PyComplex_Check(obj)) res = $self->Eq(*((Complex *) &pc));
			else res = $self->Eq(PyFloat_AsDouble(obj));
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Eq(sig1);
		}
		
		else THROW("Operator + for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};
%feature("python:slot", "tp_req", functype="reqfunc") Signal::__req__;
%extend Signal {
	PyObject * __radd__(PyObject *obj) {		
		return Signal___eq__(self,obj);
	}
};

%feature("python:slot", "tp_ne", functype="nefunc") Signal::__ne__;
%extend Signal {
	PyObject * __ne__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;
		
		// sig + number
		if (PyNumber_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			if (PyComplex_Check(obj)) res = $self->NEq(*((Complex *) &pc));
			else res = $self->NEq(PyFloat_AsDouble(obj));
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->NEq(sig1);
		}
		
		else THROW("Operator + for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};
%feature("python:slot", "tp_rne", functype="rnefunc") Signal::__rne__;
%extend Signal {
	PyObject * __rne__(PyObject *obj) {		
		return Signal___ne__(self,obj);
	}
};


%feature("python:slot", "tp_and", functype="andfunc") Signal::__and__;
%extend Signal {
	PyObject * __and__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;
		
		// sig + number
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::And : operates on real signals only");
			else res = $self->And(PyFloat_AsDouble(obj));
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->And(sig1);
		}
		
		else THROW("Operator & for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};

%feature("python:slot", "tp_rand", functype="randfunc") Signal::__rand__;
%extend Signal {
	PyObject * __rand__(PyObject *obj) {		
		return Signal___and__(self,obj);
	}
};

%feature("python:slot", "tp_or", functype="andfunc") Signal::__or__;
%extend Signal {
	PyObject * __or__(PyObject *obj) {
		
		Signal *res = NULL;
		void *argp1;
		
		// sig + number
		if (PyNumber_Check(obj)) {
			if (PyComplex_Check(obj)) THROW("Signal::Or : operates on real signals only");
			else res = $self->Or(PyFloat_AsDouble(obj));
		}
		
		// sig + sig1
		else if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig1 = reinterpret_cast< Signal * >(argp1);
			res = $self->Or(sig1);
		}
		
		else THROW("Operator | for signals only used with numbers or signals");
		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		
		return result;
	}
};

%feature("python:slot", "tp_ror", functype="rorfunc") Signal::__ror__;
%extend Signal {
	PyObject * __rand__(PyObject *obj) {		
		return Signal___or__(self,obj);
	}
};

//
// len
//
%feature("python:slot", "tp_len", functype="lenfunc") Signal::__len__;
%extend Signal {
	PyObject * __len__() {
		return PyLong_FromLong($self->Size());
	}
};

//
// +/- unary
//
%feature("python:slot", "tp_pos", functype="posfunc") Signal::__pos__;
%extend Signal {
	PyObject * __pos__() {		
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr($self),SWIGTYPE_p_Signal, 1 |  0 );
		self->ref();
		return result;
	}
};
%feature("python:slot", "tp_neg", functype="negfunc") Signal::__neg__;
%extend Signal {
	PyObject * __neg__() {		
		Signal *res = NULL;
		res = $self->Neg();
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		return result;
	}
};

%feature("python:slot", "tp_abs", functype="absfunc") Signal::__abs__;
%extend Signal {
	PyObject * __abs__() {		
		Signal *res = NULL;
		res = $self->Abs();
		PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(res),SWIGTYPE_p_Signal, 1 |  0 );
		res->ref();
		return result;
	}
};

//
// Signal::Append Method
//
// throw spError
//
%extend Signal {
	
    %DocMeth(Append(double x, double y),"","")
    void Append(double x, double y) {
        $self->Append(x,y);
    }

    %DocMeth(Append(PyObject *obj),"Append(signal or numpy_array, or number or x,y)","Concatenates to the signal another signal or a numpy array or a number or an x and y value (if non uniformely sampled signal). It casts the signal to a complex-valued signal if necessary.")
	void Append(PyObject *obj) {
		void *argp1;
		
		// Case of a signal
		if (SWIG_IsOK(SWIG_ConvertPtr(obj, &argp1,SWIGTYPE_p_Signal, 0 |  0 ))) {
			Signal *sig = reinterpret_cast< Signal * >(argp1);
			$self->Append(sig);
			return;
		}
		
		// Case of an array 
		if (PyArray_CheckExact(obj)) {
			Signal *sig = _Array2Signal(obj);
            sig->ref();
			$self->Append(sig);
			sig->unref();
			return;
		}
		
		// Case of a complex number
		if (PyComplex_Check(obj)) {
			Py_complex pc = PyComplex_AsCComplex(obj);
			$self->Append(*((Complex *) &pc));
			return;
		}
		
		// Case of a double
		double d=PyFloat_AsDouble(obj);
		if (!PyErr_Occurred()) {
			$self->Append(d);
			return;
		}
	
		THROW("Bad type for Append method");
	}

};


//
// some functions on signals
//
extern Signal *isreal(Signal *sig);
%{
	Signal *isreal(Signal *sig)
	{
		Signal *res = new Signal();
		res->SetSize(sig->Size(),false,sig->GetXYType(),sigTypeReal);
		res->SetXArray(sig);

		if (sig->IsComplex()) {
			for (unsigned long i=0;i<sig->Size();i++) {
				res->Y(i) = (imag(sig->Yc(i)) == 0);
			}
		}
		else {
			for (unsigned long i=0;i<sig->Size();i++) res->Y(i) = 1;
		}
		return res;
	}
%}	
extern Signal *iscomplex(Signal *sig);
%{
	Signal *iscomplex(Signal *sig)
	{
		Signal *res = new Signal();
		res->SetSize(sig->Size(),false,sig->GetXYType(),sigTypeReal);
		res->SetXArray(sig);
		
		if (sig->IsComplex()) {
			for (unsigned long i=0;i<sig->Size();i++) {
				res->Y(i) = (imag(sig->Yc(i)) != 0);
			}
		}
		else {
			for (unsigned long i=0;i<sig->Size();i++) res->Y(i) = 0;
		}
		return res;
	}
	%}	

extern Signal *_isinf(Signal *sig);
%{
	Signal *_isinf(Signal *sig)
	{
		Signal *res = new Signal();
		res->SetSize(sig->Size(),false,sig->GetXYType(),sigTypeReal);
		res->SetXArray(sig);
		
		if (sig->IsComplex()) {
			for (unsigned long i=0;i<sig->Size();i++) {
				res->Y(i) = npy_isinf(real(sig->Yc(i)));
			}
		}
		else {
			for (unsigned long i=0;i<sig->Size();i++) {
				res->Y(i) = npy_isinf(sig->Y(i));
			}
		}
		return res;
	}
%}	
extern Signal *_isnan(Signal *sig);
%{
	Signal *_isnan(Signal *sig)
	{
		Signal *res = new Signal();
		res->SetSize(sig->Size(),false,sig->GetXYType(),sigTypeReal);
		res->SetXArray(sig);
		
		if (sig->IsComplex()) {
			for (unsigned long i=0;i<sig->Size();i++) {
				res->Y(i) = npy_isnan(real(sig->Yc(i)));
			}
		}
		else {
			for (unsigned long i=0;i<sig->Size();i++) {
				res->Y(i) = npy_isnan(sig->Y(i));
			}
		}
		return res;
	}
	%}	
extern Signal *_isfinite(Signal *sig);
%{
	Signal *_isfinite(Signal *sig)
	{
		Signal *res = new Signal();
		res->SetSize(sig->Size(),false,sig->GetXYType(),sigTypeReal);
		res->SetXArray(sig);
		
		if (sig->IsComplex()) {
			for (unsigned long i=0;i<sig->Size();i++) {
				res->Y(i) = npy_isfinite(real(sig->Yc(i)));
			}
		}
		else {
			for (unsigned long i=0;i<sig->Size();i++) {
				res->Y(i) = npy_isfinite(sig->Y(i));
			}
		}
		return res;
	}
%}	


//
// Signal Constructors
//
%extend Signal {
	
    // 
    // Constructors of Y-signal with a single argument : 
    //
    // Signal([........])     : init with list of items
    // Signal(size)           : init to zero
    // Signal(array)          : init with array
    //
	Signal(PyObject *obj) {

		// Case of a Signal(n) constructor
		if (PyInt_Check(obj)) {
			Signal *sig = new Signal();
			sig->SetSize(PyInt_AS_LONG(obj));
			for (unsigned long i=0;i<sig->Size();i++) sig->Y(i) = 0;
			return sig;
		}

		if (PyLong_Check(obj)) {
			Signal *sig = new Signal();			
			sig->SetSize(PyLong_AsLong(obj));	
			for (unsigned long i=0;i<sig->Size();i++) sig->Y(i) = 0;
			return sig;
		}

		// Case of a Signal([...]) constructor
        if (PyList_Check(obj) || PyTuple_Check(obj)) {
            Signal *sig = new Signal();
            try {
                _SignalBuildFromList(sig,obj,false);
            }
            catch (char *str) {
                delete sig;
                throw(str);
            }
            return sig;
        }


		// Case of a Signal(array) constuctor
		if (PyArray_CheckExact(obj)) return _Array2Signal(obj);
		
		THROW("Bad arguments for sig.Signal constructor");
	}
	
    // 
    // Constructors of Y-signal with two arguments :
    //
    // Signal(size,value)
    //
	Signal(unsigned long size, PyObject *valobj) {
		
        if (PyComplex_Check(valobj)) {
            Signal *sig = new Signal(0,sigYType,sigTypeComplex);
			sig->SetSize(size);
					Py_complex pc = PyComplex_AsCComplex(valobj);
			Complex v = *((Complex *) &pc);
			for (unsigned long i=0;i<sig->Size();i++) sig->SetY(i, v);
			return sig;
        }
		if (PyNumber_Check(valobj)) {
			Signal *sig = new Signal(0,sigYType,sigTypeReal);
			sig->SetSize(size);
            double v = PyFloat_AsDouble(valobj);
            for (unsigned long i=0;i<sig->Size();i++) sig->Y(i) = v;
            return sig;
        }
		THROW("Bad arguments for sig.Signal constructor");
	}								

    //
    // Constructor of XY signal
    //
    // Signal(X,Y)
    //
	Signal(PyObject *x, PyObject *y) {
        Signal *sig = new Signal();
        try {
            _SignalBuildFromList(sig,y,false);
            if (PySlice_Check(x)) {
                PySliceObject *slice = (PySliceObject *) x;
                double start,stop,step;
                unsigned long size;
                _SliceGetInfo(slice,NULL,false,&start,&stop,&step,&size);
                if (size != sig->Size()) THROW("Inconsistent size between X and Y");
                sig->SetX0(start);
                sig->SetDx(step);
                return sig;
            }
            _SignalBuildFromList(sig,x,true);
        }
        catch (char *str) {
            delete sig;
            throw(str);
        }
        sig->XSort();
        return sig;        
	}
};


EXCEPTION_OFF

                 
                 
%rename(pp_cross_cov) CovPointProcess;
%DocMeth(CovPointProcess,"pp_cross_cov(SIGNAL y, SIGNAL z, double delta, double m, double M, long N, int oversampling=1, bool flagM=True) -> Signal","")
extern SIGNAL CovPointProcess(SIGNAL y, SIGNAL z, double delta, double m, double M, long N, int oversampling=1, bool flagM=True);
                 


//
// ITERATOR Object
//

%rename(_SignalIter) SignalIter;

class SignalIter
{
public :
    
    SignalIter(SIGNAL sig);
    virtual ~SignalIter();
    
    %extend {
    PyObject * next() {
        PyObject *result = NULL;
        try {
        if ($self->IsComplex()) {
					Complex c = $self->Nextc(); 
					result=PyComplex_FromCComplex(*((Py_complex *) &c));
				}
        else result = PyFloat_FromDouble($self->Next());
        }
        catch (const char *) {
            PyErr_SetString(PyExc_StopIteration, "");
            return NULL;
        }
        return result;
    }
    }

};


%extend Signal {
    SignalIter * __iter__() {return new SignalIter($self);}
}

SIGNAL Corr(SIGNAL in1, SIGNAL in2, double dxmin, double dxmax, bool flag_norm=true) DECLARETHROW;

//
// Python extension
//
//
%pythoncode %{
    import os
    execfile(os.path.split(os.path.abspath(__file__))[0]+"/_sig.py")
%}

extern SIGNAL_1D_ARRAY GQuadrature(double a, double b, int n);

