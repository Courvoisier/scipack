
############################################################################

UPDATE LIST 0.13 --> 0.131 
-------------------------

(REMARK : the default .spconfig file has changed)


- Adding cursor control facilities in the signal (see Section on the disp tutorial of the doc)
Taking care of eventual 'label' or 'cursor' field

- Corrected small bug in disp.
Allows to use [] in disp command instead of signal for "blank" slots

- Correction of a bug when displaying complex valued signals

- Correction of a bug which prevented printing

- Adding postscript and svg capabilities to topcontainers (still a few bugs remaining, see docs)

- Adding whiteBrushPrinting attribute to all sp.Object in order to control the background color of objects when printing
(this allows for instance, to a have a plot with white background when printed)
Changed the default .spconfig file accordingly

- Enabling to manage position and size of default disp window through the .spconfig file (see the example file the lines starting
with @DispWindow)

############################################################################



