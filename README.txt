

NOTE : you can find a full html help at http://ebacry.bitbucket.org

It is also in the repo directory DOC/_build/html/index.html



SciPack is a python package for signal processing computations and displays.
-------


There are mainly 2 subpackages :


1- scipack.sciplot (fully compatible with wxWidget and wxPython)
------------------
The displays are handled by a (C++ based) component (SciPlot).
It is fast AND high level. 
It is object oriented and new graphic objects can be defined either directly in C++ or in Python.
Since it uses wxWidget, it is wxPython compatible.
SciPlot creates a wxWidget that is entirely managed (drawings + interactions) by itself.
SciPlot has high level interactors, that can be defined either directly in C++ or in Python, that let's you handle
any events using very short codes.
Attributes of both interactors and objects can be chaned very easily

Example of objects/interactors :
* Graphic objects include spViews, which allow to display objects using real coordinates (x,y axis, ....),
comment boxes (a text box with an arrow pointing on some point),
* Interactors include zooming, scrolling of views, moving objects, changing attributes of any object (both aspects of the object
and ways of interacting with it) using popup menu, synchronization of views so that sveral views share the same axis, ....


- scipack.sciplot.spsig (written in C++)
This package defines a graphic object (in C++) that allows to displays signals (see below)
Signals are displayed very quickly (even when they are large)

- scipack.sciplot.disp (written in Python)
This package essentially defines a very high level function (disp) for controling the display of signals and interactions with them


2- scipack.lab.....   (fully compatible with NUMPY)
--------------
All the modules in scipack.lab are modules used for computations. This package can be built indepedently of scipack.sciplot

For now there is "just"

- scipack.lab.sig (written in C++)
which is a package where Signals are defined.
The Signal structure is used to store information about a signal : it has a Y array (complex or real)
and either it is uniformly sampled (i.e., x0 and dx) or it stores an X array (real valued)
Extraction are easier than for numpy arrays (e.g., you can use real abscissa instead of indices).
Size can be changed dynamically.
Most numpy functions are overloaded in this package so that they work for both signals and regular numpy arrays.

Having the X information along with Y is very useful when one does signal processing.


More to come !




