__author__ = "Emmanuel Bacry"
__copyright__ = "Copyright (C) 2014 Emmanuel Bacry"
__version__ = "0.131"


import scipack.sciplot.sp as sp

class IZoomSynchro(sp.InteractorN):
    """Interactor that handles x/y-bounds synchronization of multiple views. To use this interactor, you should create an instance and use the method AddView() to add the views you want to synchronize."""
        
    def __init__(self,mode='x',pviews = None, name = "zoom_synchro"):
        sp.InteractorN.__init__(self,name=name)
        self._iChangeBound = sp.Interactor1(cb=IZoomSynchro._CBChangeBound,eventType=sp.OnChangeBound,name="IZoomSynchro:ChangeBound").AddTo(self)
        self._iComputeBound = sp.Interactor1(cb=IZoomSynchro._CBComputeBound,eventType=sp.OnComputeBound,name="IZoomSynchro:ComputeBound").AddTo(self)
        self._vList = []
        self.mode = mode
        #        self._instances += [self]
        if (pviews):
            if (isinstance(pviews,basestring)): pviews = [pviews]
            for p in pviews:
                if (isinstance(p,basestring)): l = sp.oget(p)
                else: l = pviews
                for v in l: self.AddView(v)
    
    def CheckObjectIsValid(self,object):
        if (object.IsView()): return True
        sys.stderr.write("Non valid spObject %s for interactor of class %s" %(object,self))
        return False
    
    def Clear(self):
        """Remove all the sp.View's that are synchronized"""
        for v in self._vList:
            if (v()):
                v().RemoveInteractor(self)
        self._vList = []

    def IsEmpty(self):
        for v in self._vList:
            if (v()): return False
        return True

    def AddView(self,view):
        """Add a sp.View to be synchronized"""
        if (not view.IsView()): return
        for v in self._vList:
            if (v().this == view.this): return
        self._vList += [sp.WeakRefObject(view)]
        view.AddInteractor(self)
    
    def NotifyObjectDeleted(self,obj):
        for v in self._vList:
            if (v() is None): self._vList.remove(v)


    #
    # The mode property : either 'x' or 'xy' or ''
    #
    def _GetMode(self): return self._mode
    def _SetMode(self,mode):
        """self.mode = 'x' | 'xy' | ''
            
            if 'x' then only the x-axis is synchronized
            if 'xy' then both x and y-axis are synchronized
            if '' no synchronization is performed
        """
        if (mode != 'x' and mode != 'xy' and mode != ''): raise TypeError,"The mode should be either 'x', 'xy' or ''"
        self._mode = mode
    mode = sp.attribute(_GetMode,_SetMode)
    
    #
    # This is called when the bound of a view is set
    #
    def  _CBChangeBound(self,event):
        
        if (self.mode == ''): return
        
        view = event.object
        
        b = view.bound
        [xMin,xMax,yMin,yMax] = b

        # We loop on the views to synchronize
        for v in self._vList:
            
            # We skip the view 'view'
            if (v().this == view.this): continue
            
            # If mode is 'x' we get the MinBound of the view v() on the range xMin,xMax
            if (self._mode=='x'):
                v().SetMinBound(xMin,xMax)
            
            # Otherwise we just set the bound of v() with the one of view
            else:
                v().bound = view.bound


    #
    # This is called whenever the bound  of a view has been computed.
    # These bounds have been stored in event.xMin, event.xMax, event.yMin, event.yMax
    # We need to update these values accordingly
    #
    def  _CBComputeBound(self,event):
        
        if (self.mode ==''): return
        
        view = event.object

        # We get the bound of the view 'view' (be careful it might be empty !)
        r = sp.RealRect(sp.RealPoint(event.xMin,event.yMin),sp.RealPoint(event.xMax,event.yMax))

        # We loop on the views to synchronize
        for v in self._vList:

            # We skip the view 'view'
            if (v().this == view.this): continue
            
            # We now compute the bound of v()
            
            # Case we want to restrict the computation to the range [event.xMin,event.xMax]
            if (event.flagXFixed): b=v().GetMinBound(event.xMin,event.xMax)

            # Case there is no restriction
            else: b=v().GetMinBound()
            
            # This is the rectangle corresponding to the bounds of v()
            r1 = sp.RealRect(sp.RealPoint(b[0],b[2]),sp.RealPoint(b[1],b[3]))
            
            # We want to make th union of r1 with r
            
            # If r1 is empty, there is nothing to do
            # Case r1 is not empty
            if (not r1.IsEmpty()):
                if (not r.IsEmpty()): r.Union(r1)
                else: r = r1
    
        # Here, we finished computing the bounds. It is stored in r
        # We have to deal with the case r is empty
        if (r.IsEmpty()):
            if (r.GetPosition().x == r.GetPosition1().x): r=RealRect(0,0,1,1)
            else: r=sp.RealRect(r.x,0,r.width,1)
        
        # We return the new bounds in the event fields
        event.xMin = r.GetPosition().x
        event.xMax = r.GetPosition1().x
        if (self._mode == 'x'): return
        event.yMin = r.GetPosition().y
        event.yMax = r.GetPosition1().y

            #        def __del__(self):
            #           print "delete Interactor Synchro"
            #            import sys
#            sys.stdout.flush()

IZoomSynchro._mylist = []

def synchro(*list_of_views,**kwargs):
    """synchro(*list_of_views,[mode='x' or 'xy'])
        
        Synchronize the 'x' axis or both the 'x' and 'y' axis of different views. Arguments are
        
            - list_of_views : each argument should corresponds to a view that
                            should be synchronized with the others.
                            A view can be specified using either the corresponding spObject
                            or a string that corresponds to a pattern for the fullname of the
                            view. Each multiple views correspond to the pattern, all these
                            views are synchronized
        
                            IF list_of_views is empty then all the spView objects found are synchronized
        
            - mode          : if 'x' then only x-axis is synchronized
                            if 'xy' both x-axis and y-axis are synchronized
        
        returns the IZoomSynchro interactor responsible for the synchro
        
        **Examples** : 
            
        >>> synchro(".*::view") # To synchronize all the views named 'view' in all the windows
        
        >>> synchro(oget(klass=sp.View)) # To synchronize all the views disaplyed in all the windows
        
        which gives the same result as
                
        >>> synchro()
        
        To delete all the synchro you created this way
        
        >>> desynchro()
        
        If you want to delete a particular synchro
        
        >>> i = synchro()
        >>> desynchro(i)
    """

    # Update the list
    newlist = []
    for i in IZoomSynchro._mylist:
        if (not i.IsEmpty()): newlist += [i]
    IZoomSynchro._mylist = newlist

    if (kwargs.has_key('mode')): mode = kwargs['mode']
    else: mode = 'x'
    if (len(list_of_views)==0): list_of_views = sp.oget(klass=View)
    i=IZoomSynchro(mode=mode,pviews=list_of_views,name="myzoom_synchro")
    IZoomSynchro._mylist += [i]
    return i
    

def desynchro(synchro_interactor = None):
    """desynchro(synchro_interactor=None) 
        
        Removes synchronization created by the sp.synchro() function
        If no argument then removes all  of them.
        Otherwise removes 'synchro_interactor' which has been returned by synchro().
        
        **Examples** :
        
        >>> i=synchro() # All the views are now synchro
        >>> desynchro(i) # All the views are no longer  synchro        
        """

    if (synchro_interactor):
        IZoomSynchro._mylist.remove(synchro_interactor)
        synchro_interactor.Clear()
        return
    
    for i in IZoomSynchro._mylist:
        if (not i.IsEmpty()): i.Clear()

    IZoomSynchro._mylist = []    


