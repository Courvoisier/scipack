__author__ = "Emmanuel Bacry"
__copyright__ = "Copyright (C) 2014 Emmanuel Bacry"
__version__ = "0.131"

import wx
import sys
import inspect

import scipack.sciplot.sp as sp

from scipack.sciplot.sp import *

#
# A basic popup interactor
#
class IPopupMenu(sp.Interactor1):
    """A basic popup menu interactor.
        When triggered, it creates and displays a popup menu.
        To be used, a single method must be overloaded :
        
            - ``SetMenuItems(self,menu,object)`` 
    
        This method should create items in the menu. 
        In order to create item you should call the method ``self.NewTextItem(text,callback,arg,menu=None)``
        where you specify the text and a callback to be called (with arg as an argument) and the menu the item is in. 
        In order to create an item which is a submenu, you should call ``self.NewSubMenu,text,menu=None)``
        """
    
    
    def __init__(self,eventType,name = "popup"):
        sp.Interactor1.__init__(self,cb = IPopupMenu._CBOnPopup,name=name,eventType=eventType)
    
    def SetMenuItems(self,menu,object):
        pass
    
    
    def NewTextItem(self,text,callback,arg,menu=None):
        if (menu is None): menu = self._menu
        item = menu.Append(-1, text)
        self._items['list'] += [item]
        id = item.GetId()
        self._items[id] = dict(callback=callback,d=arg)
    
    def NewSubMenu(self,text,menu=None):
        if (menu is None): menu = self._menu
        submenu = wx.Menu()
        menu.AppendSubMenu(submenu,text)
        return submenu
    
    def _CBOnPopup(self,event):
        
        self._items = dict(list=[])
        
        # Get the object and the corresponding wxWindow
        obj = event.object
        wxWindow = wx.FindWindowById(obj.container.topcontainer.canvas.wxWindowId)
        self._obj = obj
        
        # Create the popupmenu
        self._menu = wx.Menu()
        self.SetMenuItems(self._menu,obj)
        for item in self._items['list']:
            wxWindow.Bind(wx.EVT_MENU, self._CBOnPopupItemSelected,item)
        pos = (event.mouse.x,event.mouse.y)
        wxWindow.PopupMenu(self._menu,pos)
    
    
    def _CBOnPopupItemSelected(self,wxevent):
        item = self._menu.FindItemById(wxevent.GetId())
        id = item.GetId()
        if (not self._items.has_key(id)): return
        callback=self._items[id]['callback']
        d=self._items[id]['d']
        callback(d)


class IXYScale(IPopupMenu):
    """An interactor for displaying a popup menu in a view to set the x and y scale (lin or log).
        This interactor only work in sp.View's."""
    
    def __init__(self,name = "xyscale_popup_menu"):
        IPopupMenu.__init__(self,name=name,eventType='l')

    def CheckObjectIsValid(self,object):
        if (object.IsView()): return True
        sys.stderr.write("Non valid spObject %s for interactor of class %s" %(object,self))
        return False

    def SetMenuItems(self,menu,object):
        self.NewTextItem('X-linear, Y-linear',self._SetScale,[object,'lin','lin'],menu=menu)
        self.NewTextItem('X-linear, Y-log',self._SetScale,[object,'lin','log'],menu=menu)
        self.NewTextItem('X-log, Y-lin',self._SetScale,[object,'log','lin'],menu=menu)
        self.NewTextItem('X-log, Y-log',self._SetScale,[object,'log','log'],menu=menu)

    def _SetScale(self,arg):
        obj,x,y=arg
        if(x=='lin'): obj.logx=False
        else: obj.logx=True
        if(y=='lin'): obj.logy=False
        else: obj.logy=True



class IHelpAttributesPopupMenu(IPopupMenu):
    """An interactor for displaying a popup menu for navigation on graphic object attributes"""
    
    def __init__(self,name = "attributes_popup_menu"):
        IPopupMenu.__init__(self,name=name,eventType=sp.EventType(sp.OnKeyDown,wx.WXK_ESCAPE))
    
    #
    # Adds an item in the menu for each attribute of the object
    # and links the item to a dialog that allows to change the attribute
    #
    def _AddAttItems(self,menu,obj,key=None):
        
        items = {}
        items['list'] = []
        
        for att in obj.alist() :
            
            d = dict()
            d['att_str'] = att
            if (att=='pen' or att=='font' or att=='brush'):
                d['write_only'] = True
                d['val_str'] = '(write only)'
                d['val'] = None
            else:
                d['write_only'] = False
                d['val'] = obj(att)
                if (isinstance(d['val'],basestring)):
                    val_str = "'"+d['val']+"'"
                else:
                    val_str = d['val'].__repr__()
                    if (len(val_str)>=2):
                        if (val_str[0:2] == 'sp'): val_str = val_str[2:]
                d['val_str'] = val_str
            

            text = d['att_str'] + ' : ' + d['val_str']
            d['obj'] = obj
                    
            if (key): d[key] = True
            
            self.NewTextItem(text,self._ChangeAttributeDialog,d,menu)

    
    def SetMenuItems(self,menu,obj,flag_propagate_up = True,flag_propagate_down = True):

        # Object Attributes
        klass=obj.__class__.__name__
        submenu = self.NewSubMenu(obj.fullname()+' ['+klass+']',menu=menu)
        self._AddAttItems(submenu,obj)
        
        # Container attributes
        if (not (obj.IsContainer() and obj.IsTopContainer()) and flag_propagate_up):
            klass = obj.container.__class__.__name__
            submenu = self.NewSubMenu("Container [%s]"%klass,menu=menu)
            self.SetMenuItems(submenu,obj.container,flag_propagate_down = False)
        
        # Looking for all sons
        if (flag_propagate_down and obj.IsContainer()):
            submenu = self.NewSubMenu("Objects",menu=menu)
            for o in obj.objects:
                self.SetMenuItems(submenu,o,flag_propagate_up=False)
                
        
    def _CopyNameToTerminal(self,d):
        global O
        O = self._obj
        print "\nThe global variable O is now assigned to the object "+self._obj.fullname()
        sys.stdout.flush()
            
    def _ChangeAttributeDialog(self,d):
        
        obj = d['obj']
        
        flagError=False
        while(True):
            
            help = obj.ahelp(d['att_str'],flag_print=False,flag_short=False)[0][1]
            text = "Name    : %s    [%s]\n\n----------\n\n%s\n" % (obj.fullname(),self._obj.__class__.__name__,help)
            if (flagError): text="ERROR : BAD VALUE\n\n"+text
            val = wx.GetTextFromUser(text,default_value=d['val_str'])
            if (val == ''): return
            try:
                val = eval(val)
                obj.__setattr__(d['att_str'],val)
                break
            except:
                flagError= True
        
        Update()



class IHelpInteractorsPopupMenu(IPopupMenu):
    """An interactor for displaying a popup menu for navigation on interactor attributes"""
    
    def __init__(self,name = "interactors_popup_menu"):
        IPopupMenu.__init__(self,name=name,eventType=sp.EventType(sp.OnKeyDown,wx.WXK_TAB))
    
    #
    # Adds an item in the menu for each attribute of the object
    # and links the item to a dialog that allows to change the attribute
    #
    def _AddAttItems(self,menu,obj,flag_container=False):
        
        items = {}
        items['list'] = []
        
        for att in obj.alist() :
            
            d = dict()
            d['att_str'] = att
            if (None):
                d['write_only'] = True
                d['val_str'] = '(write only)'
                d['val'] = None
            else:
                d['write_only'] = False
                d['val'] = obj(att)
                if (isinstance(d['val'],basestring)):
                    val_str = "'"+d['val']+"'"
                else:
                    val_str = d['val'].__repr__()
                    if (len(val_str)>=2):
                        if (val_str[0:2] == 'sp'): val_str = val_str[2:]
                d['val_str'] = val_str
            
            
            text = d['att_str'] + ' : ' + d['val_str']
            d['obj'] = obj
            
            self.NewTextItem(text,self._ChangeAttributeDialog,d,menu)

        if (obj.IsInteractorN()):
            for s in dir(obj):
                if (s[0:11]=='interactor_'):
                    i = obj.__getattribute__(s)
                    self._AddInteractor(menu,i,flag_container)

    
    def _AddInteractor(self,menu,inter,flag_container):
        
        if (flag_container and (isinstance(inter,IHelpInteractorsPopupMenu) or isinstance(inter,IHelpAttributesPopupMenu))): return
        if (not inter.IsInteractorN()):
            type = inter.eventType.type
            if (type == sp.OnTime): return
            if (type == sp.OnDelete): return
            if (type == sp.OnComputeBound): return
            if (type == sp.OnChangeBound): return
            
        if (inter.IsOn()):
            submenu = self.NewSubMenu('o '+inter.name,menu=menu)
        else:
            submenu = self.NewSubMenu('x '+inter.name,menu=menu)

        self._AddAttItems(submenu,inter,flag_container)
            
    
    
    def SetMenuItems(self,menu,obj):

        flag_container = False
        while (True):
            for inter in obj.interactors:
                self._AddInteractor(menu,inter,flag_container = flag_container)
            if (obj.IsTopContainer()): break
            obj = obj.container
            flag_container = True
            menu.AppendSeparator()

    
    def _ChangeAttributeDialog(self,d):
        
        obj = d['obj']
        
        flagError=False
        while(True):
            
            help = obj.ahelp(d['att_str'],flag_print=False,flag_short=False)[0][1]
            text = "Name    : %s    [%s]\n\n----------\n\n%s\n" % (obj.fullname(),self._obj.__class__.__name__,help)
            if (flagError): text="ERROR : BAD VALUE\n\n"+text
            val = wx.GetTextFromUser(text,default_value=d['val_str'])
            if (val == ''): return
            try:
                val = eval(val)
                obj.__setattr__(d['att_str'],val)
                break
            except:
                flagError= True        


