__author__ = "Emmanuel Bacry"
__copyright__ = "Copyright (C) 2014 Emmanuel Bacry"
__version__ = "0.131"

# Convenient function for adding comments to a graph
def CommentAdd(spobject,pt,str="A comment",dist = None, name=None):
    """This command is a shortcut for adding comment to a graph quickly.
        
            - ``spobject`` : either a sp.object or a pattern for its name (the first 
            matching object will be used
            - ``pt`` : the point the comment will point o
            - ``str`` : the comment itself
            - ``dist`` : a point indicating the relative position of the text (respect to the point) in pixels
            - ``name`` : the name of the created object
        """

    if (isinstance(spobject,basestring)):
        patt = spobject
        spobject = oget1(spobject)
        if (spobject is None): raise ValueError,"No object corresponding to pattern %s",patt

    if (dist is None):
        return Comment(spobject,pt=pt,str=str,name=name)
    else:
        return Comment(spobject,pt=pt,str=str,name=name).config(dist=dist)


#
# Pattern matching the object names
#

#
#  :fv1:view:1  -> canvas courant
#  :fv1::1  -> canvas courant
#  c:fv1::1 --> canvas c
#  ::1  --> canvas courant ou tous les canvas !?
#
#  if obj is specified then the path is a path relative to the object
#  (one can use ^ at the begining to refer to the container of the object)
#
#  if klass is specified then only object of the class klass are returned
#

import re
def match_obj_name(patt,obj=None,klass=None,debug=False):
    
    if (patt == None):
        flag_nopatt = True
        patt = '.*::.*'
    else: flag_nopatt = False
    
    #
    # If obj is specified then the pattern patt is a relative pattern
    #
    if (obj):
        # We first deal with the eventual '^' at the begining of the pattern
        while (patt != '' and patt[0] == '^'):
            obj = obj.container
            patt = patt[1:]
            if (obj is None): return []
        if (patt == ''): return [obj]
        if (patt[0] != ':' or not obj.IsContainer()): return []
        return _match_obj_name(obj,patt,klass)
    
    #
    # Case of an absolute pattern
    #
    
    if (patt==''): return
    
    # First we build the canvas list to search in
    if (patt[0] == ":"): # Case of the current canvas
        curCanvas = Canvas.GetCurCanvas()
        if (curCanvas == None): return []
        canvas_list = [curCanvas]
    else:   # Case canvas pattern
        m = re.match('^(.*?)(:.*)$', patt)
        if (not m):
            canvas_patt = patt
            patt = ''
        else:
            canvas_patt=m.group(1)
            patt = m.group(2)
        canvas_list = []
        for canvas in Canvas.canvases():
            m = re.match(canvas_patt,canvas.topcontainer.name)
            if (m): canvas_list += [canvas]
    
    # We then loop on the canvas list
    result = []
    for canvas in canvas_list:
        tc = canvas.topcontainer
        if (flag_nopatt or patt == ''):
            if (not klass or isinstance(tc,klass)): result += [tc]
            if (patt==''): continue
        result += _match_obj_name(tc,patt,klass,debug)
    
    return result


# In this subroutine patt is a pattern starting with :
def _match_obj_name(container,patt,klass=None,debug=False):
    
    # Case the pattern starts with a single :
    if (patt[1] != ':'):
        patt = patt[1:]
        m = re.match('^(.*?)(:.*)$', patt)
        if (not m):
            obj_patt = patt
            patt = ''
        else:
            obj_patt=m.group(1)
            patt = m.group(2)
        
        obj_list = []
        for obj in container.objects:
            m = re.match('^'+obj_patt+'$',obj.name)
            if (not m): continue
            if (not obj.IsContainer() and patt != ''): continue
            if (patt == ''):
                if (not klass or isinstance(obj,klass)):
                    obj_list += [obj]
                continue
            obj_list+=_match_obj_name(obj,patt,klass)
        
        return obj_list
    
    # Case the pattern starts with ::
    if (patt[1] == ':'):
        result = []
        m = re.match(patt[2:],container.name)
        #if (m): result = [container]
        result += _match_obj_name(container,patt[1:],klass)
        for obj in container.objects:
            if (obj.IsContainer()):
                result += _match_obj_name(obj,patt,klass)
        
        return result

#
# (Private methods) Mechanism to set/get the attributes of an object
#

# Get the value of the attribute 'name' of the object
def _att_get(object,name):
    method = "_Get"+name[0].upper()+name[1:]
    try :
        methodToCall = getattr(object, method)
        return methodToCall()
    except AttributeError:
        print "Oops!  Unknown attribute %s for %s" % (name,object)

# Set the attributes 'kwargs' of the object
def _att_set(object,update=True,**kwargs):
    for name in kwargs:
        method = "_Set"+name[0].upper()+name[1:]
        try :
            methodToCall = getattr(object, method)
            args = kwargs[name]
            if (type(args) is list):
                methodToCall(*args)
            elif (type(args) is dict):
                methodToCall(**args)
            else:
                methodToCall(args)
        except AttributeError:
            print "Oops!  Unknown attribute %s for %s" % (name,object)
    if(update): Update()
    return object


# Get the list of removed attributes
def _att_removed_list(obj):
    result = []
    klass = obj.__class__
    while (True):
        try:
            l = klass._attobj_remove_list
            result += l
        except :
            pass
        
        klass = klass.__base__
        if (klass is Object or klass is Interactor): break
    return result


# Get list of the attribute of the object
import pydoc
import inspect
def _att_list(object):
    klass = object.__class__
    methods = inspect.getmembers(klass, predicate=inspect.ismethod)
    att_methods = []
    att_removed = _att_removed_list(object)
    for method in methods:
        m = re.match('^_Set(.*)$', method[0])
        if (not m): continue
        att_name = m.group(1)[0].lower()+m.group(1)[1:]
        if (att_name in att_removed): continue
        att_methods += [att_name]
    
    return att_methods

def __att_help(object,attribute=None,flag_short=True):
    
    klass = object.__class__
    
    if (attribute is not None):
        ll = [attribute,"_Get"+attribute[0].upper()+attribute[1:]]
        properties = inspect.getmembers(klass,lambda x: x.__class__ is property and hasattr(x.fget,'func_name') and x.fget.func_name in ll)
        if (len(properties)==0): return []
        prop = properties[0]
        doc = pydoc.getdoc(prop[1].fset).strip()
        return [[attribute,doc]]
    
    methods = inspect.getmembers(klass,inspect.ismethod)
    method_names = []
    for method in methods: method_names += [method[0]]
    
    properties = inspect.getmembers(klass,lambda x: x.__class__ is property)
    
    res = []
    
    for prop in properties:
        att_name = prop[0]
        method_name = '_Set'+att_name[0].upper()+att_name[1:]
        if (not(method_name in method_names)): continue
        doc = pydoc.getdoc(prop[1].fset).strip()
        if (flag_short):
            g=re.match("(.*)[\n\r]?",doc)
            doc = g.group(1)
        res += [[att_name,doc]]
    
    return res

def _att_help(object,attribute=None,flag_print=True,flag_short=True):
    
    res = __att_help(object,attribute,flag_short)
    if (flag_print):
        for r in res:
            print "%s" %r[1]
        return None
    
    else: return res

#
# (Public methods) Mechanism to set/get the attributes of an object
#

def ahelp(pattern,attribute=None,flag_print=True,flag_short=True):
    """Get/Print help on attributes of an object whose name matches the pattern.
        
        pattern : a string pattern that corresponds to the object fullname
        attribute  : if not None then just ask the help on the specified attribute
        flag_print : if True, then the help is printed (and not 'just' returned)
        flag_short : if True, a short version of the help is asked for
        
        See also : sp.ahelp, sp.alist, sp.aset, sp.aget, sp.oget, sp.oget1
        """
    objects = match_obj_name(pattern)
    for o in objects:
        print o.__str__()
        o.ahelp(attribute,flag_print,flag_short)

def alist(pattern):
    """Gets the list of attributes of an object whose name matches the pattern.
        
        See also : sp.ahelp, sp.alist, sp.aset, sp.aget, sp.oget, sp.oget1
        """
    objects = match_obj_name(pattern)
    for o in objects:
        print o.__str__()
        return o()

def att(pattern,get_attribute=None,update=True,**kwargs):
    """Allows to set/get attribute values of an object.
        This function is designed so that interaction on the terminal is easy.
        --> If pattern is the only argument :
        * If it ends with ':'  it refers to a container --> returns the list of objects of this container
        * If not, the pattern refers to some objects    --> prints the attribute list of the first found object and return the object
        --> If get_attribute is specified
        returns the value of the atribute of the first object found matching the pattern
        --> Otherwise, kwargs corresponds to all the attributes to be set for ALL matching opjects
        Redraw is performed if update is True
        
        See also : sp.ahelp, sp.alist, sp.oget, sp.oget1
        """
    if (isinstance(get_attribute,bool)):
        update = get_attribute
        get_attribute=None
    
    if (pattern[-1] == ':'):
        pattern = pattern + '.*'
        print match_obj_name(pattern)
        return
    objects = match_obj_name(pattern)
    
    if (len(objects) == 0): return
    
    if (get_attribute): return objects[0](get_attribute)
    
    if (len(kwargs)==0):
        print _att_list(objects[0])
        return objects[0]
    
    for o in objects:
        o(update=update,**kwargs)

def oget(pattern=None,klass=None):
    """Gets the list of objects of class klass and whose name matches the pattern.
        
        See also : sp.ahelp, sp.alist, sp.aset, sp.aget, sp.oget, sp.oget1
        """
    return match_obj_name(pattern,klass=klass)

def oget1(pattern=None,klass=None):
    """Gets the first object of class klass and whose name matches the pattern.
        
        See also : sp.ahelp, sp.alist, sp.aset, sp.aget, sp.oget, sp.oget1
        """
    objects =  match_obj_name(pattern,klass=klass)
    if (len(objects)>0): return objects[0]
    return None

def attribute_hide(klass,*names):
    """Remove inherited attributes.
        
        klass : the klass object of an spInteractor or an spObject
        *names : the names of the attributes to be removed
        """
    try:
        klass._attobj_remove_list
    except AttributeError:
        klass._attobj_remove_list = []
        klass._attobj_remove_list += names


def attribute(getter,setter):
    doc = "**Attribute** "+setter.func_doc
    p = property(getter,setter,None,doc)
    p.fset.func_doc=doc
    p.fget.func_doc=doc
    return p

def attribute(getter,setter):
    doc = "**Attribute** "+setter.func_doc
    p = property(getter,setter,None,doc)
    p.fset.func_doc=doc
    p.fget.func_doc=doc
    return p

import wx
class wxTopContainer(wx.Window):
    """A class that implements a wxWidget which is associated to a spTopContainer.
        The corresponding spTopContainer is accessed through self.tc
        
        - frame : the wxWidget it belongs to
        - pos   : the position of the wxTopContainer
        - size  : its size
        - tc    : the corresponding spTopContainer. If None, then a new spTopContainer is created and assigned to self.tc
        Otherwise self.tc is assigned to tc. This allows to create a new class derived
        from spTopContainer and use an instance of this new class as tc.
        """
    def __init__(self,frame,pos=(0,0),size=(100,100),tc=None):
        if (tc is None):
            tc = TopContainer(canvas = None, name = "SciPlot").config()
        wx.Window.__init__(self,frame,wx.ID_ANY,pos,size)
        self.tc = tc
        self._canvas = Canvas(self.Id,tc=tc,name=tc.name)


def GetWindow(all = False):
    """A function to get the current window or all windows containing sciplot objects"""

    if (not all): return(wx.FindWindowById(Canvas.GetCurCanvas().wxWindowId).TopLevelParent)
    
    id_list = []
    win_list = []
    for c in Canvas.canvases():
        id = c.wxWindowId
        if (id in id_list): continue
        id_list.append(id)
        win_list.append(wx.FindWindowById(id).TopLevelParent)

    return win_list



