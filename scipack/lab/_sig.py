
__author__ = "Emmanuel Bacry"
__copyright__ = "Copyright (C) 2014 Emmanuel Bacry"
__version__ = "0.131"

#
# Python code for shortcuts :
#
#   Y[...]
#   XY[X,Y]
#
class SignalShortcuts:
    """These are shortcuts for building signals.
        There are mainly two shortcuts syntaxes:
        
        Y[...] : allows to build Y signals (the abscissa are uniformely sampled x0=0 and dx=1)
                 [...] is a comma separated list of any combination of
                    - numbers
                    - signals
                    - numpy arrays
                    - slices of the forms
                        * a:b                    --> all the integers in [a,b[ with a step of 1
                        * a:b:s (with s real)    --> all the integers in [a,b[ with a step of s (let us note that step can be negative if b<a)
                        * a:b:s (with s complex) --> The interval [a,b[ uniformely sampled on s points
                    - a sequence of the elements above
        
        XY[x,y] : allows to build XY signals where x and y have the same form as the [...] above 
                  Warning : x and y must have the same size and x should be real valued
                  Warning : the signal will be automatically sorted accoring to the x values
        
        **Examples** :
        
        >>> import * from scipack.lab.sig
        >>> Y[1,2,3,6j]    
        <1,2,3,6j>
        >>> Y[1,3:9:2]
        <1,3,5,7>
        >>> Y[0:3:6j]
        <0,0.5,1,1.5,2,2.5>
        
        and To build the sinus function on [0,2pi[ sampled on 100 points
        
        >>> import * from scipack.lab.sig
        >>> x = slice(0,2*pi,100j)
        >>> XY[x,sin(x)]
        <size=100,x0=0,dx=0,0628319;0,0,06279051952931337,0,1253332335643043,0,1873813145857246,0,2486898871648548,0,3090169943749474,...>
        """
    
    def __init__(self,xy=False):
        self.xy = xy
    def __getitem__(self,args):
        if (self.xy):
            return Signal(args[0],args[1])
        else :
            return Signal([args])
    def __call__(self,*args,**kwargs):
        return Signal(*args,**kwargs)
	
Y = SignalShortcuts()
XY = SignalShortcuts(xy=True)



import numpy as np

#
# Create numpy matrix shortcut
#
def mat0(nRows,nCols=None):
    """Returns a numpy null matrix. If nCols is not specified then the matrix is squared."""
    if (nCols is None): nCols = nRows
    return np.mat(np.zeros(shape=(nCols,nRows)))

def mat1(size):
    """Returns a numpy squared matrix corresponding to identity."""
    return np.mat(np.eye(size))

#
# Here we create some unary operators returning signal of doubles
#
unary = [np.trunc,np.ceil,np.floor,np.rad2deg,np.deg2rad,np.arctanh,np.arccosh,np.arcsinh,np.tanh,np.cosh,np.sinh,np.arctan,np.arccos,np.arcsin,np.tan,np.cos,np.sin,np.square,np.sqrt,np.log1p,np.expm1,np.log10,np.log2,np.log,np.exp2,np.exp,np.conjugate,np.absolute,np.negative,np.sign,np.rint,np.real,np.imag,np.angle,np.sort]
for u in unary:
   str = """
def %(name)s(y,out=None):
   if (isinstance(y,slice)): y = Y[y]
   if (not isinstance(y,Signal)):
      return np.%(name)s(y,out)
   if (out is None):
      if (not y.IsXY()):
         z = Y(np.%(name)s(y.Yarray()))
         z.x0 = y.x0
         z.dx = y.dx
         return z
      else:
         return XY[y['X'],np.%(name)s(y.Yarray())]
   elif (out is y):
      a = y.Yarray()
      np.%(name)s(a,a)
      return y
   else:
      out[:] = XY[y['X'],np.%(name)s(y.Yarray())]
%(name)s.__doc__ = np.%(name)s.__doc__
""" % {"name":u.__name__}
   exec(str)
del unary
conj = conjugate
fabs = absolute

#
# Unary operator returning a signal of booleans
#
# The three operators isnan, isinfinite and isinf are
# implemented in C for signals
# There are two other functions defined in C : isreal and iscomplex
#
others = [np.isnan,np.isfinite,np.isinf]
for u in others:
   str = """
def %(name)s(y,out=None):
  if (isinstance(y,slice)): y = Y[y]
  if (not isinstance(y,Signal)):
    return np.%(name)s(y,out)
  return _%(name)s(y)
%(name)s.__doc__ = np.%(name)s.__doc__
""" % {"name":u.__name__}
   exec(str)
del others

#
# Binary operators returning a signal of doubles
#
others = [np.power]
for u in others:
   str = """
def %(name)s(x,y,out=None):
   if (isinstance(y,slice)): y = Y[y]
   if (isinstance(x,slice)): x = Y[x]
   if ((not isinstance(y,Signal)) and (not isinstance(x,Signal))):
      return np.%(name)s(x,y,out)
   if (out is None):
       if (not y.IsXY()):
          z = Y(np.%(name)s(x.Yarray(),y.Yarray()))
          z.x0 = y.x0
          z.dx = y.dx
          return z
       else:
          return XY[y['X'],np.%(name)s(x.Yarray(),y.Yarray())]
   elif (out is y):
      a = y.Yarray()
      np.%(name)s(x.Yarray(),a,a)
      return y
   elif (out is x):
      a = x.Yarray()
      np.%(name)s(a,y.Yarray(),a)
      return x
   else:
      out[:] = YY(y,np.%(name)s(x.Yarray(),y.Yarray()))
%(name)s.__doc__ = np.%(name)s.__doc__
""" % {"name":u.__name__}
   exec(str)
del others


#
# Unary operators that return a single boolean
#
others = [np.all,np.any,np.unique]
for u in others:
   str = """
def %(name)s(x,*args,**kwargs):
   if (isinstance(x,slice)): x = Y[x]
   if (not isinstance(x,Signal)):
      return np.%(name)s(x,*args,**kwargs)
   else:
      return np.%(name)s(x.Yarray())
%(name)s.__doc__ = np.%(name)s.__doc__
""" % {"name":u.__name__}
   exec(str)
del others


#
# Unary operators that return a single number
#
others = [np.sum,np.mean,np.max,np.min,np.argmin,np.argmax]
for u in others:
   str = """
def %(name)s(x,*args,**kwargs):
   if (isinstance(x,slice)): x = Y[x]
   if (not isinstance(x,Signal)):
      return np.%(name)s(x,*args,**kwargs)
   else:
      return np.%(name)s(x.Yarray())
%(name)s.__doc__ = np.%(name)s.__doc__
""" % {"name":u.__name__}
   exec(str)
del others

_min = min
def min(*args,**kwargs):
    if (len(args)==1 and len(kwargs)==0 and isinstance(args[0],Signal)): return amin(args[0])
    if (isinstance(args[0], np.ndarray)): return np.min(*args,**kwargs)
    return _min(*args,**kwargs)
_max = max
def max(*args,**kwargs):
    if (len(args)==1 and len(kwargs)==0 and isinstance(args[0],Signal)): return amax(args[0])
    if (isinstance(args[0], np.ndarray)): return np.max(*args,**kwargs)
    return _max(*args,**kwargs)


# Special treatment for interMode
others = [np.cumsum]
for u in others:
    str = """
def %(name)s(x,*args,**kwargs):
    if (isinstance(x,slice)): x = Y[x]
    if (not isinstance(x,Signal)):
        return np.%(name)s(x,*args,**kwargs)
    else:
        if (not x.IsXY()):
            z = Y(np.%(name)s(x.Yarray()))
            z.x0 = x.x0
            z.dx = x.dx
        else:
            z=XY[x['X'],np.%(name)s(x.Yarray())]
        if (x.interMode == INTER_DIRAC): z.interMode = INTER_CONST_RIGHT
        return z
%(name)s.__doc__ = np.%(name)s.__doc__
""" % {"name":u.__name__}
    exec(str)
prim = cumsum
del others


others = [np.round]
for u in others:
   str = """
def %(name)s(x,*args,**kwargs):
   if (isinstance(x,slice)): x = Y[x]
   if (not isinstance(x,Signal)):
      return np.%(name)s(x,*args,**kwargs)
   else:
      if (not x.IsXY()):
         z = Y(np.%(name)s(x.Yarray()))
         z.x0 = x.x0
         z.dx = x.dx
         return z
      else:
         return XY[x['X'],np.%(name)s(x.Yarray())]
%(name)s.__doc__ = np.%(name)s.__doc__
""" % {"name":u.__name__}
   exec(str)
del others



def der(sig):
    """This is the inverse of the prim() function."""
    if (not sig.IsXY()):
        z=Y[[sig[0],sig[1:]-sig[:-1]]]
        z.x0 = sig.x0
        z.dx = sig.dx
    else:
        z=XY[sig['X'],[sig[0],sig[1:]-sig[:-1]]]

    if (sig.interMode == INTER_CONST_RIGHT or sig.interMode == INTER_CONST_LEFT): z.interMode = INTER_DIRAC
    return z


#
# rnorm
#
def rnorm(size,mean=0.0,sigma=1.00):
    """A shortcut for building a gaussian white signal of size.
        You should use numpy.random package for other laws"""
    return Y[np.random.normal(mean,sigma,size)]

def where(y,*args):
    if (isinstance(y,slice)): y = Y[y]
    if (not isinstance(y,Signal)):
        return np.where(y,*args)
    return Y[(np.where(y.Yarray()==1,*args))[0]]
where.__doc__ = np.where.__doc__ 


#
# copy
#
def copy(sig):
    if (not isinstance(sig,Signal)):
        return np.copy(sig)
    if (sig.IsXY()):
        x = sig['X']
        y = sig['Y']
        s = XY[x,y]
    else:
        s = Signal(sig.size)
        s.dx = sig.dx
        s.x0 = sig.x0
        s = sig[:]
    s.name = sig.name
    s.interMode= sig.interMode
    return s
copy.__doc__=np.copy.__doc__



#
# Histogram
#
def histogram(sig,bins=10,range=None,normed=False,weights=None,density=None):
   if (isinstance(sig,slice)): sig = Y[sig]
   if (not isinstance(sig,Signal)):
       return np.histogram(sig,bins=bins,range=range,normed=normed,weights=weights,density=density)
   y = sig.Yarray()
   h = np.histogram(y,bins=bins,range=range,normed=normed,weights=weights,density=density)
   x = h[1]
   for i in np.arange(x.size-1):
      x[i] = (x[i]+x[i+1])/2
   return XY[x[:-1],np.asfarray(h[0])]
histogram.__doc__=np.histogram.__doc__
histo = histogram
    

# Write header of a binary file
def _writeheader(fid,sig):
    header = "Signal Header\n"
    xySig = "xy\n"
    ySig = "y\n"
    fid.write(header)
    if (sig.IsXY()): fid.write(xySig)
    else : fid.write(ySig)
    
def _tryread(fid,str):
    pos = fid.tell()
    if (fid.read(len(str))==str):
        return True
    fid.seek(pos)
    return False

# Read header for binary files
def _readheader(fid):
    header = "Signal Header\n"
    xySig = "xy\n"
    ySig = "y\n"
    d = dict()
    if (not _tryread(fid,header)): raise ValueError('Bad file format')

    if (_tryread(fid,xySig)):
        d['xy']=True
    elif (_tryread(fid,ySig)):
        d['xy']=False
    else: raise ValueError('Bad file format')

    return d
    
# Save a signal in a binary file
def save(fid,self):
    """Save a signal into a binary file specified either by its name or by its descriptor (in each case it is the argument fid)
        
        Instead, you should consider using the cPickle package, e.g.,
        
        >>> import cPickle
        >>> cPickle.dump(self,fid)
        
        and to load
            
        >>> cPickle.load(fid)        
        """
    flagClose = False
    if (not isinstance(fid,file)):
        fid = open(fid, 'wb')
        flagClose = True

    _writeheader(fid,self)

    x = self['X']
    if (isinstance(x,slice)):
        np.save(fid,np.array([x.start,x.stop,x.step]))
    else:
        np.save(fid,x)
    np.save(fid,self.Yarray())
    if (flagClose): fid.close()

# Save a signal in a text file
def savetxt(fid,sig, type="y", fmt='%.18e', delimiter=' ', newline='\n'):
    """Save a signal into a mulicolumn text file
        
        fid       : name or descriptor of the text file
        sig       : The signal to be saved
        type      : either 'y' (single colum) or 'xy' (two columns)
        fmt       : the text format used
        delimiter : the delimiter between the columns
        newline   : the newline character to be used
            """
    flagClose = False
    if (not isinstance(fid,file)):
        fid = open(fid, 'w')
        flagClose = True

    if (type == "x" or type == "xy"):
        x = sig['X']
        if (isinstance(x,slice)):
            x = np.arange(0,sig.size)*x.step+x.start
    
    if (type == "x"):
        np.savetxt(fid,x,fmt=fmt,delimiter=delimiter,newline=newline)
    elif (type == "y"):
        np.savetxt(fid,sig.Yarray(),fmt=fmt,delimiter=delimiter,newline=newline)
    elif (type == "xy"):
        np.savetxt(fid,np.array([x,sig.Yarray()]).transpose(),fmt=fmt,delimiter=delimiter,newline=newline)
    else:
        if (flagClose): fid.close()
        raise ValueError('Bad value for type argument')
        
    if (flagClose):
        fid.close()
    

# Load a signal from a binary
def load(fid):
    """Allows to load a signal from a binary file created by the save function. 
        fid is either the name of the file or a descriptor."""

    flagClose = False
    if (not isinstance(fid,file)):
        fid = open(fid, 'rb')
        flagClose = True

    d = _readheader(fid)

    if (not d['xy']):
        x = np.load(fid)
        x = slice(x[0],x[1],x[2])
        y = np.load(fid)
    else:
        x = np.load(fid)
        y = np.load(fid)

    if (flagClose): fid.close()

    return(XY[x,y])


# Load a signal form a text file
def loadtxt(fid,comments='#', delimiter=None, skiprows=0, xcol=-1,ycol=0):
    """Load a signal from a mulicolumn text file
        
        fid       : name or descriptor of the text file
        comments  : a line starting by this string will be skipped
        delimiter : the delimiter (can be a list) between the columns (if None, then ' ' and '\t' are used)
        skiprows  : if a list of column numbers (starting from 1) --> skip the corresponding columns
        xcol      : specifies the column number for the x array (if -1 then a Y signal is built)
        ycol      : specifies the column number for the y array
        """

    usecols = []
    if (xcol >= 0): usecols += [xcol]
    if (ycol >= 0): usecols += [ycol]

    a = np.loadtxt(fid, comments=comments, delimiter=delimiter, skiprows=skiprows, usecols=usecols)

    if (xcol<0 and ycol>=0): return Y[a]
    if (xcol>=0 and ycol>=0): 
        x = a[...,0]
        y = a[...,1]
        return XY[x,y]


#
# Pickling management
#


def _funcToMethod(func,klass,method_name=None):
    import new
    method = new.instancemethod(func,None,klass)
    if not method_name: method_name=func.__name__
    setattr(klass,method_name,method)

import cPickle
def _sigserialize(self):
    d = dict(name=self.name,xy=self.IsXY(),Y=self.Yarray(),dx=self.dx,x0=self.x0,interMode=self.interMode)
    if (self.IsXY()):
        d['X']=self['X'].Yarray()
    return((_sigdeserialize,(cPickle.dumps(d),)))

def _sigdeserialize(d):
    d = cPickle.loads(d)
    if (not d['xy']):
        sig = Signal(d['Y'])
        sig.dx = d['dx']
        sig.x0 = d['x0']
    else:
        sig = XY[d['X'],d['Y']]
    sig.name = d['name']
    sig.interMode = d['interMode']
    return sig

_funcToMethod(_sigserialize,Signal,"__reduce__")


def _fit(sig):
    x = sig.Xarray()
    y = sig.Yarray()
    
    return(np.linalg.lstsq(np.array([x,np.ones(sig.size)]).T,sig))
#    return(np.ma.polyfit(x,y,1,full=True))

_funcToMethod(_fit,Signal,"lstsq")



############################# FFT

def cfft(sig,shift=True,n=None):
    """Computes the complex fft of the signal `sig`
        
        - sig : the signal to compute the fourier transform of
        It should be a Y-signal with x0==0
        - shift : if True then the result has frequency 0 centered
        - n : if specified this is the size of the output (if greater than sig.size
        then zero padding)
        
        Examples
        
        >>> s = Signal(512)
        >>> s[1] = 1
        >>> c = cfft(s)
        >>> c
        <size=512,x0=-3.14159,dx=0.0122718;-1,-0.9999247018391445+0.0122715j,-0.9996988186962042+0.0245412j,-0.9993223845883495+0.0368072j,-0.9987954562051724+0.0490677j,-0.9981181129001492+0.0613207j,...>
        >>> icfft(c)
        <size=512;0,1+4.24755e-16j,0,2.586532461534974e-16+7.66212e-19j,0,-8.279219717160377e-18+9.85218e-18j,...>        
        """
    
    if (sig.IsXY() or sig.x0 != 0):
        raise TypeError,"sig should be a Y-signal with sig.x0 == 0"

    farray = np.fft.fft(sig.Yarray(),n=n)

    if (not shift):
        res = Signal(farray)
        res.x0 = 0
        res.dx = 2*np.pi/(res.size*sig.dx)
        return res


    else:
        res = Signal(farray.size)
        n = farray.size
        if (n%2 == 0):
            res[0:n/2] = Signal(farray[n/2:])
            res[n/2:] = Signal(farray[0:n/2])
            res.dx = 2*np.pi/(res.size*sig.dx)
            res.x0 = -n/2*res.dx
            return res
        else:
            res[0:(n-1)/2] = Signal(farray[(n+1)/2:])
            res[(n-1)/2:] = Signal(farray[0:(n+1)/2])
            res.dx = 2*np.pi/(res.size*sig.dx)
            res.x0 = -(n-1)/2*res.dx
            return res


def icfft(sig,shift=True,n=None):
    """Computes the complex ifft of the signal `sig`
        
        - sig : the signal to compute the inverse fourier transform of
        It should be a Y-signal.
        - shift : if True then the frequency 0 is identified (using the X).
        - n : if specified this is the size of the output (if greater than sig.size
        then zero padding)

        Examples
        
        >>> s = Signal(512)
        >>> s[1] = 1
        >>> c = cfft(s)
        >>> c
        <size=512,x0=-3.14159,dx=0.0122718;-1,-0.9999247018391445+0.0122715j,-0.9996988186962042+0.0245412j,-0.9993223845883495+0.0368072j,-0.9987954562051724+0.0490677j,-0.9981181129001492+0.0613207j,...>
        >>> icfft(c)
        <size=512;0,1+4.24755e-16j,0,2.586532461534974e-16+7.66212e-19j,0,-8.279219717160377e-18+9.85218e-18j,...>
        """
    
    if (sig.IsXY() or (not shift and sig.x0 != 0)):
        raise TypeError,"sig should be a Y-signal and if shift==False then sig.x0 should be 0"
    
    # We first unshift it
    if (shift):
        sig1 = Signal(sig.size)
        index0 = sig.X2Index(0)
        sig1[0:sig.size-index0] = sig[index0:]
        sig1[sig.size-index0:] = sig[0:index0]
    
    
    # Then we ifft
    farray = np.fft.ifft(sig1.Yarray(),n=n)
    
    sig1 = Signal(farray)
    sig1.x0 = 0
    sig1.dx = 2*np.pi/(sig.size*sig.dx)
    
    return sig1


def rfft(sig,n=None):
    """Computes the real fft of the signal `sig`
        
        - sig : the signal to compute the fourier transform of
        It should be a real-valued Y-signal with x0==0
        - n : if specified this is the size of the output (if greater than sig.size
        then zero padding)

        Examples
        
        >>> s = Signal(512)
        >>> s[1] = 1
        >>> r = rfft(s)
        >>> r
        <size=257,x0=0,dx=0.0122718;1,0.9999247018391445-0,0122715j,0.9996988186962042-0.0245412j,0.9993223845883495-0.0368072j,0.9987954562051724-0.0490677j,0.9981181129001492-0.0613207j,...>
        >>> irfft(r)
        <size=512;0,1.0,-3.791914848499984e-18,0,7.800423226929346e-18,...>
        """
    
    if (sig.IsXY() or sig.x0 != 0 or sig.IsComplex()):
        raise TypeError,"sig should be a real-valued Y-signal with sig.x0 == 0"
    
    farray = np.fft.rfft(sig.Yarray(),n=n)
    
    res = Signal(farray)
    n = farray.size
    res.dx = np.pi/((res.size-1)*sig.dx)
    res.x0 = 0
    return res

def irfft(sig,n=None):
    """Computes the inverse fft of a signal `sig` which has been obtained through rfft
        
        - sig : the signal to compute the inverse fourier transform of
        It should be a Y-signal with x0==0
        - n : if specified this is the size of the output (if greater than sig.size
        then zero padding)

        Examples
        
        >>> s = Signal(512)
        >>> s[1] = 1
        >>> r = rfft(s)
        >>> r
        <size=257,x0=0,dx=0.0122718;1,0.9999247018391445-0,0122715j,0.9996988186962042-0.0245412j,0.9993223845883495-0.0368072j,0.9987954562051724-0.0490677j,0.9981181129001492-0.0613207j,...>
        >>> irfft(r)
        <size=512;0,1.0,-3.791914848499984e-18,0,7.800423226929346e-18,...>
        """
    
    if (sig.IsXY() or sig.x0 != 0):
        raise TypeError,"sig should be a Y-signal with sig.x0 == 0"
    
    farray = np.fft.irfft(sig.Yarray(),n=n)
    
    res = Signal(farray)
    n = farray.size
    res.dx = np.pi/((sig.size-1)*sig.dx)
    res.x0 = 0
    return res


def rhilbert(sig):
    """Computes the Hilbert transform of a real-valued signal
        
        Examples : 
        
        >>> x = slice(0,2*pi,1024j)
        >>> s = XY(x,sin(x))
        >>> c = rhilbert(s)
        """
    return real(chilbert(sig))

def chilbert(sig):
    """Computes the Hilbert transform of a complex-valued signal"""
    out = cfft(sig)
    x = out['X',:]
    sgn = (x>0) - (x<0)
    out1 = (imag(out)-1j*real(out))*sgn
    return icfft(out1)



