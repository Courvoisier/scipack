lab.sig Tutorial
===============

The Signal class
----------------

The Signal class has been designed for dealing with 1d complex or real valued signals. Why not just simply use numpy.array ?
Well actually, as you will see the Signal class is compatible with numpy arrays and know how to share data with them. It has a few enhancements specific to 1d. The main enhancements are : 

    - The Signal class not only stores the value of the signal but also the abscissa points. This is extremely useful in many cases. Here are a few examples :
        * When you deal with sounds signals, it stores the sampling frequency (don't need to remember it!)
        * When you cut part of the signal, the class remembers from which part the cut was taken from
        * When you display a signal (see the disp module), you don't need to specify what the x's are
        * You can store x values which are uniformely sampled in a very efficient way (just the origin and the step)
        * All the operators/functions which are dealing with Signal's try manage the x abscissa
        * When you compute a histogram, you get Signal back which holds all the information for display
        * ...

    There are basically two cases : 
        * the signal is uniformely sampled (i.e., the method ``IsXY()`` returns ``False``, and the XYType of the signal is ``SIG_Y``) : two fields allow to code the sampling
            * ``dx`` : the sampling period
            * ``x0`` : the abscissa of the first point
        * the signal is not uniformely sampled (i.e., the method ``IsXY()`` returns ``True``, and the XYType of the signal is ``SIG_XY``) : the abscissa are stored using an array of doubles.
        
    In any case there is also of course an array (Y) of double or complex for coding the values of the signal
    
    - The Signal class lets you increase the size of a signal in-place (i.e., without loosing the reference to the signal and without loosing the data inside it). This lets you append new values (or signals) to a given signal. In order not to perform new allocation whenever a new value is appended to a signal, the Signal class always allocate a "little" more memory than needed. 
    
    - The Signal class let you perform extraction from a signal using either (integer) indices or (double) abscissa. When extracting from abscissa, you can specify an interpolation method.
    
    - The class Signal knows about border effects. You can specify for instance a mirror and periodic border effects so that you can extract a signal out of the actual range of indices or abscissa.
    
    - It is fully integrated to the scipack.sciplot.disp package which is an advanced graphic package for dispalying Signal's and interacting with them.

    
And of course, it is fully compatible with numpy arrays : 

    - Both the ``Y`` and ``X`` arrays can share data with a numpy array.
    - All the functions on arrays have been adapted to work on both Signal's and arrays. In the case they operate on Signal's they would return a Signal if an array is generally returned.
    - You can ``cPickle`` the Signal class for I/O binaries
    - It has an associated iterator class


Let's play around
-----------------

A constant Signal

>>> from scipack.lab.sig import *
>>> s = Signal(500) # A zero valued signal of size 500
>>> s
<size=500;0,0,0,0,0,0,...>

Creating a Signal from a numpy array
>>> import  numpy as np
>>> a = np.array([11.,2.,3.])
>>> s = Signal(a)
>>> s
<11,2,3>
>>> a[1] = -1
>>> s
<11,-1,3>

When a new value is appended the "sharing" with numpy is no longer guaranteed of course
>>> s.Append(18)
>>> s
<11,-1,3,18>
>>> s.Append(s)
>>> s
<size=8;11,-1,3,18,11,-1,...>

Extraction with indices

>>> s[3:8]
<x0=3,dx=1;18,11,-1,3,18>

As you see, it remembers the abscissa. Let's play with border effect

>>> s['bmir',5:12]
<size=7,x0=5,dx=1;-1,3,18,3,-1,11,...>
>>> s['bmir1',5:12]
<size=7,x0=5,dx=1;-1,3,18,18,3,-1,...>

Let's play with abscissa

>>> s.dx = 0.5
>>> s
<size=8,x0=0,dx=0.5;11,-1,3,18,11,-1,...>

Extraction with abscissa

>>> s['x',0]
11.0
>>> s['x',0.5]
-1.0
>>> s['x',1]
3.0

Or again

>>> s = Signal([1,2,3,4,5,6,7])
>>> s.dx = 0.5
>>> s['x',0:2] # Extract all abscissa between 0 and 2
<x0=0,dx=0,5;1,2,3,4,5>

resampling, using linear interpolation
>>> s['xlin',0:2:.25] 
<size=8,x0=0,dx=0,25;1,1.5,2,2.5,3,3.5,...>

Shortcuts
---------

There are mainly two shortcuts that let you build Signal's more easily. The most usefule one is ``Y[]``.

>>> Y[1,2,3,4j]
<1,2,3,4j>
>>> Y[1:4:0.4]
<size=8;1,1.4,1.8,2.2,2.6,3,...>
>>> Y[4:1:-1]
<4,3,2>
>>> s = Y[1,2]
>>> a = np.array([3.,4.])
>>> s1 = Y[-1:1:1,s,a,[5,6],(7,8),9]
<size=11;-1,0,1,2,3,4,...>

There is also a ``XY[]`` shortcur for building non uniformely sampled signals. Thus to build the sinus function on ``[0,2pi[`` sampled on 100 points
        
>>> x = slice(0,2*pi,100j)

using a slice with a step which is complex lets you specify a number of points instead of a step.

>>> s= XY[x,sin(x)]
>>> s
<size=100,x0=0,dx=0.0628319;0,0.06279051952931337,0.1253332335643043,0.1873813145857246,0.2486898871648548,0.3090169943749474,...>

You want to resample it using linear interpolation on 200 points ? Easy : 

>>> s['xlin',::200j]
<size=200,x0=0,dx=0.0311018;0,0.03108130716701012,0.06216261433402024,0.09312373583628396,0.1240823792836044,0.1548060720494789,...>


More examples to come ....

** In CONSTRUCTION **

