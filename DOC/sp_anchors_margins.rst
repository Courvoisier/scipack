
Anchors and Margins classes
========================

Anchors Classes
--------------

.. autoclass:: scipack.sciplot.sp.Anchor
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: scipack.sciplot.sp.AnchorFunc
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: scipack.sciplot.sp.AnchorGrid
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: scipack.sciplot.sp.AnchorNone
    :members:
    :undoc-members:
    :show-inheritance:


Margins classes
-------------

.. autoclass:: scipack.sciplot.sp.Margin
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: scipack.sciplot.sp.MarginCentered
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: scipack.sciplot.sp.MarginCenteredPixel
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: scipack.sciplot.sp.MarginFunc
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: scipack.sciplot.sp.MarginGrid
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: scipack.sciplot.sp.MarginInfinite
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: scipack.sciplot.sp.MarginPixel
    :members:
    :undoc-members:
    :show-inheritance:

