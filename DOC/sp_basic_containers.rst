

Basic Containers
==============

Container
-----------
.. autoclass:: scipack.sciplot.sp.Container
    :members:
    :undoc-members:
    :show-inheritance:
    :exclude-members: Draw, IsContainer

FramedView
--------------

.. autoclass:: scipack.sciplot.sp.FramedView
    :members:
    :undoc-members:
    :show-inheritance:
       :exclude-members: Draw

Grid
-----

.. autoclass:: scipack.sciplot.sp.Grid
    :members:
    :undoc-members:
    :show-inheritance:
    :exclude-members: Draw, IsGrid

Group
-------

.. autoclass:: scipack.sciplot.sp.Group
    :members:
    :undoc-members:
    :show-inheritance:
    :exclude-members: Draw, IsGroup

TopContainer
---------------

.. autoclass:: scipack.sciplot.sp.TopContainer
    :members: Print, ToPS, ToSVG, GetCurTopContainer
    :undoc-members:
    :show-inheritance:
      :exclude-members: Draw, IsTopContainer

View
-----

.. autoclass:: scipack.sciplot.sp.View
    :members:
    :undoc-members:
    :show-inheritance:
    :exclude-members: Draw, IsView
