Displaying Signals - The disp tutorial
======================================

Configuration
--------------

If in the following tutorial the disp window has a bad position or size, you can set the default size and position
in the ``.spconfig`` file in your home directory.
Just edit the two lines

    @DispWindow.pos = (50,800)
    
    @DispWindow.size = (600,300)


 
Basics
------

The disp function is part of the scipack.sciplot.disp module. It is an advanced function for displaying Signal's and interacting with them.
This tutorial includes all what you have learned in other tutorials. Please refer to them when you don't understand what is going on!


Let's play around right away.

First we need to import some stuff (you should have the following lines automatically
executed at each startup)

>>> from scipack.lab.sig import *
>>> from scipack.sciplot.disp import disp

Let's create a simple random noise of 2000 points (sampling period is 0.5)
>>> w = rnorm(2000)
>>> w.dx = 0.5

Display it 

>>> disp(w)

By default (in the .spconfig file) there are lots of ways to interact with this figure. Here is a non exhaustive list : 

    - press on any object the 'escape' key to change/visualize object attributes (try to change the name of the title)
    - press on any object the 'tab' key to visualize interactors available anc change their attributes
    - move the title and the labels using drag and drop with the left button (while holding the shift key pressed)
    - right/left click in the view to zoom (drop part which is on the right/left x-side of the mouse)
    - hit the space key in the view to unzoom
    - hit the 'z' key on the view to switch between 3 zoom modes :
        * selecting a y-constrained rectangle
        * selecting a no-constrained rectangle
        * using the right/left click as described abpve
    - The 'c' key should change the cursor mode
    - scrolling using the mouse wheel
    - zooming around one point using the mouse wheel + shift key
    - hitting the 'f' key while the mouse close to a signal, performs linear fit
    - hitting the 'l' pops up a menu for choosing lin-lin or log-log scales

You can set some attributes directly in the ``disp`` command

>>> disp(w(pen='blue',symbol='o')

These attributes are attributes of the sciplot object scipack.sciplot.objects.spsig.SignalObject used for displaying Signal's (you can learn about them using the doc).
If you want to display ``w`` in another window named ``Nice`` : 

>>> disp("Nice",w(pen='red'))

By default the "last" window is used for display (i.e., the last one you displayed something in or the last one visited by the mouse).
Let's add a comment in the ``View``. For that purpose we need to get the View object : 

>>> import scipack.sciplot.sp as sp
>>> view = sp.oget1(klass=sp.View) # you could have done also : view = sp.oget1("::view")
>>> r = sp.Comment(view,pt=(500,0),str="This is the \n point (500,0)").config()

Since you might want to add comment a lot, there is more convenient function for doing so.
The above 2 lines are equivalent to

>>> r = sp.CommentAdd("::view",pt=(500,0),str="This is the \n point (500,0)")

Try to zoom/unzoom/scroll etc ... Try to zoom using the mouse wheel + shift key around the (500,0) point ....
You can move the comment box using the shift+left button drag. You don't like the color ?
Make the text box of the comment a little transparent :

>>> r.textObject(colour=wx.Colour(0,200,0,200))

The function disp does not copy the signal. Thus it holds a reference to the signal ``w``. There is a very convenient way to adress the SignalObject associated to a given Signal :

>>> o = w.obj()
Attributes of spSignalObject(Nice:fv1:view:1)
['brush', 'brushColour', 'brushStyle', 'colour', 'font', 'fontColour', 'fontFaceName', 'fontFamily', 'fontSize', 'fontStyle', 'fontWeight', 'labelx', 'labely', 'pen', 'penColour', 'penStyle', 'penWidth', 'symbol', 'title']
>>> o
spSignalObject(Nice:fv1:view:1)

The method ``w.obj()`` is the same as ``o()``. Thus with no argument it prints the different attributes. You want to change the colour of the signal ?

>>> w.obj(pen='black')

If the Signal is complex, then two ``SignalObject`` are created, one from the real part and one from the imaginary part and displayed in the same view.
The imaginary part is displayed using a much more transparent color.

>>> x = slice(0,2*pi,1024j)
>>> s = XY(x,sin(x)+1j*cos(x))
>>> disp(s)

The window has a TopContainer associated to it which is a grid. disp let's you display signals using this grid.

>>> w = rnorm(2000)
>>> w.dx = 0.5
>>> x = rnorm(1500)
>>> y = rnorm(1700)
>>> z = rnorm(1800)
>>> disp([w,x(pen='blue',symbol='o')],[[y(pen='green'),z]])

By default the x-bounds of all the plots within a window are synchronized. Try to zoom or scroll on one plot, you will understand. 
Do you want to synchronize the y-bounds too ? You can do it two ways.

    - Using the tab key on any view, look for the interactor zoom_syncro and change the mode to 'xy'
    
    - just type 

>>> disp(synchro='xy')

Go ahead ... zoom again. 
Desynchronize everything ?

>>> disp(synchro='')

Let us display x in another window

>>> disp("Another",x)

Do you want to synchronize the two views of ``x`` ? We have to get them first. Let's look at all the views we've got

>>> sp.oget(klass=sp.View)
[spView(Nice:fv1:view),
 spView(Nice:fv2:view),
 spView(Nice:fv3:view),
 spView(Another:fv1:view)]

The ``fv`` objects are the FramedView's which are numbered from top-left to bottom-right. Thus the two views of the signal ``x`` are ``Nice:fv2:view`` and ``Another:fv1:view``
Let's synchronize the x-bounds of these Views.

>>> from scipack.sciplot.interactors.zoom_synchro import synchro,desynchro
>>> sync = synchro('Nice:fv1:view','Another:fv1:view')

Try to zoom one of the view.
Let's desynchronize : 

>>> desynchro(sync)


Playing around with the cursor
------------------------------

Whenever the mouse is close to a graphic object, the cursor interactor sends a message to this object to get information about 
what information should be displayed and around what point the cursor should be drawn.
Actually the interactor calls a callback function and you can set the callback function using the method :func:`~scipack.sciplot.sp.Object.SetCursorCallback`.
When the object is displaying a signal through the ``disp`` command, the object corresponds to an instance
of the class :class:`~scipack.sciplot.disp.SignalObjectDisp`.
It automatically sets some cursor callbacks depending on the fields of the signal to be displayed.

Here is an example

>>> a = rnorm(26)
>>> a.label = [chr(i) for i in range(65,65+26)]
>>> disp(a)

Just go on the window with the mouse, elect the window and then hit 'c' such that you switch to the regular cursor mode.
Now move the mouse and see what's printed at the bottom.

We can also specify a callback to print more complex stuff.
More precisely, the signal to be displayed must have a ``cursor`` field which is a callable of the form
                ``f(obj,sig,index,mouse) --> (string,x,y)``
where
    - ``obj``    : is the ``sp.SignalObjectDisp``
    - ``sig``    : the corresponding signal
    - ``mouse``  : the mouse coordinate in the signal coordinate
    - ``index``  : the index of the signal which is the closest to the mouse
It must return a triplet
    - ``string`` : the string to be printed
    - ``x`` and ``y`` : the (signal) coordinate of the cursor

So the standard cursor behavior is obtained using the callback

>>> def f(obj,sig,index,mouse) 
...     return ("x = %g [index = %d] y = %g" % (sig['X',index],index,sig[index]),sig['X',index],sig[index])

Thus you could do

>>> def f(obj,sig,index,mouse): 
...     return ("(%g,%g) and y^2 = %g" % (sig['X',index],sig[index],sig[index]*sig[index]),sig['X',index],sig[index])
>>> a = rnorm(26)
>>> a.cursor=f
>>> disp(a)


Printing or creating a postscript or svg file
---------------------------------------------

You can print or create a postscript or svg file from the content of a
:class:`~scipack.sciplot.sp.TopContainer` instance. For that purpose you can use one of the methods
:func:`~scipack.sciplot.sp.TopContainer.Print`, :func:`~scipack.sciplot.sp.TopContainer.ToPS` or :func:`~scipack.sciplot.sp.TopContainer.ToSVG`.


>>> r = prim(rnorm(1000))
>>> disp(r)
>>> tc = sp.TopContainer.GetCurTopContainer()
>>> tc.Print()

Let us point out that each object has a field ``whiteBrushPrinting`` which (when set) allows to force the background (i.e., the brush) to be white when printing operations are performed.


** More examples to come **










